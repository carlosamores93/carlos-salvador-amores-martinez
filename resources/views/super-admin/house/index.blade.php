@extends('templates.super-admin.layout')

@section('head')
	<title>Houses</title>
	<meta name="description" content="All houses">
	<!-- DataTables -->
	<link rel="stylesheet"
		href="{{ asset('admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection


@section('content')

	<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All houses</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">All houses</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
	</section>

	<section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
			  <h3 class="card-title">
					List all houses
					<a href="{{ route('houses.create') }}"
						type="button" class="btn btn-outline-primary">
						Create house
					</a>
					<a href="{{ route('houses.index') }}?like=true"
						type="button" class="btn btn-outline-success">
						House liked
					</a>
					<a href="{{ route('houses.index') }}?like=0"
						type="button" class="btn btn-outline-warning">
						House dont liked
					</a>
					<a href="{{ route('houses.index') }}?precio=120000"
						type="button" class="btn btn-outline-default">
						Filter by 120.000€
					</a>
					<a href="{{ route('houses.index') }}?precio=120000&like=1"
						type="button" class="btn btn-primary">
						Liked and 120.000€
					</a>
				</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
					<tr>
						<th>Like</th>
						<th>Dirección Precio Superficie</th>
						{{-- <th>Precio</th>
						<th>Superficie</th>
						<th>Precio m²</th> --}}
						<th>Links</th>
						<th>Actiones</th>
					</tr>
                </thead>
                <tbody>
					@foreach ($houses as $house)	
						<tr>
							<td>
								{{ $house->like ? 'YES' : 'NO'}}
							</td>
							<td>
								<a href="{{ route('houses.edit',$house) }}">
									{{$house->direccion}} #{{$house->numero}} {{$house->piso}} {{$house->puerta}}
								</a>
								<br>
								Precio: {{number_format($house->precio, 0, ',', '.')}}€
								<br>
								Superficio: {{$house->superficie}} m²
								<br>
								Precio m²: {{number_format($house->precio_m_cuadrado, 2, ',', '.')}} €/m²
							</td>
							{{-- <td>{{$house->precio}}</td>
							<td>{{$house->superficie}} m²</td>
							<td>{{number_format($house->precio_m_cuadrado, 2, ',', ' ')}} €/m²</td> --}}
							<td>
								Catastro: 
								<a href="{{$house->catastro}}" target="_blank">
									Link catastro
								</a>
								<br>
								Link:
								<a href="{{$house->link_ad}}" target="_blank">
									Link anuncio
								</a>
								<br>
								Map:
								<a href="{{$house->map}}" target="_blank">
									Link mapa
								</a>
								<br>
							</td>
							<td>
								{{-- <a class="btn btn-success" href="{{ route('houses.edit',$house) }}">
									Edit
								</a> --}}
								<form method="post"
								onclick="return confirm('Do you want delete this item? Are you sure?')"
								action="{{ route('houses.destroy', $house) }}">
									@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-danger">Delete</button>
								</form>
							</td>
						</tr>                
					@endforeach
                </tbody>
                <tfoot>
					<tr>
						<th>Id</th>
						<th>Direccion Precio Superficie</th>
						{{-- <th>Precio</th>
						<th>Superficie</th>
						<th>Precio m²</th> --}}
						<th>Links</th>
						<th>Actiones</th>
					</tr>
                </tfoot>
			  </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
	
@endsection


@section('script')
	<!-- jQuery -->
	<script src="{{ asset('admin-lte/plugins/jquery/jquery.min.js') }}"></script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- DataTables -->
	<script src="{{ asset('admin-lte/plugins/datatables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('admin-lte/dist/js/adminlte.min.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('admin-lte/dist/js/demo.js') }}"></script>
	<!-- page script -->
	<script>
	$(function () {
		$("#example1").DataTable();
		$('#example2').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": false,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		});
	});
	</script>
@endsection