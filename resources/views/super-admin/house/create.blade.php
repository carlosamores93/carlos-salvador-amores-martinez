@extends('templates.super-admin.layout')

@section('head')
	<title>Create house</title>
	<meta name="description" content="Create house">
	<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
@endsection


@section('content')

	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create house</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Create house</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
	</section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
			<div class="col-md-12">
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">Create house</h3>
					</div>
					<!-- /.card-header -->
					<!-- form start -->
					<form method="POST" class="form-horizontal"
                        action="{{ route('houses.store') }}" enctype="multipart/form-data">
                        @csrf
						@method('POST')

						<div class="row">
							<div class="col-md-12">
								<div class="card-body">
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Direccion</label>
										<div class="col-sm-9">
											<input type="text" name="direccion" class="form-control"
												id="inputEmail3" placeholder="Direccion">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Numero</label>
										<div class="col-sm-9">
											<input type="number" name="numero" class="form-control"
												id="inputEmail3" placeholder="Numero">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Piso</label>
										<div class="col-sm-9">
											<input type="number" name="piso" class="form-control"
												id="inputEmail3" placeholder="Piso">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Puerta</label>
										<div class="col-sm-9">
											<input type="text" name="puerta" class="form-control"
												id="inputEmail3" placeholder="Puerta">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Superficio</label>
										<div class="col-sm-9">
											<input type="number" name="superficie" class="form-control"
												id="inputEmail3" placeholder="Superficio">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Precio</label>
										<div class="col-sm-9">
											<input type="number" name="precio" class="form-control"
												id="inputEmail3" placeholder="Precio">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Habitaciones</label>
										<div class="col-sm-9">
											<input type="text" name="habitaciones" class="form-control"
												id="inputEmail3" placeholder="Habitaciones">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Terraza</label>
										<div class="col-sm-9">
											<input type="text" name="terraza" class="form-control"
												id="inputEmail3" placeholder="Terraza">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Cocina</label>
										<div class="col-sm-9">
											<input type="text" name="cocina" class="form-control"
												id="inputEmail3" placeholder="Cocina">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Baño</label>
										<div class="col-sm-9">
											<input type="text" name="banio" class="form-control"
												id="inputEmail3" placeholder="Baño">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Salon</label>
										<div class="col-sm-9">
											<input type="text" name="salon" class="form-control"
												id="inputEmail3" placeholder="Salon">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Trastero</label>
										<div class="col-sm-9">
											<input type="text" name="trastero" class="form-control"
												id="inputEmail3" placeholder="Trastero">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Ascensor</label>
										<div class="col-sm-9">
											<input type="text" name="ascensor" class="form-control"
												id="inputEmail3" placeholder="Ascensor">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Catastro</label>
										<div class="col-sm-9">
											<input type="text" name="catastro" class="form-control"
												id="inputEmail3" placeholder="Catastro">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Link anuncio</label>
										<div class="col-sm-9">
											<input type="text" name="link_ad" class="form-control"
												id="inputEmail3" placeholder="Link anuncio">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Mapa</label>
										<div class="col-sm-9">
											<input type="text" name="map" class="form-control"
												id="inputEmail3" placeholder="Mapa">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Fecha construccion</label>
										<div class="col-sm-9">
											<input type="text" name="fecha_construccion" class="form-control"
												id="inputEmail3" placeholder="Fecha construccion">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Estado cargos</label>
										<div class="col-sm-9">
											<input type="text" name="estado_cargas" class="form-control"
												id="inputEmail3" placeholder="Estado cargos">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Inspección tecnica edificio</label>
										<div class="col-sm-9">
											<input type="text" name="inspecccion_tecnica_edificios" class="form-control"
												id="inputEmail3" placeholder="Inspección tecnica edificio">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Electr Font Baja</label>
										<div class="col-sm-9">
											<input type="text" name="estado_fontaneria_electricidad" class="form-control"
												id="inputEmail3" placeholder="Electr Font Baja">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Comunidad</label>
										<div class="col-sm-9">
											<input type="text" name="comunidad_propietarios" class="form-control"
												id="inputEmail3" placeholder="Comunidad propietarios">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Obras previstas</label>
										<div class="col-sm-9">
											<input type="text" name="obras_previstas_edificio" class="form-control"
												id="inputEmail3" placeholder="Obras previstas">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Calefaccion</label>
										<div class="col-sm-9">
											<input type="text" name="calefaccion" class="form-control"
												id="inputEmail3" placeholder="Calefaccion">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Descripcion</label>
										<div class="col-sm-9">
											<textarea name="description" class="form-control" 
												id="" cols="30" rows="10"></textarea>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Like</label>
										<div class="col-sm-9">
											<div class="custom-control custom-radio">
												<input type="radio" class="form-control custom-control-input" id="customControlValidation1" name="like" value="1" required>
												<label class="custom-control-label" for="customControlValidation1">LIKE</label>
											</div>
											<div class="custom-control custom-radio">
												<input type="radio" class="form-control custom-control-input" id="customControlValidation2" name="like" value="0" required>
												<label class="custom-control-label" for="customControlValidation2">NOT LIKE</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						

						
						<!-- /.card-body -->
						<div class="card-footer">
							<button type="submit" class="btn btn-info">Save</button>
							<a href="{{ route('houses.index') }}"
							type="submit" class="btn btn-default float-right">Cancel</a>
						</div>
						<!-- /.card-footer -->
					</form>
				</div>
			</div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection


@section('script')
	<!-- jQuery -->
	<script src="{{ asset('admin-lte/plugins/jquery/jquery.min.js') }}"></script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- bs-custom-file-input -->
	<script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('admin-lte/dist/js/adminlte.min.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('admin-lte/dist/js/demo.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			bsCustomFileInput.init();
		});
	</script>
	<script>
			CKEDITOR.replace( 'editor1' );
	</script>
@endsection