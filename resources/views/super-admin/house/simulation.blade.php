@extends('templates.super-admin.layout')

@section('head')
	<title>Simulation</title>
	<meta name="description" content="Simulation">
	<!-- DataTables -->
	<link rel="stylesheet"
		href="{{ asset('admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection


@section('content')

	<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Simulation</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Simulation</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
	</section>

	<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- Input addon -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Simulation</h3>
              </div>
              <div class="card-body">

                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Vivienda</span>
                  </div>
				  <input type="number" id='precio' min="50000" max="500000" step="1000"
				  	class="form-control" placeholder="Precio vivienda">
				</div>
				
				 <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Ahorros</span>
                  </div>
				  <input type="number" class="form-control" min="10000" max="100000"
				  	id='ahorros' placeholder="Ahorros totales">
				</div>
				
				<div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Inmobiliaria</span>
                  </div>
				  <input type="number" class="form-control" min="0" max="10000"
				  	id='inmobiliaria' placeholder="Inmobiliaria">
				</div>

				<button id='runSimulation'
					type="button" class="btn btn-block bg-gradient-primary">
					Ejecutar simulación
				</button>

				
				{{-- <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Gastos compra-venta</span>
                  </div>
				  <input type="number" class="form-control"
				  	id='gastos' placeholder="Gastos compra-venta">
				</div> --}}

				{{-- <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Ahorros</span>
                  </div>
				  <input type="number" class="form-control"
				  	id='ahorros_financiacion' disabled>
				</div> --}}

				{{-- <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Financiación</span>
                  </div>
				  <input type="text" class="form-control" 
				  	id='financiacion' disabled >
				</div> --}}
				<br>
				<table class="table table-bordered text-center" id="tableHip"
					hidden="true">
                  {{-- <thead>                  
                    <tr>
                      <th>Concepto</th>
                      <th>Coste</th>
                    </tr>
                  </thead> --}}
                  <tbody>
					{{-- <tr>
						<td colspan="2">
							<b>Gastos de Compra-Venta
						</td>
					</tr> --}}
					<tr>
                      <td>
						  IVA o Transmisiones (6%)
					  </td>
                      <td id="impuestos"></td>
					</tr>
					<tr>
                      <td>
						  Notaría
					  </td>
                      <td id="notaria"></td>
					</tr>
					<tr>
                      <td>
						  Registro
					  </td>
                      <td id="registro"></td>
					</tr>
					<tr>
                      <td>
						  Gestoría
					  </td>
                      <td id="gestoria"></td>
					</tr>
					<tr>
                      <td>
						<strong>Total gastos Compra-Venta</strong>
					  </td>
                      <td>
						  <strong id="gastosCompraVenta"></strong>
					  </td>
					</tr>
                    {{-- <tr>
						<td colspan="2">
							<b>Gastos de Hipoteca
						</td>
					</tr> --}}
					<tr>
                      <td>
						  Tasación
					  </td>
                      <td id="gastosTasacion"></td>
					</tr>
					<tr>
                      <td>
						<strong>Total gastos Hipoteca</strong>
					  </td>
                      <td>
						  <strong id="gastosHipoteca"></strong>
					  </td>
					</tr>
					<tr>
						<td>Inmobiliaria</td>
						<td>
							<strong id="gastosInmobiliaria"></strong>
						</td>
					</tr>
					<tr>
						<td>
							<strong>
								Total Gastos</td>
						</strong> 
						<td>
							<strong id="totalGastos"></strong>
						</td>
					</tr>
					
					<tr>
                      <td>Ahorros aportados</td>
                      <td id="ahorrosAportados"></td>
					</tr>
					<tr>
                      <td>Precio vivienda</td>
                      <td id="precioVivienda"></td>
                    </tr>
					<tr>
                      <td>Financiación</td>
                      <td id="financiacion"></td>
                    </tr>
                  </tbody>
				</table>
				<br>

				<div id="result">
				</div>
				<br>

				
				
                <!-- /input-group -->
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
	
@endsection


@section('script')
	<!-- jQuery -->
	<script src="{{ asset('admin-lte/plugins/jquery/jquery.min.js') }}"></script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- bs-custom-file-input -->
	<script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('admin-lte/dist/js/adminlte.min.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('admin-lte/dist/js/demo.js') }}"></script>
	<!-- page script -->
	<script type="text/javascript">
		$(document).ready(function () {
			bsCustomFileInput.init();
			$('#runSimulation').click(function(event) {
				let precio = $('#precio').val();
				let ahorros = $('#ahorros').val();
				let inmobiliaria = $('#inmobiliaria').val();

				let tasacion = 351.04;
				let gastosHipoteca = tasacion;

				let impuestos = precio * 0.06;
				let notaria = 950;
				let registro = 614.40;
				let gestoria = 435.60;
				let gastosCompraVenta = impuestos + notaria + registro + gestoria;

				if (inmobiliaria < 10) {
					inmobiliaria = precio * inmobiliaria / 100 * 1.21;
				} else {
					inmobiliaria = inmobiliaria * 1.21;
				}

		
				$('#gastosTasacion').html(Math.round(tasacion) + ' €');
				$('#impuestos').html(Math.round(impuestos) + ' €');
				$('#notaria').html(Math.round(notaria) + ' €');
				$('#registro').html(Math.round(registro) + ' €');
				$('#gestoria').html(Math.round(gestoria) + ' €');
				 $('#gastosHipoteca').html(Math.round(gastosHipoteca) + ' €');
				 $('#gastosCompraVenta').html(Math.round(gastosCompraVenta) + ' €');
				 $('#gastosInmobiliaria').html(Math.round(inmobiliaria) + ' €');
				 let totalGastos = gastosHipoteca + gastosCompraVenta + inmobiliaria;
				 $('#totalGastos').html('<b>' + Math.round(totalGastos) + ' €</b>');
				 let ahorrosAportados = ahorros - totalGastos;
				 $('#ahorrosAportados').html(Math.round(ahorrosAportados) + ' €');
				 let financiacion = precio - ahorrosAportados;
				 $('#financiacion').html('<b>' + Math.round(financiacion) + ' €</b>');
				 $('#precioVivienda').html('<b>' + precio + ' €</b>');
				 $('#tableHip').removeAttr('hidden');

				let porcentaje = financiacion / precio * 100;
				$('#result').html('Solicitas una hipoteca de <b> ' + Math.round(financiacion)
					+ ' € </b>, lo que supone un <b>' + Math.round(porcentaje) + ' %</b> del precio de la vivienda (<b>'
					+ precio +' €</b>)');




				// let gastos = $('#gastos').val();
				// let ahorros_financiacion = ahorros - gastos - inmobiliaria;
				// let financiacion = precio - ahorros_financiacion;
				// $('#ahorros_financiacion').val(ahorros_financiacion);
				// let fin = 'Hipoteca: ' + financiacion + ' €, que es el ' 
				// + financiacion/precio*100 + ' % del precio final'
				// $('#financiacion').val(fin);
			});
		});
	</script>
@endsection