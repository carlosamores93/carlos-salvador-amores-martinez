@extends('templates.super-admin.layout')

@section('head')
	<title>Super Admin</title>
    <meta name="description" content="">
	<!-- Tempusdominus Bbootstrap 4 -->
	<link rel="stylesheet" href="{{ asset('admin-lte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
	<!-- iCheck -->
	<link rel="stylesheet" href="{{ asset('admin-lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
	<!-- JQVMap -->
	<link rel="stylesheet" href="{{ asset('admin-lte/plugins/jqvmap/jqvmap.min.css') }}">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="{{ asset('admin-lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="{{ asset('admin-lte/plugins/daterangepicker/daterangepicker.css') }}">
	<!-- summernote -->
	<link rel="stylesheet" href="{{ asset('admin-lte/plugins/summernote/summernote-bs4.css') }}">
	{{-- <script src="https://cdn.ckeditor.com/ckeditor5/18.0.0/classic/ckeditor.js"></script> --}}
	{{-- <script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script> --}}
@endsection


@section('content')
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Dashboard</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
				<li class="breadcrumb-item"><a href="{{route('super.admin.home')}}">Home</a></li>
				<li class="breadcrumb-item active">Dashboard v1</li>
				</ol>
			</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">



			{{-- <textarea name="editor1"></textarea>
			<script>
					CKEDITOR.replace( 'editor1' );
			</script> --}}


			{{-- <div id="editor">
				<p>This is some sample content.</p>
			</div>
			<script>
			ClassicEditor
				.create( document.querySelector( '#editor' ) )
				.catch( error => {
					console.error( error );
				} );
			</script> --}}
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div class="col-lg-3 col-6">
					<!-- small box -->
					<div class="small-box bg-info">
						<div class="inner">
							<h3>{{$numUsers}}</h3>

							<p>Users</p>
						</div>
						<div class="icon">
							<i class="ion ion-person"></i>
						</div>
						<a href="{{route('user.index')}}" class="small-box-footer">
							More info <i class="fas fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>
				<!-- ./col -->
				<div class="col-lg-3 col-6">
					<!-- small box -->
					<div class="small-box bg-success">
					<div class="inner">
						<h3>{{$numWorks}}</h3>

						<p>Works</p>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="{{ route('work.index') }}" class="small-box-footer">
						More info <i class="fas fa-arrow-circle-right"></i>
					</a>
					</div>
				</div>

				<div class="col-lg-3 col-6">
					<!-- small box -->
					<div class="small-box bg-warning">
					<div class="inner">
						<h3>{{$numNotices}}</h3>

						<p>Notices</p>
					</div>
					<div class="icon">
						<i class="ion ion-alert"></i>
					</div>
					<a href="{{ route('notice.index') }}" class="small-box-footer">
						More info <i class="fas fa-arrow-circle-right"></i>
					</a>
					</div>
				</div>

				<div class="col-lg-3 col-6">
					<!-- small box -->
					<div class="small-box bg-danger">
					<div class="inner">
						<h3>{{$numPosts}}</h3>

						<p>Posts</p>
					</div>
					<div class="icon">
						<i class="ion ion-pencil"></i>
					</div>
					<a href="{{ route('post.index') }}" class="small-box-footer">
						More info <i class="fas fa-arrow-circle-right"></i>
					</a>
					</div>
				</div>

				<div class="col-lg-3 col-6">
					<!-- small box -->
					<div class="small-box bg-danger">
					<div class="inner">
						<h3>{{$numAmazonProducts}}</h3>

						<p>Amazon product</p>
					</div>
					<div class="icon">
						<i class="ion ion-product"></i>
					</div>
					<a href="{{ route('amazon-product.index') }}" class="small-box-footer">
						More info <i class="fas fa-arrow-circle-right"></i>
					</a>
					</div>
				</div>
				<!-- ./col -->
			</div>
			<!-- /.row -->

			
		</div><!-- /.container-fluid -->
	</section>
	<!-- /.content -->
@endsection


@section('script')
	<!-- jQuery -->
	<script src="{{ asset('admin-lte/plugins/jquery/jquery.min.js') }}"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="{{ asset('admin-lte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	$.widget.bridge('uibutton', $.ui.button)
	</script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- ChartJS -->
	<script src="{{ asset('admin-lte/plugins/chart.js/Chart.min.js') }}"></script>
	<!-- Sparkline -->
	<script src="{{ asset('admin-lte/plugins/sparklines/sparkline.js') }}"></script>
	<!-- JQVMap -->
	<script src="{{ asset('admin-lte/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
	<script src="{{ asset('admin-lte/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
	<!-- jQuery Knob Chart -->
	<script src="{{ asset('admin-lte/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
	<!-- daterangepicker -->
	<script src="{{ asset('admin-lte/plugins/moment/moment.min.js') }}"></script>
	<script src="{{ asset('admin-lte/plugins/daterangepicker/daterangepicker.js') }}"></script>
	<!-- Tempusdominus Bootstrap 4 -->
	<script src="{{ asset('admin-lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
	<!-- Summernote -->
	<script src="{{ asset('admin-lte/plugins/summernote/summernote-bs4.min.js') }}"></script>
	<!-- overlayScrollbars -->
	<script src="{{ asset('admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('admin-lte/dist/js/adminlte.js') }}"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="{{ asset('admin-lte/dist/js/pages/dashboard.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('admin-lte/dist/js/demo.js') }}"></script>
@endsection