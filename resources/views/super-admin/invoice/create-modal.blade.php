<!-- Button trigger modal -->
<a href="" data-toggle="modal" data-target="#exampleModalCreate">
	New invoice
</a>

<!-- Modal -->
<div class="modal fade" id="exampleModalCreate" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">

				<form method="POST" class="form-horizontal"
					action="{{ route('invoice.store') }}" enctype="multipart/form-data">
					@csrf

					<div class="row">
						<div class="col-md-12">
							<div class="card-body">
								<div class="form-group row">
									<label for="address" class="col-sm-3 col-form-label">Address</label>
									<div class="col-sm-9">
										<input type="text" name="address" class="form-control"
											id="address" placeholder="Address"
											required>
									</div>
								</div>
								<div class="form-group row">
									<label for="city" class="col-sm-3 col-form-label">City</label>
									<div class="col-sm-9">
										<input type="text" name="city" class="form-control"
											id="city" placeholder="city"
											required>
									</div>
								</div>
								<div class="form-group row">
									<label for="nif" class="col-sm-3 col-form-label">NIF</label>
									<div class="col-sm-9">
										<input type="text" name="nif" class="form-control"
											id="nif" placeholder="nif"
											required>
									</div>
								</div>
								<div class="form-group row">
									<label for="status" class="col-sm-3 col-form-label">Status</label>
									<div class="col-sm-9">
										<div class="custom-control custom-radio">
											<input type="radio" class="form-control custom-control-input"
											id="customControlValidation1" name="status"
											value="1" required>
											<label class="custom-control-label" for="customControlValidation1">Active</label>
										</div>
										<div class="custom-control custom-radio">
											<input type="radio" class="form-control custom-control-input"
											id="customControlValidation2" name="status"
											value="0" required>
											<label class="custom-control-label" for="customControlValidation2">Inactive</label>
										</div>
									</div>
								</div>
									<div class="form-group row">
										<div class="col-sm-3">
											<input type="text"
												class="form-control"
												id="service"
												name="service"
												placeholder="Service"
												>
										</div>
										<div class="col-sm-6">
											<input type="text"
												class="form-control"
												id="service_description"
												name="service_description"
												placeholder="Description"
												>
										</div>
										<div class="col-sm-3">
											<input type="number"
												class="form-control"
												id="price"
												name="price"
												placeholder="Price"
												>
										</div>
									</div>
							</div>
						</div>
					</div>
					<div>
						<button type="submit" class="btn btn-info">Save</button>
						<button type="button" class="btn btn-secondary float-right" data-dismiss="modal">
							Close
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>