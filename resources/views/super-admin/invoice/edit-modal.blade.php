<!-- Button trigger modal -->
<a href="" data-toggle="modal" data-target="#exampleModal{{$invoice->id}}" class="btn btn-info mt-1">
	<i class="far fa-edit"></i>
</a>

<!-- Modal -->
<div class="modal fade" id="exampleModal{{$invoice->id}}" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">

				<form method="POST" class="form-horizontal" action="{{ route('invoice.update', $invoice) }}"
					enctype="multipart/form-data">
					@csrf
					@method('PUT')

					<div class="row">
						<div class="col-md-12">
							<div class="card-body">
								<div class="form-group row">
									<label for="address" class="col-sm-3 col-form-label">Address</label>
									<div class="col-sm-9">
										<input type="text" name="address" class="form-control" id="address"
											placeholder="Address" value="{{ $invoice->address }}" required>
									</div>
								</div>
								<div class="form-group row">
									<label for="city" class="col-sm-3 col-form-label">City</label>
									<div class="col-sm-9">
										<input type="text" name="city" class="form-control" id="city" placeholder="city"
											value="{{ $invoice->city }}" required>
									</div>
								</div>
								<div class="form-group row">
									<label for="nif" class="col-sm-3 col-form-label">NIF</label>
									<div class="col-sm-9">
										<input type="text" name="nif" class="form-control" id="nif" placeholder="nif"
											value="{{ $invoice->nif }}" required>
									</div>
								</div>
								<div class="form-group row">
									<label for="status" class="col-sm-3 col-form-label">Status</label>
									<div class="col-sm-9">
										<div class="custom-control custom-radio">
											<input type="radio" class="form-control custom-control-input"
												id="customControlValidation1{{$invoice->id}}" name="status" value="1"
												@if ($invoice->status === 1) checked @endif required>
											<label class="custom-control-label"
												for="customControlValidation1{{$invoice->id}}">Active</label>
										</div>
										<div class="custom-control custom-radio">
											<input type="radio" class="form-control custom-control-input"
												id="customControlValidation2{{$invoice->id}}" name="status" value="0"
												@if ($invoice->status === 0) checked @endif required>
											<label class="custom-control-label"
												for="customControlValidation2{{$invoice->id}}">Inactive</label>
										</div>
									</div>
								</div>
								@foreach(json_decode($invoice->description) as $key => $value)
								<div class="form-group row">
									<div class="col-sm-3">
										<input type="text" class="form-control" id="service_{{$invoice->id}}_{{$key}}"
											name="service_{{$invoice->id}}_{{$key}}" placeholder="Service"
											value="{{$value->service}}">
									</div>
									<div class="col-sm-9">
										<input type="text" class="form-control"
											id="service_description_{{$invoice->id}}_{{$key}}"
											name="service_description_{{$invoice->id}}_{{$key}}"
											placeholder="Description" value="{{$value->description}}">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-sm-3">
										<input type="number" class="form-control" id="price_{{$invoice->id}}_{{$key}}"
											name="price_{{$invoice->id}}_{{$key}}" placeholder="Price" step="any"
											value="{{$value->price}}">
									</div>
									<div class="col-sm-6">
										<label class="" for="tax_{{$invoice->id}}_{{$key}}">
											IVA incluido
											<input type="checkbox" class="" name="tax_{{$invoice->id}}_{{$key}}"
												id="tax_{{$invoice->id}}_{{$key}}" value="true">
										</label>
										{{-- <label for="city" class="col-sm-3 col-form-label">City</label>
										<div class="col-sm-9">
											<input type="text" name="city" class="form-control" id="city"
												placeholder="city" value="{{ $invoice->city }}" required>
										</div> --}}
									</div>
								</div>
								@endforeach
								{{-- <div class="form-group row">
									<div class="col-sm-3">
										<input type="text" name="service" class="form-control"
											id="service_{{$invoice->id}}" placeholder="Service">
									</div>
									<div class="col-sm-6">
										<input type="text" name="description" class="form-control"
											id="service_description_{{$invoice->id}}" placeholder="Description">
									</div>
									<div class="col-sm-2">
										<input type="number" name="price" class="form-control"
											id="price_{{$invoice->id}}" placeholder="Price">
									</div>
									<div class="col-sm-1">
										<a class="form-control-plaintext" id="serviceAdd_{{$invoice->id}}">
											<i class="fas fa-plus-circle"></i>
										</a>
									</div>
								</div>
								@foreach(json_decode($invoice->description) as $key => $value)
								<div class="form-group row">
									<div class="col-sm-3">
										<input type="text" class="form-control-plaintext"
											id="service_{{$invoice->id}}_{{$key}}"
											name="service_{{$invoice->id}}_{{$key}}" placeholder="Service"
											value="{{$value->service}}">
									</div>
									<div class="col-sm-6">
										<input type="text" class="form-control-plaintext"
											id="service_description_{{$invoice->id}}_{{$key}}"
											name="service_description_{{$invoice->id}}_{{$key}}"
											placeholder="Description" value="{{$value->description}}">
									</div>
									<div class="col-sm-2">
										<input type="number" class="form-control-plaintext"
											id="price_{{$invoice->id}}_{{$key}}" name="price_{{$invoice->id}}_{{$key}}"
											placeholder="Price" value="{{$value->price}}">
									</div>
									<div class="col-sm-1">
										<a class="form-control-plaintext serviceDelete_{{$invoice->id}}_{{$key}}">
											<i class="fas fa-minus-circle"></i>
										</a>
									</div>
								</div>
								@endforeach --}}

							</div>
						</div>
					</div>
					<div>
						<button type="submit" class="btn btn-info">Save</button>
						<button type="button" class="btn btn-secondary float-right" data-dismiss="modal">
							Close
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

{{-- <script type="text/javascript">
	$(document).ready(function() {
		$("#serviceAdd_{{$invoice->id}}").click(function() {
			let service = $("#service_{{$invoice->id}}").val();
			console.log(service);
			let description = $("#service_description_{{$invoice->id}}").val();
			console.log(description);
			let price = $('#price_{{$invoice->id}}').val();
			console.log(price);
		});
	});
</script> --}}