@extends('templates.super-admin.layout')

@section('head')
<title>Invoices</title>
<meta name="description" content="All Invoices">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Invoices</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Invoices</li>
				</ol>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Facturas Carlos Cueva - {{ $month }} {{date('Y')}}</h3>
						<br>
						@include('super-admin.invoice.create-modal')
					</div>
					<!-- /.card-header -->
					@if (isset($allMonths))
					<div class="row">
						@foreach ($allMonths as $key => $value)
						@if ($month == $value)
						@break;
						@endif
						<a href="{{ route('download-all-invoices', $key) }}" type="button"
							class="btn btn-outline-success btn-sm" style="margin: 5px;">
							<i class="fa fa-download"></i> {{ $value }} {{date('Y')}}
						</a>
						<form method="POST" action="{{ route('sent-all-invoices', $key) }}">
							@csrf
							<button type="submit" class="btn btn-outline-success btn-sm" style="margin: 5px;">
								<i class="fa fa-envelope"></i> {{ $value }} {{date('Y')}}
							</button>
						</form>
						@endforeach
					</div>
					@endif
					<div class="card-body">
						<table id="example1" class="table table-bordered  table-striped">
							<thead>
								<tr>
									<th style="width: 10px">#</th>
									<th>Dirección</th>
									<th>Estado</th>
									<th>Servicios</th>
									<th>Con IVA</th>
								</tr>
							</thead>
							<tbody>
								@php
								$total = 0;
								@endphp
								@foreach ($invoices as $key => $invoice)
								<tr>
									<td>{{$key+1}}</td>
									<td>
										@if ($invoice->status)
										<span class="text-primary">
											@else
											<span class="text-secondary">
												@endif
												{{$invoice->address}}
											</span>
											<br>
											<a href="{{route('download-invoice',[$invoice->nif, $date['year'], $date['month']])}}"
												target="_blank" class="btn btn-info mt-1">
												<i class="fa fa-download"></i>
											</a>
											<a href="{{route('show-invoice',[$invoice->nif, $date['year'], $date['month']])}}"
												target="_blank" class="btn btn-info mt-1">
												<i class="far fa-file-pdf"></i>
											</a>
											@include('super-admin.invoice.edit-modal', $invoice)
											<form method="post"
												onclick="return confirm('Do you want delete this item? Are you sure?')"
												action="{{ route('invoice.destroy', $invoice) }}"
												style="position: absolute;display: contents;">
												@csrf
												@method('DELETE')
												<button type="submit" class="btn btn-danger mt-1">
													<i class="far fa-trash-alt"></i>
												</button>
											</form>
									</td>
									<td>
										@if ($invoice->status)
										Activa
										@else
										Inactiva
										@endif
									</td>
									<td>
										<ul>
											@foreach (json_decode($invoice->description, true) as $key => $value)
											<li>
												{{ $value['service'] }}: {{ $value['description'] }} | {{
												number_format($value['price'], 2, ',', '') }}€
											</li>
											<br>
											@endforeach
										</ul>
									</td>
									<td>
										<ul>
											@foreach (json_decode($invoice->description, true) as $key => $value)
											<li>
												<span>
													{{ number_format($value['price']*1.21,2, ',','') }}€
												</span>
											</li>
											@endforeach
										</ul>
									</td>
								</tr>
								@php
								if ($invoice->status) {
								$data = json_decode($invoice->description, true);
								foreach ($data as $key => $value) {
								$total += $value['price'];
								}
								}
								@endphp
								@endforeach

							</tbody>
							<tfoot>
								<tr>
									<th style="width: 10px">#</th>
									<th>TOTAL</th>
									<th>
									</th>
									<th>{{ number_format($total, 2, '.', '') }} € - {{ number_format($total*1.21, 2,
										'.', '') }} €</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection


@section('script')
<!-- jQuery -->
<script src="{{ asset('admin-lte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('admin-lte/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('admin-lte/dist/js/demo.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('admin-lte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
	$(function () {
			$("#example1").DataTable();
			$('#example2').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			});
		});
</script>
@endsection