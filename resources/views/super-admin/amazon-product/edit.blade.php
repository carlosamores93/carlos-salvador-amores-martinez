@extends('templates.super-admin.layout')

@section('head')
	<title>Edit amazon product</title>
	<meta name="description" content="Edit amazon product">
	<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
@endsection


@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit amazon product</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit amazon product</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
			<div class="col-md-12">
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">Edit amazon product</h3>
					</div>
					<!-- /.card-header -->
					<!-- form start -->
					<form method="POST" class="form-horizontal"
                        action="{{ route('amazon-product.update', $amazonProduct) }}" enctype="multipart/form-data">
                        @csrf
						@method('PUT')

						<div class="row">
							<div class="col-md-12">
								<div class="card-body">
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Name</label>
										<div class="col-sm-9">
											<input type="text" name="description" class="form-control"
												id="inputEmail3" placeholder="description" 
												value="{{ $amazonProduct->description }}" required>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Subject</label>
										<div class="col-sm-9">
											<textarea class="form-control" name='content'
												rows="5" required>{{ $amazonProduct->content }}</textarea>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Status</label>
										<div class="col-sm-9">
											<div class="custom-control custom-radio">
												<input type="radio" class="form-control custom-control-input"
												id="customControlValidation1" name="status"
												value="1" @if ($amazonProduct->status === 1) checked  @endif required>
												<label class="custom-control-label" for="customControlValidation1">Active</label>
											</div>
											<div class="custom-control custom-radio">
												<input type="radio" class="form-control custom-control-input"
												id="customControlValidation2" name="status"
												value="0" @if ($amazonProduct->status === 0) checked  @endif required>
												<label class="custom-control-label" for="customControlValidation2">Inactive</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						

						
						<!-- /.card-body -->
						<div class="card-footer">
							<button type="submit" class="btn btn-info">Save</button>
							<a href="{{ route('amazon-product.index') }}"
							type="submit" class="btn btn-default float-right">Cancel</a>
						</div>
						<!-- /.card-footer -->
					</form>
				</div>
			</div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection


@section('script')
	<!-- jQuery -->
	<script src="{{ asset('admin-lte/plugins/jquery/jquery.min.js') }}"></script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- bs-custom-file-input -->
	<script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('admin-lte/dist/js/adminlte.min.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('admin-lte/dist/js/demo.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			bsCustomFileInput.init();
		});
	</script>
	<script>
			CKEDITOR.replace( 'editor1' );
	</script>
@endsection