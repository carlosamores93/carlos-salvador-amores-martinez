@extends('templates.super-admin.layout')

@section('head')
	<title>Amazon products</title>
	<meta name="description" content="All amazon-products">
	<!-- DataTables -->
	<link rel="stylesheet"
		href="{{ asset('admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection


@section('content')

	<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All amazon products</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">All amazon products</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
	</section>

	<section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
			  <h3 class="card-title">
					List all amazon products
					<a href="{{ route('amazon-product.create') }}"
						type="button" class="btn btn-block btn-outline-primary">
						Create product
					</a>
				</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
					<tr>
						{{-- <th>Id</th> --}}
						<th>Description</th>
						<th>Content</th>
						{{-- <th>Status</th> --}}
						{{-- <th>Actiones</th> --}}
					</tr>
                </thead>
                <tbody>
					@foreach ($amazonProducts as $amazonProduct)	
						<tr>
							{{-- <td>{{$amazonProduct->id}}</td> --}}
							<td>
								Id: {{$amazonProduct->id}}
								<br> 
								@if (isset($amazonProduct->deleted_at))
									{{$amazonProduct->description}}
								@else
									<a href="{{ route('amazon-product.edit',$amazonProduct) }}">
										{{$amazonProduct->description}}
									</a>
								@endif
								<br>
								@if (isset($amazonProduct->deleted_at))
									<span class="badge bg-danger">
										Deleted
									</span>
								@else	
									<span class="badge bg-{{$amazonProduct->status ? 'success' : 'warning'}}">
										{{$amazonProduct->status ? 'Active' : 'Inactive'}}
									</span>
								@endif
								<br>
								@if (!isset($amazonProduct->deleted_at))
									<form method="POST"
									onclick="return confirm('Do you want delete this item? Are you sure?')"
									action="{{ route('amazon-product.destroy', $amazonProduct) }}">
										@csrf
										@method('DELETE')
										<button type="submit" class="btn btn-danger">Delete</button>
									</form>
								@endif

							</td>
							<td>{!!$amazonProduct->content!!}</td>
							{{-- <td>	
								@if (isset($amazonProduct->deleted_at))
									<span class="badge bg-danger">
										Deleted
									</span>
								@else	
									<span class="badge bg-{{$amazonProduct->status ? 'success' : 'warning'}}">
										{{$amazonProduct->status ? 'Active' : 'Inactive'}}
									</span>
								@endif
							</td> --}}
							{{-- <td>
								@if (!isset($amazonProduct->deleted_at))	
									<a class="btn btn-success" href="{{ route('amazon-product.edit',$amazonProduct) }}">
										Edit
									</a>
									<form method="POST"
									onclick="return confirm('Do you want delete this item? Are you sure?')"
									action="{{ route('amazon-product.destroy', $amazonProduct) }}">
										@csrf
										@method('DELETE')
										<button type="submit" class="btn btn-danger">Delete</button>
									</form>
								@endif
							</td> --}}
						</tr>                
					@endforeach
                </tbody>
                <tfoot>
					<tr>
						{{-- <th>Id</th> --}}
						<th>Description</th>
						<th>Content</th>
						{{-- <th>Status</th> --}}
						{{-- <th>Actiones</th> --}}
					</tr>
                </tfoot>
			  </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
	
@endsection


@section('script')
	<!-- jQuery -->
	<script src="{{ asset('admin-lte/plugins/jquery/jquery.min.js') }}"></script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- DataTables -->
	<script src="{{ asset('admin-lte/plugins/datatables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('admin-lte/dist/js/adminlte.min.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('admin-lte/dist/js/demo.js') }}"></script>
	<!-- page script -->
	<script>
	$(function () {
		$("#example1").DataTable({
			"order": [[ 0, "desc" ]]
		});
		$('#example2').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"autoWidth": false,
		});
	});
	</script>
@endsection