@extends('templates.super-admin.layout')

@section('head')
	<title>Create user</title>
	<meta name="description" content="Create user">
	<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
@endsection


@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create user</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Create user</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
			<div class="col-md-12">
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">Create user</h3>
					</div>
					<!-- /.card-header -->
					<!-- form start -->
					<form method="POST" class="form-horizontal"
                        action="{{ route('user.store') }}" enctype="multipart/form-data">
                        @csrf
						@method('POST')

						<div class="row">
							<div class="col-md-6">
								<div class="card-body">
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Name</label>
										<div class="col-sm-9">
											<input type="text" name="name" class="form-control"
												id="inputEmail3" placeholder="Name" required>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Lastname</label>
										<div class="col-sm-9">
											<input type="text" name="lastname" class="form-control"
												id="inputEmail3" placeholder="Lastname">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Username</label>
										<div class="col-sm-9">
											<input type="text" name="username" class="form-control"
												id="inputEmail3" placeholder="Username">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Email</label>
										<div class="col-sm-9">
											<input type="email" name="email" class="form-control"
												id="inputEmail3" placeholder="Email" required>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Phone</label>
										<div class="col-sm-9">
											<input type="number" name="phone" class="form-control"
												id="inputEmail3" placeholder="Phone" required>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-3 col-form-label">Password</label>
										<div class="col-sm-9">
										<input type="password" name="password" class="form-control"
											id="inputPassword3" placeholder="Password" required>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-3 col-form-label">Career</label>
										<div class="col-sm-9">
										<input type="text" name="career" class="form-control"
											id="inputPassword3" placeholder="Career">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-3 col-form-label">Profession</label>
										<div class="col-sm-9">
										<input type="text" name="profession" class="form-control"
											id="inputPassword3" placeholder="Profession">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-3 col-form-label">University</label>
										<div class="col-sm-9">
										<input type="text" name="university" class="form-control"
											id="inputPassword3" placeholder="University">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card-body">

									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-3 col-form-label">Faculty</label>
										<div class="col-sm-9">
										<input type="text" name="faculty" class="form-control"
											id="inputPassword3" placeholder="Faculty">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-3 col-form-label">Address</label>
										<div class="col-sm-9">
										<input type="text" name="address" class="form-control"
											id="inputPassword3" placeholder="Address">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-3 col-form-label">Gitlab</label>
										<div class="col-sm-9">
										<input type="text" name="gitlab" class="form-control"
											id="inputPassword3" placeholder="Gitlab">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-3 col-form-label">Github</label>
										<div class="col-sm-9">
										<input type="text" name="github" class="form-control"
											id="inputPassword3" placeholder="Github">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-3 col-form-label">Linkedin</label>
										<div class="col-sm-9">
										<input type="text" name="linkedin" class="form-control"
											id="inputPassword3" placeholder="Linkedin">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-3 col-form-label">Website</label>
										<div class="col-sm-9">
											<input type="text" name="website" class="form-control"
											id="inputPassword3" placeholder="Website">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-3 col-form-label">Photo</label>
										<div class="col-sm-9 custom-file">
											<input type="file" name="image"
												class="custom-file-input" id="exampleInputFile">
											<label class="custom-file-label" for="exampleInputFile">Choose file</label>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-3 col-form-label">CV</label>
										<div class="col-sm-9 custom-file">
											<input type="file" name="cv"
												class="custom-file-input" id="exampleInputFile">
											<label class="custom-file-label" for="exampleInputFile">Choose file</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="card-body">
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-12 col-form-label">Description</label>
										<div class="col-sm-12">
											<textarea name="editor1"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>



						<!-- /.card-body -->
						<div class="card-footer">
							<button type="submit" class="btn btn-info">Save</button>
							<a href="{{ route('user.index') }}"
							type="submit" class="btn btn-default float-right">Cancel</a>
						</div>
						<!-- /.card-footer -->
					</form>
				</div>
			</div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection


@section('script')
	<!-- jQuery -->
	<script src="{{ asset('admin-lte/plugins/jquery/jquery.min.js') }}"></script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- bs-custom-file-input -->
	<script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('admin-lte/dist/js/adminlte.min.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('admin-lte/dist/js/demo.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			bsCustomFileInput.init();
		});
	</script>
	<script>
		CKEDITOR.replace( 'editor1', {
			language: 'es',
			uiColor: '#9AB8F3',
			width: '100%',
			height: 500
		});
	</script>
@endsection