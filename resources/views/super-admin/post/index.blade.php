@extends('templates.super-admin.layout')

@section('head')
	<title>Posts</title>
	<meta name="description" content="All posts">
	<!-- DataTables -->
	<link rel="stylesheet"
		href="{{ asset('admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection


@section('content')

	<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All posts</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">All posts</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
	</section>

	<section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
			  <h3 class="card-title">
					List all posts
					<a href="{{ route('post.create') }}"
						type="button" class="btn btn-block btn-outline-primary">
						Create post
					</a>
				</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
					<tr>
						<th>Id</th>
						<th>Title</th>
						<th>Subtitle</th>
						<th>Status</th>
						<th>Published</th>
						<th>Actiones</th>
					</tr>
                </thead>
                <tbody>
					@foreach ($posts as $post)	
						<tr>
							<td>{{$post->id}}</td>
							<td>{{$post->title}}</td>
							<td>{{$post->subtitle}}</td>
							<td>
								{{$post->status->name}}
							</td>
							<td>
								@if ((bool)$post->published)
									<span class="badge bg-success">Yes</span>
									@else
									<span class="badge bg-warning">No</span>
								@endif
							</td>
							<td>
								<a class="btn btn-success" href="{{ route('post.edit',$post) }}">
									Edit
								</a>
								<form method="POST"
								onclick="return confirm('Do you want delete this item? Are you sure?')"
								action="{{ route('post.destroy', $post) }}">
									@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-danger">Delete</button>
								</form>
							</td>
						</tr>                
					@endforeach
                </tbody>
                <tfoot>
					<tr>
						<th>Id</th>
						<th>Title</th>
						<th>Subtitle</th>
						<th>Status</th>
						<th>Published</th>
						<th>Actiones</th>
					</tr>
                </tfoot>
			  </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
	
@endsection


@section('script')
	<!-- jQuery -->
	<script src="{{ asset('admin-lte/plugins/jquery/jquery.min.js') }}"></script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- DataTables -->
	<script src="{{ asset('admin-lte/plugins/datatables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('admin-lte/dist/js/adminlte.min.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('admin-lte/dist/js/demo.js') }}"></script>
	<!-- page script -->
	<script>
	$(function () {
		$("#example1").DataTable();
		$('#example2').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": false,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		});
	});
	</script>
@endsection