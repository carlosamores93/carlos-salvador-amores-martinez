@extends('templates.super-admin.layout')

@section('head')
	<title>Create post</title>
	<meta name="description" content="Create post">
	<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
@endsection


@section('content')

	@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create post</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Create post</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
	</section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
			<div class="col-md-12">
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">Create post</h3>
					</div>
					<!-- /.card-header -->
					<!-- form start -->
					<form method="POST" class="form-horizontal"
                        action="{{ route('post.store') }}" enctype="multipart/form-data">
                        @csrf
						@method('POST')

						<div class="row">
							<div class="col-md-12">
								<div class="card-body">
									<div class="form-group row">
										<label for="title" class="col-sm-3 col-form-label">Title</label>
										<div class="col-sm-9">
											<input type="text" name="title" class="form-control @error('title') is-invalid @enderror"
												id="title" placeholder="Title" required>
											@error('title')
												<div class="alert alert-danger">{{ $message }}</div>
											@enderror
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Subtitle</label>
										<div class="col-sm-9">
											<input type="text" name="subtitle" class="form-control"
												id="inputEmail3" placeholder="Subtitle" required>
										</div>
									</div>

									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Status</label>
										<div class="col-sm-9">
											<select name="status_id" id="" class="form-control" required>
												<option value="">Select status</option>
												@foreach (\Carlos\Amores\Models\Status::all() as $item)
												<option value="{{$item->id}}">{{$item->name}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Post type</label>
										<div class="col-sm-9">
											<select name="post_type_id" id="" class="form-control" required>
												<option value="">Select post type</option>
												@foreach (\Carlos\Amores\Models\PostType::all() as $item)
												<option value="{{$item->id}}">{{$item->name}}</option>
												@endforeach
											</select>
										</div>
									</div>
									{{-- <div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Published</label>
										<div class="col-sm-9">
											<input type="checkbox" name="published" id="" class="form-control">
										</div>
									</div> --}}
									<div class="form-group row">
										<label for="inputPassword3" class="col-sm-12 col-form-label">Description</label>
										<div class="col-sm-12">
											<textarea name="editor1"></textarea>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Photo</label>
										<div class="col-sm-9">
											<input type="file" name="photo" class="form-control">
											<label class="custom-file-label">Choose file</label>
										</div>
									</div>
								</div>
							</div>
						</div>



						<!-- /.card-body -->
						<div class="card-footer">
							<button type="submit" class="btn btn-info">Save</button>
							<a href="{{ route('post.index') }}"
							type="submit" class="btn btn-default float-right">Cancel</a>
						</div>
						<!-- /.card-footer -->
					</form>
				</div>
			</div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection


@section('script')
	<!-- jQuery -->
	<script src="{{ asset('admin-lte/plugins/jquery/jquery.min.js') }}"></script>
	<!-- Bootstrap 4 -->
	<script src="{{ asset('admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<!-- bs-custom-file-input -->
	<script src="{{ asset('admin-lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('admin-lte/dist/js/adminlte.min.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('admin-lte/dist/js/demo.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			bsCustomFileInput.init();
		});
	</script>
	<script>
		CKEDITOR.replace( 'editor1', {
			language: 'es',
			uiColor: '#9AB8F3',
			width: '100%',
			height: 500
		});
	</script>
@endsection