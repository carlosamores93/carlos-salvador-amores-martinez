<aside class="left-sidebar" data-sidebarbg="skin5">
    {{-- Sidebar scroll--}}
    <div class="scroll-sidebar">
        {{-- Sidebar navigation--}}
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">

				@if (Auth::user()->isSuperadmin())
					<li class="sidebar-item">
						<a class="sidebar-link waves-effect waves-dark sidebar-link"
							href="{{ route('super.admin.home') }}" aria-expanded="false">
							<i class="mdi mdi-view-dashboard"></i>
							<span class="hide-menu">Super Admin</span>
						</a>
					</li>
				@endif

                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link"
                        href="{{ route('cms-home') }}" aria-expanded="false">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
				</li>

                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link"
                    href="{{ route('user-edit', Auth::user()) }}" aria-expanded="false">
                        <i class="mdi mdi-face-profile"></i>
                        <span class="hide-menu">Profile</span>
                    </a>
				</li>

				@if (Auth::user()->isSuperadmin() || Auth::user()->isCarlosCueva())
					<li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('invoice.index') }}" aria-expanded="false">
                            <i class="mdi mdi-file-document"></i>
                            <span class="hide-menu">Facturas</span>
                        </a>
                    </li>
				@endif
				
				
                
                @if (Auth::user()->isSuperadmin())

                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark sidebar-link"
                        href="{{ route('user.index') }}" aria-expanded="false">
                            <i class="mdi mdi-table-large"></i>
                            <span class="hide-menu">Users</span>
                        </a>
                    </li>
                    
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark"
                            href="javascript:void(0)" aria-expanded="false">
                            <i class="mdi mdi-laptop"></i>
                            <span class="hide-menu">Works </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{ route('work.index') }}" class="sidebar-link">
                                    <i class="mdi mdi-note-outline"></i>
                                    <span class="hide-menu"> All works </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{ route('work.create') }}" class="sidebar-link">
                                    <i class="mdi mdi-note-plus"></i>
                                    <span class="hide-menu"> New work </span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="mdi mdi-bookmark-plus"></i>
                            <span class="hide-menu">Skills </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{ route('skill.index') }}" class="sidebar-link">
                                    <i class="mdi mdi-note-outline"></i>
                                    <span class="hide-menu"> All skills </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{ route('skill.create') }}" class="sidebar-link">
                                    <i class="mdi mdi-note-plus"></i>
                                    <span class="hide-menu"> New skills </span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="mdi mdi-bookmark-plus"></i>
                            <span class="hide-menu">Miniskills </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{ route('miniskill.index') }}" class="sidebar-link">
                                    <i class="mdi mdi-note-outline"></i>
                                    <span class="hide-menu"> All miniskills </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{ route('miniskill.create') }}" class="sidebar-link">
                                    <i class="mdi mdi-note-plus"></i>
                                    <span class="hide-menu"> New miniskills </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif

                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link"
                    href="{{ route('car.index') }}" aria-expanded="false">
                        <i class="mdi mdi-table-large"></i>
                        <span class="hide-menu">Cars</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link"
                    href="{{ route('encryption.index') }}" aria-expanded="false">
                        <i class="mdi mdi-table-large"></i>
                        <span class="hide-menu">Encryption</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link"
                    href="{{ route('articles.index') }}" aria-expanded="false">
                        <i class="mdi mdi-table-large"></i>
                        <span class="hide-menu">Article</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('home') }}" aria-expanded="false">
                        <i class="mdi mdi-google-chrome"></i>
                        <span class="hide-menu">Go to home</span>
                    </a>
                </li>


            </ul>
        </nav>
        {{-- End Sidebar navigation --}}
    </div>
    {{-- End Sidebar scroll--}}
</aside>