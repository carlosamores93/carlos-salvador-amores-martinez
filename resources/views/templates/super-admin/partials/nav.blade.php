<nav class="main-header navbar navbar-expand navbar-white navbar-light">
	<!-- Left navbar links -->
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
		</li>
		<li class="nav-item d-none d-sm-inline-block">
			<a href="{{route('super.admin.home')}}" class="nav-link">
				Superadmin Dashboard
			</a>
		</li>
		<li class="nav-item d-none d-sm-inline-block">
			<a href="{{route('cms-home')}}" class="nav-link">
				Admin Dashboard
			</a>
		</li>
	</ul>

	<!-- Right navbar links -->
	<ul class="navbar-nav ml-auto">
		
		<li class="nav-item">
			<a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
			<i class="fas fa-th-large"></i>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link"
				href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
				<i class="fas fa-sign-out-alt"></i>
			</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
			</form>
		</li>
	</ul>
</nav>