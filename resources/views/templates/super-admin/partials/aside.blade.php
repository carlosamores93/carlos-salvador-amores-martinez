<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<!-- Brand Logo -->
	<a href="{{ route('super.admin.home')}}" class="brand-link">
		<img src="{{ asset('img/logo-icon.png') }}"
			alt="CSAM Logo" class="brand-image img-circle elevation-3"
			style="opacity: .8">
		<span class="brand-text font-weight-light">
			{{-- C S A M --}}
			<img src="{{ asset('admin-lte/dist/img/carlosamores.png') }}"
			alt="homepage" class="light-logo" style="width: 180px;"/>
		</span>
	</a>

	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar user panel (optional) -->
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image">
				<img src="{{asset(Auth::user()->image)}}"
				class="img-circle elevation-2" alt="{{ Auth::user()->full_name }}">
				</div>
				<div class="info">
				<a href="{{ route('user.edit',Auth::user()) }}"
					class="d-block">{{ Auth::user()->name }}</a>
			</div>
		</div>

		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
			<!-- Add icons to the links using the .nav-icon class
				with font-awesome or any other icon font library -->
				<li class="nav-item">
					<a href="{{route('user.index')}}" class="nav-link">
					<i class="nav-icon fas fa-users"></i>
						<p>Users</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{route('amazon-product.index')}}" class="nav-link">
					<i class="nav-icon fas fa-ad"></i>
						<p>Amazon products</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{route('simulation')}}" class="nav-link">
					<i class="nav-icon fas fa-home"></i>
						<p>Simulation</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{route('houses.index')}}" class="nav-link">
					<i class="nav-icon fas fa-home"></i>
						<p>Houses</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{route('notice.index')}}" class="nav-link">
					<i class="nav-icon fas fa-bell"></i>
						<p>Notices</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{route('invoice.index')}}" class="nav-link">
					<i class="nav-icon fas fa-file-invoice"></i>
						<p>Invoices</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{route('post.index')}}" class="nav-link">
					<i class="nav-icon fas fa-blog"></i>
						<p>Blog</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{route('cms-home')}}" class="nav-link">
					<i class="nav-icon fas fa-tachometer-alt"></i>
						<p>Admin</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{route('home')}}" class="nav-link">
					<i class="nav-icon fas fa-home"></i>
						<p>Home</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="https://adminlte.io/docs/3.0"
						class="nav-link" target="_blank">
					<i class="nav-icon fas fa-file"></i>
					<p>Documentation</p>
					</a>
				</li>

				<li class="nav-item">
					<a class="nav-link"
						href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
						<i class="nav-icon fas fa-sign-out-alt"></i>
						<p>Logout</p>
					</a>
					<form id="logout-form" action="{{ route('logout') }}"
						method="POST" style="display: none;">
						@csrf
					</form>
				</li>


			</ul>
		</nav>
	<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>