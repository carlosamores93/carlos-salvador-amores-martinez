<!DOCTYPE html>
<html lang="en">
	<head>
		@yield('head')
		@include('templates.miniblog.partials.head')
	</head>
	<body>
		<div class="site-wrap">
			@include('templates.miniblog.partials.navbar')
			@yield('content')
			@include('templates.miniblog.partials.footer')
		</div>
		@include('templates.miniblog.partials.scripts')
  	</body>
</html>