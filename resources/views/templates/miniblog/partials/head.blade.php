<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
{{-- Favicons --}}
<link href="{{ asset('img/favicon.ico') }}" rel="icon">
<link href="{{ asset('img/favicon.ico') }}" rel="apple-touch-icon">
<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700|Playfair+Display:400,700,900" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('miniblog/fonts/icomoon/style.css') }}">
<link rel="stylesheet" href="{{ asset('miniblog/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('miniblog/css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('miniblog/css/jquery-ui.css') }}">
<link rel="stylesheet" href="{{ asset('miniblog/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('miniblog/css/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('miniblog/css/bootstrap-datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('miniblog/fonts/flaticon/font/flaticon.css') }}">
<link rel="stylesheet" href="{{ asset('miniblog/css/aos.css') }}">
<link rel="stylesheet" href="{{ asset('miniblog/css/style.css') }}">