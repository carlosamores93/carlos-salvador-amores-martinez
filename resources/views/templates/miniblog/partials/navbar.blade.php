<div class="site-mobile-menu">
	<div class="site-mobile-menu-header">
		<div class="site-mobile-menu-close mt-3">
			<span class="icon-close2 js-menu-toggle"></span>
		</div>
	</div>
	<div class="site-mobile-menu-body"></div>
</div>

<header class="site-navbar" role="banner">
	<div class="container-fluid">
		<div class="row align-items-center">

			{{-- <div class="col-12 search-form-wrap js-search-form">
				<form method="get" action="#">
					<input type="text" id="s" class="form-control" placeholder="Search...">
					<button class="search-btn" type="submit"><span class="icon-search"></span></button>
				</form>
			</div> --}}

			<div class="col-4 site-logo">
				<a href="{{route('home')}}" class="text-black h2 mb-0">Home</a>
			</div>

			<div class="col-8 text-right">
				<nav class="site-navigation" role="navigation">
					<ul class="site-menu js-clone-nav mr-auto d-none d-lg-block mb-0">
						<li><a href="{{route('home')}}">Home</a></li>
						<li><a href="{{route('blog')}}">Blog</a></li>
						@guest
						<li class="nav-item">
							<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
						</li>
						@if (Route::has('register') && env('APP_ENV') === 'local')
						<li class="nav-item">
							<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
						</li>
						@endif
						<li class="nav-item">
							<a class="nav-link" href="{{ route('curriculum') }}">
								Curriculum
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{ route('l5-swagger.default.api') }}" target="_blank">
								API Doc
							</a>
						</li>
						@else
						<li class="nav-item dropdown">
							<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
								data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
								{{ Auth::user()->full_name }} <span class="caret"></span>
							</a>

							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

								@if (Auth::user()->isSuperadmin())
								<a class="dropdown-item" href="{{ route('super.admin.home') }}">
									Super Admin
								</a>
								@endif

								<a class="dropdown-item" href="{{ route('cms-home') }}">
									Admin
								</a>

								<a class="dropdown-item" href="{{ route('curriculum') }}">
									Curriculum
								</a>

								<a class="dropdown-item" href="{{ route('blog') }}">
									Blog
								</a>

								<a class="dropdown-item" href="{{ route('l5-swagger.default.api') }}" target="_blank">
									API Documentation
								</a>


								<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
												document.getElementById('logout-form').submit();">
									{{ __('Logout') }}
								</a>

								<form id="logout-form" action="{{ route('logout') }}" method="POST"
									style="display: none;">
									@csrf
								</form>


							</div>
						</li>
						@endguest



						{{-- <li><a href="category.html">Tech</a></li>
						<li class="d-none d-lg-inline-block">
							<a href="#" class="js-search-toggle">
								<span class="icon-search"></span>
							</a>
						</li> --}}
					</ul>
				</nav>
				<a href="#" class="site-menu-toggle js-menu-toggle text-black d-inline-block d-lg-none">
					<span class="icon-menu h3"></span>
				</a>
			</div>
		</div>

	</div>
</header>