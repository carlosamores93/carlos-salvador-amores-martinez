@component('mail::message')
# {{$month}} Invoices

@if (isset($message))
{!! $message !!}

@else

Here you have yours {{$month}}'s invoices.

See you soon perris


😘 🤣
@endif

Thanks,<br>
{{ config('app.name') }}
@endcomponent