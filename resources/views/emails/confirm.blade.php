{{-- Hola {{$user->name}}

Has cambiado tu correo electronico. Por favor verificala el nuevo email usando el siguiente enlace:

{{ route('verify', $user->verification_token)}} --}}


@component('mail::message')
# Confirm verification

Hola {{$user->name}}

Has cambiado tu correo electronico. Por favor verificala el nuevo email usando el siguiente enlace:

@component('mail::button', ['url' => route('verify', $user->verification_token)])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
