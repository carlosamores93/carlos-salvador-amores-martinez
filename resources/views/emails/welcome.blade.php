{{-- Hola {{$user->name}}

Gracias por crear una cuenta. Por favor verificala usando el siguiente enlace:

{{ route('verify', $user->verification_token)}} --}}


@component('mail::message')
# Email verification

Hola {{$user->name}}

Gracias por crear una cuenta. Por favor verificala usando el siguiente enlace:

@component('mail::button', ['url' => route('verify', $user->verification_token)])
verificar
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
