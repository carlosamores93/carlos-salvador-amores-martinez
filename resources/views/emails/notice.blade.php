@component('mail::message')
# Notice

<p>{!! $message !!}</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
