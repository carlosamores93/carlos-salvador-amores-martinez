@extends('templates.curriculum.layout')

@section('content')
	@include('client.curriculum-vitae.partials.nav_star')
	@include('client.curriculum-vitae.partials.intro')
	@include('client.curriculum-vitae.partials.about')
	@include('client.curriculum-vitae.partials.skills')
	@include('client.curriculum-vitae.partials.work')
	@include('client.curriculum-vitae.partials.contact')
@endsection