</html>

<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Carlos Amores - Curriculum Vitae</title>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="The Curriculum Vitae of Carlos Salvador Amores Martínez."/>
		<style>
			html,body,div,span,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,abbr,address,cite,code,del,dfn,em,img,ins,kbd,q,samp,small,strong,sub,sup,var,b,i,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,figcaption,figure,footer,header,hgroup,menu,nav,section,summary,time,mark,audio,video {
			border:0;
			font:inherit;
			font-size:100%;
			margin:0;
			padding:0;
			vertical-align:baseline;
			}

			article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section {
			display:block;
			}

			html, body {font-family: 'Lato', helvetica, arial, sans-serif; font-size: 16px; color: #222;}

			.clear {clear: both;}

			p {
				font-size: 1em;
				line-height: 1.4em;
				margin-bottom: 20px;
				color: #444;
			}

			#cv {
				width: 100%;
				max-width: 800px;
				margin: 30px auto;
			}

			.mainDetails {
				padding: 25px 35px;
				border-bottom: 2px solid #cf8a05;
				/* background: #ededed; */
			}

			#name h1 {
				font-size: 2.3em;
				font-weight: 700;
				font-family: 'Rokkitt', Helvetica, Arial, sans-serif;
				margin-bottom: -6px;
			}

			#name h2 {
				margin-top: 20px;
    			font-size: 1.5em;
				margin-left: 2px;
				font-family: 'Rokkitt', Helvetica, Arial, sans-serif;
			}

			#mainArea {
				padding: 0 40px;
			}

			#headshot {
				width: 12.5%;
				float: left;
				margin-right: 40px;
			}

			#headshot img {
				width: 100px;
				height: auto;
				-webkit-border-radius: 10px;
				border-radius: 10px;
			}

			#name {
				float: left;
			}

			#contactDetails {
				float: right;
				margin-top: 25px;
			}

			#contactDetails ul {
				list-style-type: none;
				font-size: 15px;
				margin-top: 2px;
			}

			#contactDetails ul li {
				margin-bottom: 4px;
				color: #444;
			}

			#contactDetails ul li span {
				color: #cf8a05;
			}

			#contactDetails ul li a, a[href^=tel] {
				color: #444;
				text-decoration: none;
				-webkit-transition: all .3s ease-in;
				-moz-transition: all .3s ease-in;
				-o-transition: all .3s ease-in;
				-ms-transition: all .3s ease-in;
				transition: all .3s ease-in;
			}

			#contactDetails ul li a:hover {
				color: #cf8a05;
			}


			section {
				border-top: 1px solid #dedede;
				padding: 20px 0 0;
			}

			section:first-child {
				border-top: 0;
			}

			section:last-child {
				padding: 20px 0 10px;
			}

			.sectionTitle {
				float: left;
				width: 25%;
			}

			.sectionContent {
				float: right;
				width: 100%;
				text-align: justify;
			}

			.sectionTitle h1 {
				font-family: 'Rokkitt', Helvetica, Arial, sans-serif;
				font-style: italic;
				font-size: 1.5em;
				color: #cf8a05;
			}

			.sectionContent h2 {
				font-family: 'Rokkitt', Helvetica, Arial, sans-serif;
				font-size: 1.5em;
				margin-bottom: -2px;
			}

			.sectionContent ul li strong {
				font-weight: 700;
			}

			.subDetails {
				font-size: 0.8em;
				font-style: italic;
				margin-bottom: 3px;
			}

			.keySkills {
				list-style-type: none;
				-moz-column-count:3;
				-webkit-column-count:3;
				column-count:3;
				margin-bottom: 20px;
				font-size: 1em;
				color: #444;
			}

			.keySkills ul li {
				margin-bottom: 3px;
			}

			@media all and (min-width: 602px) and (max-width: 800px) {
				/* #headshot {
					display: none;
				} */

				.keySkills {
				-moz-column-count:2;
				-webkit-column-count:2;
				column-count:2;
				}
			}

			@media all and (max-width: 601px) {
				#cv {
					width: 95%;
					margin: 10px auto;
					min-width: 280px;
				}

				/* #headshot {
					display: none;
				} */

				#name, #contactDetails {
					float: none;
					width: 100%;
					text-align: center;
				}

				.sectionTitle, .sectionContent {
					float: none;
					width: 100%;
				}

				.sectionTitle {
					margin-left: -2px;
					font-size: 1.25em;
				}

				.keySkills {
					-moz-column-count:2;
					-webkit-column-count:2;
					column-count:2;
				}
			}

			@media all and (max-width: 480px) {
				.mainDetails {
					padding: 15px 15px;
				}

				#headshot {
					display: none;
				}

				section {
					padding: 15px 0 0;
				}

				#mainArea {
					padding: 0 25px;
				}


				.keySkills {
				-moz-column-count:1;
				-webkit-column-count:1;
				column-count:1;
				}

				#name h1 {
					line-height: .8em;
					margin-bottom: 4px;
				}
			}

			@media print {
				#cv {
					width: 100%;
				}
			}

			@-webkit-keyframes reset {
				0% {
					opacity: 0;
				}
				100% {
					opacity: 0;
				}
			}

			@-webkit-keyframes fade-in {
				0% {
					opacity: 0;
				}
				40% {
					opacity: 0;
				}
				100% {
					opacity: 1;
				}
			}

			@-moz-keyframes reset {
				0% {
					opacity: 0;
				}
				100% {
					opacity: 0;
				}
			}

			@-moz-keyframes fade-in {
				0% {
					opacity: 0;
				}
				40% {
					opacity: 0;
				}
				100% {
					opacity: 1;
				}
			}

			@keyframes reset {
				0% {
					opacity: 0;
				}
				100% {
					opacity: 0;
				}
			}

			@keyframes fade-in {
				0% {
					opacity: 0;
				}
        	$this->parseStartEndDateWorks($works);
				40% {
					opacity: 0;
				}
				100% {
					opacity: 1;
				}
			}

			.instaFade {
				-webkit-animation-name: reset, fade-in;
				-webkit-animation-duration: 1.5s;
				-webkit-animation-timing-function: ease-in;

				-moz-animation-name: reset, fade-in;
				-moz-animation-duration: 1.5s;
				-moz-animation-timing-function: ease-in;

				animation-name: reset, fade-in;
				animation-duration: 1.5s;
				animation-timing-function: ease-in;
			}

			.quickFade {
				-webkit-animation-name: reset, fade-in;
				-webkit-animation-duration: 2.5s;
				-webkit-animation-timing-function: ease-in;

				-moz-animation-name: reset, fade-in;
				-moz-animation-duration: 2.5s;
				-moz-animation-timing-function: ease-in;

				animation-name: reset, fade-in;
				animation-duration: 2.5s;
				animation-timing-function: ease-in;
			}

			.delayOne {
				-webkit-animation-delay: 0, .5s;
				-moz-animation-delay: 0, .5s;
				animation-delay: 0, .5s;
			}

			.delayTwo {
				-webkit-animation-delay: 0, 1s;
				-moz-animation-delay: 0, 1s;
				animation-delay: 0, 1s;
			}

			.delayThree {
				-webkit-animation-delay: 0, 1.5s;
				-moz-animation-delay: 0, 1.5s;
				animation-delay: 0, 1.5s;
			}

			.delayFour {
				-webkit-animation-delay: 0, 2s;
				-moz-animation-delay: 0, 2s;
				animation-delay: 0, 2s;
			}

			.delayFive {
				-webkit-animation-delay: 0, 2.5s;
				-moz-animation-delay: 0, 2.5s;
				animation-delay: 0, 2.5s;
			}
		</style>
	</head>
	<body id="top">
		@if(isset($download))
		<div id="cv" class="instaFade" style="border: #cf8a05 solid 3px;">
		@else
		<div id="cv" class="instaFade">
		@endif
			<div class="mainDetails">
				<div id="headshot" class="quickFade">
					<img src="{{asset($carlos->image)}}" alt="{{$carlos->full_name}}" />
				</div>

				<div id="name">
					<h1 class="quickFade delayTwo">
						{{$carlos->name}} <br> {{$carlos->lastname}}
					</h1>
					<h2 class="quickFade delayThree">{{$carlos->profession}}</h2>
				</div>

				<div id="contactDetails" class="quickFade delayFour">
					<ul>
						<li> <span> Dirección: </span> Moratalaz, Madrid</li>
						<li> <span> Email: </span> <a href="mailto:amorescarlos93@hotmail.com"
							target="_blank">amorescarlos93@hotmail.com</a></li>
						<li> <span> Teléfono: </span> <a href="tel:628606093"
							target="_blank">628606093</a></li>
						<li> <span> Web: </span> <a href="http://carlosamores.hol.es"
							target="_blank">carlosamores.hol.es</a></li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>


			<div id="mainArea" class="quickFade delayFive">

				<section>
					<article>
						<div class="sectionContent">
							{!! $carlos->description !!}
							<p></p>
						</div>
					</article>
					<div class="clear"></div>
				</section>


				<section>
					<div class="sectionContent">
						@foreach ($works as $work)
							<article>
								<h2>{{$work->company}}</h2>
								<p class="subDetails">
									{{$work->job}}, {{$work->start_date}} - {{$work->end_date}}
								</p>
								{!! $work->description !!}
							</article>
						@endforeach
					</div>
					<div class="clear"></div>
				</section>


				@if(isset($download))
					<br>
					<p>
						<a href="{{route('download-cv')}}">
							Descargar versión 'lite' de mi Curriculum Vitae
						</a>
						<br>
						Para mayor información acerca de mi perfil:
						<a href="{{route('curriculum')}}" target="_blank">
							curriculum-vitae-online
						</a>
					</p>
				@endif

			</div>
		</div>
	</body>
</html>