@extends('templates.curriculum.layout')

@section('content')
	@include('client.bautizo.partials.nav_star')
    @include('client.bautizo.partials.intro')
    @include('client.bautizo.partials.about')
    @include('client.bautizo.partials.skills')
    @include('client.bautizo.partials.work')
    @include('client.bautizo.partials.contact')
@endsection