<div class="row">
	@foreach ($amazonProducts as $amazonProduct)
		<div class="col-md-6">
			<strong>
				{{$amazonProduct->description}}
			</strong>
			<br>
			{!!$amazonProduct->content!!}
		</div>
		<br>
		<br>
	@endforeach
</div>