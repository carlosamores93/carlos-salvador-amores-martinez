@extends('templates.miniblog.layout')


@section('head')
<title>CARLOS SALVADOR AMORES MARTINEZ</title>
<meta name="description" content="CARLOS SALVADOR AMORES MARTINEZ WEB PERSONAL/PERSONAL PAGE">
@endsection

@section('content')
<div class="site-section bg-light">
	<div class="container">
		<div class="row align-items-stretch retro-layout-2">
			<div class="col-md-4">
				<a href="{{ route('login') }}" + class="h-entry mb-30 v-height gradient"
					style="background-image: url('images/img_3.jpg');">
					<div class="text">
						<h2>LOGIN</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="{{ route('curriculum') }}" class="h-entry mb-30 v-height gradient"
					style="background-image: url('images/img_3.jpg');">

					<div class="text">
						<h2>Curriculum Carlos Salvador Amores Martinez</h2>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a href="{{route('blog')}}" class="h-entry v-height gradient"
					style="background-image: url('images/img_1.jpg');">
					<div class="text">
						<h2>Blog</h2>
					</div>
				</a>
			</div>
			{{-- {{ dd( asset('images/img_3.jpg') ); }} --}}
			<div class="col-md-4">
				<a href="{{ route('l5-swagger.default.api') }}" class="h-entry mb-30 v-height gradient"
					style="background-image: url('images/img_3.jpg');">
					<div class="text">
						<h2>API Doc</h2>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
@endsection