@extends('templates.curriculum.layout')

@section('content')
	@include('client.curriculum-default.partials.nav_star')
	@include('client.curriculum-default.partials.intro')
	@include('client.curriculum-default.partials.about')
	@include('client.curriculum-default.partials.skills')
	@include('client.curriculum-default.partials.work')
	@include('client.curriculum-default.partials.contact')
@endsection