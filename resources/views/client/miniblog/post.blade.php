@extends('templates.miniblog.layout')


@section('head')
	<title>{{$post->title}}</title>
    <meta name="description" content="{{$post->subtitle}}">
@endsection

@section('content')

	<div class="site-cover site-cover-sm same-height overlay single-page"
		style="background-image: url('{{$post->photo}}');">
      <div class="container">
        <div class="row same-height justify-content-center">
          <div class="col-md-12 col-lg-10">
            <div class="post-entry text-center">
              {{-- <span class="post-category text-white bg-success mb-3">Nature</span> --}}
              <h1 class="mb-4">{{$post->title}}</h1>
              <div class="post-meta align-items-center text-center">
				  <figure class="author-figure mb-0 mr-3 d-inline-block">
					  <img src="{{asset($post->user->image)}}" alt="{{$post->user->fullName}}" class="img-fluid">
					</figure>
                <span class="d-inline-block mt-1">
					{{$post->user->fullName}}
				</span>
                <span>&nbsp;-&nbsp;
					{{date_format(date_create($post->published_at), 'M Y')}}
				</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <section class="site-section py-lg">
      	<div class="container">
        	<div class="row blog-entries element-animate">
         	 	<div class="col-md-12 col-lg-12 main-content">
					<div class="post-content-body">
						{!!$post->description!!}
					</div>
          		</div>
        	</div>
      	</div>
    </section>
@endsection
