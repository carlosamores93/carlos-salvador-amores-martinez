@extends('templates.admin.layout')

@section('head')
	<title>CMS</title>
    <meta name="description" content="">
    <link href="{{ asset('cms/assets/libs/flot/css/float-chart.css') }}"rel="stylesheet">
    <link href="{{ asset('cms/dist/css/style.min.css') }}"rel="stylesheet">
@endsection


@section('content')
	<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex align-items-center">
                            <div>
                                <h1 class="card-title">Dasboard</h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-2 col-xlg-3">
                                <a href="{{ route('user-edit', Auth::user()) }}">
                                    <div class="card card-hover">
                                        <div class="box bg-cyan text-center">
                                            <h1 class="font-light text-white">
                                                <i class="mdi mdi-face-profile"></i>
                                            </h1>
                                            <h6 class="text-white">Profile</h6>
                                        </div>
                                    </div>
                                </a>
							</div>
							
							@if (Auth::user()->isSuperadmin() || Auth::user()->isCarlosCueva())
								<div class="col-md-6 col-lg-2 col-xlg-3">
									<a href="{{ route('invoice.index') }}">
										<div class="card card-hover">
											<div class="box bg-cyan text-center">
												<h1 class="font-light text-white">
													<i class="mdi mdi-file-document"></i>
												</h1>
												<h6 class="text-white">Facturas</h6>
											</div>
										</div>
									</a>
								</div>
							@endif
								

                            @if (Auth::user()->isSuperadmin())

                                <div class="col-md-6 col-lg-2 col-xlg-3">
                                    <a href="{{ route('user.index') }}">
                                        <div class="card card-hover">
                                            <div class="box bg-cyan text-center">
                                                <h1 class="font-light text-white">
                                                    <i class="mdi mdi-table-large"></i>
                                                </h1>
                                                <h6 class="text-white">Users</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-md-6 col-lg-2 col-xlg-3">
                                    <a href="{{ route('work.index') }}">
                                        <div class="card card-hover">
                                            <div class="box bg-cyan text-center">
                                                <h1 class="font-light text-white">
                                                    <i class="mdi mdi-laptop"></i>
                                                </h1>
                                                <h6 class="text-white">Works</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-md-6 col-lg-2 col-xlg-3">
                                    <a href="{{ route('skill.index') }}">
                                        <div class="card card-hover">
                                            <div class="box bg-cyan text-center">
                                                <h1 class="font-light text-white">
                                                    <i class="mdi mdi-bookmark-plus"></i>
                                                </h1>
                                                <h6 class="text-white">Skills</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-md-6 col-lg-2 col-xlg-3">
                                    <a href="{{ route('miniskill.index') }}">
                                        <div class="card card-hover">
                                            <div class="box bg-cyan text-center">
                                                <h1 class="font-light text-white">
                                                    <i class="mdi mdi-bookmark-plus"></i>
                                                </h1>
                                                <h6 class="text-white">Miniskills</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endif

                            <div class="col-md-6 col-lg-2 col-xlg-3">
                                <a href="{{ route('car.index') }}">
                                    <div class="card card-hover">
                                        <div class="box bg-cyan text-center">
                                            <h1 class="font-light text-white">
                                                <i class="mdi mdi-table-large"></i>
                                            </h1>
                                            <h6 class="text-white">Cars</h6>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-6 col-lg-2 col-xlg-3">
                                <a href="{{ route('encryption.index') }}">
                                    <div class="card card-hover">
                                        <div class="box bg-cyan text-center">
                                            <h1 class="font-light text-white">
                                                <i class="mdi mdi-table-large"></i>
                                            </h1>
                                            <h6 class="text-white">Encryption</h6>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-6 col-lg-2 col-xlg-3">
                                <a href="{{ route('articles.index') }}">
                                    <div class="card card-hover">
                                        <div class="box bg-cyan text-center">
                                            <h1 class="font-light text-white">
                                                <i class="mdi mdi-table-large"></i>
                                            </h1>
                                            <h6 class="text-white">Article</h6>
                                        </div>
                                    </div>
                                </a>
                            </div>


                            <div class="col-md-6 col-lg-2 col-xlg-3">
                                <a href="{{ route('home') }}">
                                    <div class="card card-hover">
                                        <div class="box bg-cyan text-center">
                                            <h1 class="font-light text-white">
                                                <i class="mdi mdi-google-chrome"></i>
                                            </h1>
                                            <h6 class="text-white">Home</h6>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
@endsection


@section('script')
	<script src="{{ asset('cms/assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('cms/assets/extra-libs/sparkline/sparkline.js') }}"></script>
    <script src="{{ asset('cms/dist/js/waves.js') }}"></script>
    <script src="{{ asset('cms/dist/js/sidebarmenu.js') }}"></script>
    <script src="{{ asset('cms/dist/js/custom.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/excanvas.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.crosshair.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('cms/dist/js/pages/chart/chart-page-init.js') }}"></script>
@endsection