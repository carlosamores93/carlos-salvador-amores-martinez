@extends('templates.admin.layout')

@section('head')
	<title>Article</title>
    <meta name="description" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{ asset('cms/assets/libs/flot/css/float-chart.css') }}"rel="stylesheet">
    <link href="{{ asset('cms/dist/css/style.min.css') }}"rel="stylesheet">
@endsection


@section('content')
    <div id="app" class="content">
    <!--La equita id debe ser app, como hemos visto en app.js-->
        <article-component></article-component>
        {{-- <example-component></example-component> --}}
        <!--Añadimos nuestro componente vuejs-->
    </div>
@endsection
            
            
@section('script')
    <script src="{{asset('js/app.js')}}"></script>
	<script src="{{ asset('cms/assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('cms/assets/extra-libs/sparkline/sparkline.js') }}"></script>
    <script src="{{ asset('cms/dist/js/waves.js') }}"></script>
    <script src="{{ asset('cms/dist/js/sidebarmenu.js') }}"></script>
    <script src="{{ asset('cms/dist/js/custom.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/excanvas.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.crosshair.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('cms/dist/js/pages/chart/chart-page-init.js') }}"></script>
@endsection