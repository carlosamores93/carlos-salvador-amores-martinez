@extends('templates.admin.layout')

@section('head')
	<title>Users</title>
    <meta name="description" content="">
    <link rel="stylesheet" type="text/css" href="{{ asset('cms/assets/extra-libs/multicheck/multicheck.css') }}">
    <link href="{{ asset('cms/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <link href="{{ asset('cms/dist/css/style.min.css') }}" rel="stylesheet">
@endsection


@section('content')
	<div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">All users</h2>
                        <a href="{{ route('user.create') }}"
                            type="submit" class="btn btn-info">
                            Create user
                        </a>
                        <div class="table-responsive">
                            <table id="user_datatble" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Lastname</th>
                                        <th>Email</th>
                                        <th>Username</th>
                                        <th>Phone</th>
                                        {{-- <th>Career</th>
                                        <th>Profession</th>
                                        <th>University</th>
                                        <th>Faculty</th>
                                        <th>Address</th>
                                        <th>Github</th>
                                        <th>Gitlab</th>
                                        <th>Linkedin</th>
                                        <th>Website</th>
                                        <th>Description</th>
                                        <th>Start date</th>
                                        <th>End date</th> --}}
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach ($users as $w)
	                                    <tr>
	                                        <td>{{ $w->id }}</td>
	                                        <td>{{ $w->name }}</td>
	                                        <td>{{ $w->lastname }}</td>
	                                        <td>{{ $w->email }}</td>
	                                        <td>{{ $w->username }}</td>
	                                        <td>{{ $w->phone }}</td>
	                                        {{-- <td>{{ $w->career }}</td>
	                                        <td>{{ $w->profession }}</td>
	                                        <td>{{ $w->university }}</td>
	                                        <td>{{ $w->faculty }}</td>
	                                        <td>{{ $w->address }}</td>
	                                        <td>{{ $w->github }}</td>
	                                        <td>{{ $w->gitlab }}</td>
	                                        <td>{{ $w->linkedin }}</td>
	                                        <td>{{ $w->website }}</td>
	                                        <td>{!! $w->description !!}</td> --}}
	                                        {{-- <td>{{ $w->created_at }}</td>
	                                        <td>{{ $w->updated_at }}</td> --}}
	                                        <td>
	                                        	<a class="btn btn-success" href="{{ route('user.edit',$w) }}">Edit</a>
                                                <form method="POST"
                                                onclick="return confirm('Do you want delete this item? Are you sure?')"
                                                action="{{ route('user.destroy', $w) }}">
													@csrf
													@method('DELETE')
	                                        		<button type="submit" class="btn btn-danger">Delete</button>
	                                        	</form>
	                                        </td>
	                                    </tr>
                                	@endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Lastname</th>
                                        <th>Email</th>
                                        <th>Username</th>
                                        <th>Phone</th>
                                        {{-- <th>Career</th>
                                        <th>Profession</th>
                                        <th>University</th>
                                        <th>Faculty</th>
                                        <th>Address</th>
                                        <th>Github</th>
                                        <th>Gitlab</th>
                                        <th>Linkedin</th>
                                        <th>Website</th>
                                        <th>Description</th>
                                        <th>Start date</th>
                                        <th>End date</th> --}}
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                        {{ $users->links() }}
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('script')
	<script src="{{ asset('cms/assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('cms/assets/extra-libs/sparkline/sparkline.js') }}"></script>
    <script src="{{ asset('cms/dist/js/waves.js') }}"></script>
    <script src="{{ asset('cms/dist/js/sidebarmenu.js') }}"></script>
    <script src="{{ asset('cms/dist/js/custom.min.js') }}"></script>
    <script src="{{ asset('cms/assets/extra-libs/multicheck/datatable-checkbox-init.js') }}"></script>
    <script src="{{ asset('cms/assets/extra-libs/multicheck/jquery.multicheck.js') }}"></script>
    <script src="{{ asset('cms/assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        $('#user_datatble').DataTable();
    </script>
@endsection