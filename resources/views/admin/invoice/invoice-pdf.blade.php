<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>{{strtoupper($invoice->address)}}</title>
	{{--
	<link rel="stylesheet" href="style.css" media="all" /> --}}
	<style>
		.clearfix:after {
			content: "";
			display: table;
			clear: both;
		}

		a {
			color: #5D6975;
			text-decoration: underline;
		}

		body {
			position: relative;
			width: 18cm;
			height: 29.7cm;
			margin: 0 auto;
			color: #001028;
			background: #FFFFFF;
			font-family: Arial, sans-serif;
			font-size: 12px;
			font-family: Arial;
		}

		header {
			padding: 10px 0;
			margin-bottom: 30px;
		}

		#logo {
			text-align: center;
			margin-bottom: 10px;
		}

		#logo img {
			width: 90px;
		}

		h1 {
			border-top: 1px solid #5D6975;
			border-bottom: 1px solid #5D6975;
			color: #5D6975;
			font-size: 2.4em;
			line-height: 1.4em;
			font-weight: normal;
			text-align: center;
			margin: 0 0 20px 0;
			background: url(dimension.png);
		}

		#project {
			float: left;
			font-size: 15px;
		}

		#project span {
			color: #5D6975;
			text-align: right;
			width: 85px;
			margin-right: 10px;
			display: inline-block;
			font-size: 15px;
		}

		#company {
			float: right;
			font-size: 15px;
			/* text-align: right; */
		}

		#project div,
		#company div {
			white-space: nowrap;
		}

		table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 20px;
		}

		table tr:nth-child(2n-1) td {
			background: #F5F5F5;
		}

		table th,
		table td {
			text-align: center;
		}

		table th {
			padding: 5px 20px;
			color: #5D6975;
			border-bottom: 1px solid #C1CED9;
			white-space: nowrap;
			font-weight: normal;
		}

		table .service,
		table .desc {
			text-align: left;
			font-size: 15px;
		}

		table .price {
			text-align: center;
			font-size: 15px;
		}

		table td {
			padding: 20px;
			text-align: right;
		}

		table td.service,
		table td.desc {
			vertical-align: top;
		}

		table td.unit,
		table td.qty,
		table td.total {
			/* font-size: 1.2em; */
			font-size: 15px;
		}

		table td.grand {
			border-top: 1px solid #5D6975;
			;
		}

		#notices .notice {
			color: #5D6975;
			font-size: 1.2em;
		}

		footer {
			color: #5D6975;
			width: 100%;
			height: 30px;
			position: absolute;
			bottom: 0;
			border-top: 1px solid #C1CED9;
			padding: 8px 0;
			text-align: center;
		}
	</style>

</head>

<body>
	<header class="clearfix">
		<br>
		<br>
		<br>
		<div id="logo">
			{{-- <img src="{{ asset('img/jardinero.jpg') }}"> --}}
		</div>
		<h1>
			@if(isset($invoice->number) && $invoice->number === 100 ||
			isset($invoice->number) && $invoice->number === 101)
			PRESUPUESTO
			@elseif (isset($invoice->number) && $invoice->number > 0)
			FACTURA - Número: {{$invoice->number}}
			@else
			FACTURA
			@endif
		</h1>
		@if(isset($invoice->download))
		@if(isset($invoice->number))
		<a
			href="{{route('download-invoice',[$invoice->nif, $invoice->year, $invoice->monthNumber, $invoice->number])}}">
			Download
		</a>
		@else
		<a href="{{route('download-invoice',[$invoice->nif, $invoice->year, $invoice->monthNumber])}}">
			Download
		</a>
		@endif
		@endif
		<br>
		<br>
		<br>
		<div id="company" class="clearfix">
			<div><strong>CARLOS ALBERTO CUEVA CUEVA</strong></div>
			<div><strong>50498995B</strong></div>
			<div>CALLE ENCOMIENDA DE PALACIOS #93<br /> 28030, MADRID</div>
			<div>630 133 268</div>
			<div>
				cuevacuevacarlosalberto9@gmail.com
			</div>
		</div>
		<div id="project">
			<div><span>CLIENTE</span>{{strtoupper($invoice->address)}}</div>
			<div><span>DIRECCIÓN</span>28030, MADRID</div>
			<div><span>NIF</span><strong>{{strtoupper($invoice->nif)}}</strong></div>
			@if($invoice->number !== 101)
			<div>
				<span>
					FECHA
				</span><strong>@if($invoice->nif === 'E78131323'){{ now()->format('d') }} DE
					@endif{{strtoupper($invoice->month)}} DE {{$invoice->year}}
				</strong>
			</div>
			@endif
		</div>
	</header>
	<br>
	<br>
	<main>
		<table>
			<thead>
				<tr>
					<th class="service">SERVICIO</th>
					<th class="desc">DESCRIPCIÓN</th>
					<th class="price">PRECIO</th>
					<th class="price">TOTAL</th>
				</tr>
			</thead>
			<tbody>
				@php
				$subtotal = 0;
				@endphp
				@foreach ($invoice->description as $item)
				@php
				$subtotal += $item['price'];
				@endphp
				<tr>
					<td class="service">{{$item['service']}}</td>
					<td class="desc">{{$item['description']}}</td>
					<td class="price">{{number_format($item['price'], 2, ',', '.')}} €</td>
					<td class="price">{{number_format($item['price'], 2, ',', '.')}} €</td>
				</tr>
				@endforeach
				<tr>
					<td colspan="3" style="font-size: 15px;">SUBTOTAL</td>
					<td class="price">{{number_format($subtotal, 2, ',', '.')}} €</td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 15px;">IVA 21%</td>
					@php
					$iva = $subtotal * 21 / 100;
					@endphp
					<td class="price">{{number_format($iva, 2, ',', '.')}} €</td>
				</tr>
				<tr>
					<td colspan="3" class="grand total" style="font-size: 18px;">
						<strong>TOTAL</strong>
					</td>
					<td class="grand total" style="font-size: 18px; text-align:center;">
						<strong>
							{{number_format($subtotal + $iva, 2, ',', '.')}} €
						</strong>
					</td>
				</tr>
			</tbody>
		</table>
		{{-- <div id="notices">
			<div>AVISO:</div>
			<div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
		</div> --}}
	</main>
	{{-- <footer>
		Invoice was created on a computer and is valid without the signature and seal.
	</footer> --}}
</body>

</html>