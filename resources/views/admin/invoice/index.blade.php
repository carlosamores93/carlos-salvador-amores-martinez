@extends('templates.admin.layout')

@section('head')
	<title>CMS</title>
    <meta name="description" content="">
    <link href="{{ asset('cms/assets/libs/flot/css/float-chart.css') }}"rel="stylesheet">
    <link href="{{ asset('cms/dist/css/style.min.css') }}"rel="stylesheet">
@endsection


@section('content')
	<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

				<div class="card">
					<div class="card-body">
						<h5 class="card-title m-b-0">Facturas Chaperra - {{ $month }} {{date('Y')}}</h5>
						@if (isset($allMonths))
					  @foreach ($allMonths as $key => $value)
					  	@if ($month == $value)
							@break;
						@endif
						<a href="{{ route('download-all-invoices', $key) }}"
							type="button" class="btn btn-outline-success"
							style="margin: 5px;">
							Facturas de {{ $value }} {{date('Y')}}
						</a>
					@endforeach
				  @endif
					</div>
					<table class="table">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Dirección</th>
								<th scope="col">Precio sin IVA</th>
								<th scope="col">Precio con IVA</th>
							</tr>
						</thead>
						<tbody>
							@php
								$total = 0;
							@endphp
							@foreach ($invoices as $key => $invoice)
								{{-- <a href="{{route('download-invoice',[$invoice->nif, 2020, 1])}}">
									Descargar factura de {{$invoice->address}}
								</a> --}}
								<tr>
									<th scope="row">{{$key+1}}</th>
									<td>
										<a href="{{route('show-invoice',[$invoice->nif, $date['year'], $date['month']])}}"
											target="_blank">
											{{$invoice->address}}
										</a>
									</td>
									<td>
									{{ number_format(json_decode($invoice->description, true)[0]['price'], 2, ',', '') }}€
									</td>
									<td>
									{{ number_format(json_decode($invoice->description, true)[0]['price']*1.21, 2, ',','') }}€
									</td>
								</tr>
								@php
									$data = json_decode($invoice->description, true);
									foreach ($data as $key => $value) {
										$total += $value['price'];
									}
								@endphp
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th scope="col">#</th>
								<th scope="col">TOTAL FINAL</th>
								<th scope="col">
					  				{{ number_format($total, 1, '.', '') }} €
								</th>
								<th scope="col">
									{{ number_format($total*1.21, 3, '.', '') }} € klk
								</th>
							</tr>
						</tfoot>
					</table>
				</div>
            </div>
        </div>
    </div>
@endsection


@section('script')
	<script src="{{ asset('cms/assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('cms/assets/extra-libs/sparkline/sparkline.js') }}"></script>
    <script src="{{ asset('cms/dist/js/waves.js') }}"></script>
    <script src="{{ asset('cms/dist/js/sidebarmenu.js') }}"></script>
    <script src="{{ asset('cms/dist/js/custom.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/excanvas.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot/jquery.flot.crosshair.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('cms/dist/js/pages/chart/chart-page-init.js') }}"></script>
@endsection