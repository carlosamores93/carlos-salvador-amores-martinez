@extends('templates.admin.layout')

@section('head')
	<title>Encryptions</title>
    <meta name="description" content="">
    <link rel="stylesheet" type="text/css" href="{{ asset('cms/assets/extra-libs/multicheck/multicheck.css') }}">
    <link href="{{ asset('cms/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <link href="{{ asset('cms/dist/css/style.min.css') }}" rel="stylesheet">
@endsection


@section('content')
	<div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ route('encryption.create') }}"
                            type="submit" class="btn btn-info">Create item</a>
                        <h2 class="card-title">All encryptions</h2>
                        <div class="table-responsive">
                            <table id="encryption_datatble" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Uuid</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach ($encryptions as $w)
	                                    <tr>
	                                        <td>{{ $w->id }}</td>
	                                        <td>{{ $w->uuid }}</td>
	                                        <td>{{ $w->title }}</td>
	                                        <td>{!! $w->description !!}</td>
	                                        <td>
                                                <a class="btn btn-success"
                                                    href="{{ route('encryption.edit', $w) }}">
                                                    Edit
                                                </a>
                                                <form method="POST"
                                                onclick="return confirm('Do you want delete this item? Are you sure?')"
                                                action="{{ route('encryption.destroy',$w) }}">
													@csrf
													@method('DELETE')
	                                        		<button type="submit" class="btn btn-danger">Delete</button>
	                                        	</form>
	                                        </td>
	                                    </tr>
                                	@endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Uuid</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('script')
	<script src="{{ asset('cms/assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('cms/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('cms/assets/extra-libs/sparkline/sparkline.js') }}"></script>
    <script src="{{ asset('cms/dist/js/waves.js') }}"></script>
    <script src="{{ asset('cms/dist/js/sidebarmenu.js') }}"></script>
    <script src="{{ asset('cms/dist/js/custom.min.js') }}"></script>
    <script src="{{ asset('cms/assets/extra-libs/multicheck/datatable-checkbox-init.js') }}"></script>
    <script src="{{ asset('cms/assets/extra-libs/multicheck/jquery.multicheck.js') }}"></script>
    <script src="{{ asset('cms/assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        $('#encryption_datatble').DataTable();
    </script>
@endsection