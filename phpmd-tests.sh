#!/bin/bash

DIR="tests"

if [ -d "$DIR" ]; then
	./vendor/bin/phpmd tests text rulesets/ruleset-tests.xml
fi