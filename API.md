# API
Doc for use this API.

To generate doc:
```sh
php artisan l5-swagger:generate
```

[API Doc](http://carlosamores.hol.es/api/documentation)
[Anotations](https://github.com/DarkaOnLine/L5-Swagger/blob/master/tests/storage/annotations/OpenApi/Anotations.php)

# Users

Get all users or only one
```
http://localhost:8000/api/users/{id}
```

Store user, method POST, params (form-data):
```
Params: name, email, password, lastname

http://localhost:8000/api/users
```

Update user, method PUT, params (x-www-form-urlencoded):
```
Params: name, email, password, lastname...

http://localhost:8000/api/users
```

Delete user, method DELETE
```
http://localhost:8000/api/users/{id}
```


# Buyers
Get all buyers or only one
```
http://localhost:8000/api/buyers/{id}
```
## Buyers Transactions
List of a buyer's transactions
```
http://localhost:8000/api/buyers/{id}/transactions
```
## Buyers Products
List of a buyer's products
```
http://localhost:8000/api/buyers/{id}/products
```
## Buyers Sellers
List of a buyer's sellers
```
http://localhost:8000/api/buyers/{id}/sellers
```
## Buyers Categories
List of a buyer's categories
```
http://localhost:8000/api/buyers/{id}/categories
```

# Sellers
Get all sellers or only one
```
http://localhost:8000/api/sellers/{id}
```
## Sellers Transactions
List of a seller's transactions
```
http://localhost:8000/api/sellers/{id}/transactions
```
## Sellers Products
List of a seller's products
```
http://localhost:8000/api/sellers/{id}/products
```
Create product for seller, method POST, params (form-data):
```
Params: name, description, quantity, image(file)

http://localhost:8000/api/sellers/{id}/products
```
Update product for seller, method PUT, params (x-www-form-urlencoded):
```
Params: name, description, quantity, image(file), status(1/0)

http://localhost:8000/api/sellers/{id}/products/{id}
```
Delete product for seller, method DELETE
```
http://localhost:8000/api/sellers/{id}/products/{id}
```
View imagen product
```
http://localhost:8000/img/api/field_image_product.jpeg
```
Update imagen product, method POST, params (form-data):
```
Params: name, description, quantity, image(file), _method : PUT or PATCH

http://localhost:8000/img/api/field_image_product.jpeg
```
## Sellers Buyers
List of a seller's buyers
```
http://localhost:8000/api/sellers/{id}/buyers
```
## Sellers Categories
List of a seller's categories
```
http://localhost:8000/api/sellers/{id}/categories
```

# Categories
Get all categories or only one
```
http://localhost:8000/api/categories/{id}
```
## Categories Transactions
List of a category's transactions
```
http://localhost:8000/api/categories/{id}/transactions
```
## Categories Products
List of a category's products
```
http://localhost:8000/api/categories/{id}/products
```
## Categories Buyers
List of a category's buyers
```
http://localhost:8000/api/categories/{id}/buyers
```
## Categories Sellers
List of a category's sellers
```
http://localhost:8000/api/categories/{id}/sellers
```

# Transactions
Get all transactions or only one
```
http://localhost:8000/api/transactions/{id}
```
## Transactions Sellers
List of a transaction's sellers
```
http://localhost:8000/api/transactions/{id}/sellers
```
## Transactions Categories
List of a transaction's categories
```
http://localhost:8000/api/transactions/{id}/categories
```


# Products
Get all products or only one
```
http://localhost:8000/api/products/{id}
```
## Products Transactions
List of a product's transactions
```
http://localhost:8000/api/products/{id}/transactions
```
## Products Buyers
List of a product's buyers
```
http://localhost:8000/api/products/{id}/buyers
```
## Products Categories
List of a product's categories
```
http://localhost:8000/api/products/{id}/categories
```
Add category to product, method PUT
```
http://localhost:8000/api/products/{id}/categories/{id}
```
Remove to category from product, method DELETE
```
http://localhost:8000/api/products/{id}/categories/{id}
```
## Products Buyers Transactions
Create a new transaction, method POST, params (form-data):
```
Params: quantity

http://localhost:8000/api/products/{id}/buyers/{id}/transactions
```