# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [8.2.0] - 2024-07-24

### Changes

Button to sent invoices.

## [8.1.2] - 2024-07-20

### Changes

Fix in blades and list invoices

## [8.1.1] - 2024-07-13

### Changes

Fix in blade

## [8.1.0] - 2024-07-13

### Changes

Improves invoices edit

## [8.0.0] - 2023-09-04

### Update

Update to Laravel 10

## [7.1.0] - 2023-01-04

### Update

Redirect errors in web navigation

## [7.0.1] - 2023-01-02

### Update

Composer documentation for production env

## [7.0.0] - 2022-12-09

### Changes Update

Update Laravel 9 and php 8.1
Use intervention/image for optimize images

## [6.0.2] - 2022-10-02

### Changes

Update message for Chapi

## [6.0.1] - 2022-07-26

### Changes

Update php-cs-fixer and update readme.md

## [6.0.0] - 2022-02-25

### Update Created

Update faker, localization and add jobs

## [5.9.0] - 2022-01-16

### Update Created

Refact flow invoices, changes in class and tests
Changes in commands
Updated all proyect
Updated mysql-schema.dump

## [5.8.0] - 2021-12-30

### Update Created

Fix update image for works
Created WorkControllerCRUDTest
Created views/errors

## [5.7.0] - 2021-12-21

### Update

Store images in storage path for works
Add exmaple Factory pattern

## [5.6.1] - 2021-12-20

### Update

Icon fab fa-gitlab

## [5.6.0] - 2021-12-06

### Update Created

Update all project and add package SpeedTrapListener for test
Add examaples of Desing Pattern as Chain of Responsibilities

## [5.5.0] - 2021-09-24

### Update

New homepage
Change in notice command

## [5.4.0] - 2021-08-19

### Update Created

Update all project
Datatable in invoices list
Add endpoint works, skills and miniskills

## [5.3.0] - 2021-06-20

### Update

Update all project as grumphp, phpmd add other package and more changes.

## [5.2.0] - 2021-06-13

### Created

Invoices CRUD

## [5.1.3] - 2021-04-09

### Created

Endpoint task

## [5.1.2] - 2021-04-07

### Update

CORS

## [1.2.0] - 2021-03-29

### Update

Perfomance API

## [5.1.1] - 2021-02-10

### Fixed Update

Bug fix invoices and update package

## [5.1.0] - 2021-01-15

### Fixed

Bug fix invoices

## [5.0.1] - 2020-12-07

### Fixed

L5-swager fix

## [5.0.0] - 2020-12-07

### Added Changed Fixed

Update project, Laravel 8.x and php7.4

## [4.0.1] - 2020-12-05

### Fixed

Generate only active invoices

## [4.0.0] - 2020-12-05

### Added

Analized code with grumphp: phpcs, phpcsfixer, phpmd, phpunit and composer

## [3.2.0] - 2020-10-10

### Added

CRUD amazon product for afiliate

## [3.1.1] - 2020-06-27

### Fixed Added Changed

Bugfixe date compare year in command notice

Backoffice houses and simulation

## [3.1.0] - 2020-05-16

### Added Changed

Changes in phpunit.xml and add tests

Swagger

## [3.0.1] - 2020-05-06

### Added Changed

Bugs:

Error save slug post

Show user's image in list post and post

Show only post published in client view

## [3.0.0] - 2020-05-06

### Added Changed

Mini blog, CRUD posts and views client, add template to use in the blog

Superadmmin View dasboardh and more changes

Changes invoices, admin and superadmin

All changes can see in this commit [a826e4c9](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/a826e4c9fbb18f380a95c851a54c01b46b5ac344)

## [2.0.1] - 2020-04-11

### Added Changed

View CV one page as HTML and download CV to PDF from HTML
Changes storage user's images, and changes in CRUD users
All changes can see in this commit [ffefc2f7](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/ffefc2f74abed99d1793cc3469a3b36a3c77c796)

## [2.0.0] - 2020-04-03

### Added Changed

Upgrade Laravel to 6.x from 5.8.\* [Changes](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/d2466db477f28ae9689cdb7228c4897ade91fbcc)

## [1.1.1] - 2020-03-26

### Changed

Changes filesystem for production [0fb88aad](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/0fb88aad59ce756f189f9d361b31f0a9ede294a4)

## [1.1.0] - 2020-03-26

### Added Changed

Add many operations for API and CRUD imgs for products [Changes](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/merge_requests/6/diffs)

## [1.0.14] - 2020-02-29

### Added Changed

New views home admin and main home. Changes in .env file, add template admin as zip, add field username in users table [4ab5049f](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/4ab5049f787bdf8abee9c31d80571132af068c81)

## [1.0.13] - 2020-02-28

### Added

Create full_name Serialization [e27f7a2e](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/e27f7a2ecfdd3e6e7024cbc3a4bccf2a93342428)

## [1.0.12] - 2020-02-27

### Added

Crud users in admin view, only users with role superAdmin [92c7da80](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/92c7da80e494caeff06a20530524c454847b1769)

## [1.0.11] - 2020-02-27

### Changed

Changes in DatabaseSeeder and more changes in factories, seeders and views [df07bac0](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/df07bac0e339ebd531a8d9a301cf592415a97ea6)

## [1.0.10] - 2020-02-25

### Fixed

Fix mongodb in production[8e56b63d](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/8e56b63d5c3e9f7c568d0fe525da42cef265e36b)

## [1.0.9] - 2020-02-25

### Added

Crud with MongoDB enviroments develops

## [1.0.8] - 2020-02-23

### Changed

Test generate pdf and vuejs crud to admin view [928763c4](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/928763c4ee93b661269a0f4cc853ceba8248d92a)

## [1.0.7] - 2020-02-22

### Removed Added

Remove crud with angularjs and added crud encryption with uuid [b2762558](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/b27625588daacaf1d870c4680161c25fb3dc8e46)

## [1.0.6] - 2020-02-21

### Security

Dont edit superadmin user from API [c46b4cd0](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/c46b4cd06875780e15d0672d1eaa23b6711fd22f)

## [1.0.5] - 2020-02-21

### Added

Added roles for users and middleware [e1b82a00](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/e1b82a00bb52ed7d34acadb3dd40447cc2e37933)

## [1.0.4] - 2020-02-16

### Added

Pretty phpunit print with command alias [ee3dc1c6](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/ee3dc1c6585befdf607cd16bd2e182b21a3f9e61)

## [1.0.3] - 2020-02-16

### Changed

Rename namespace project correct [1a611a1a](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/1a611a1ada49dfe4e1e5865fff826e5b68d8562f)

## [1.0.2] - 2020-02-16

### Changed

Rename namespace project [87bec0e6](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/87bec0e64b064f4e7d7547032111b3dd2b6cf32b)

## [1.0.1] - 2020-02-16

### Added

Invoices Carlos Cueva [cdf6ce93](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/cdf6ce93a4095dd6dbf7065d58d5f7ddfb59070c)

## [1.0.0] - 2020-02-12

### Added

First version API with C¡crud users and more [9e84851a](https://gitlab.com/carlosamores93/carlos-salvador-amores-martinez/-/commit/9e84851a41b19d6965ec0284e8f79a66e569d547)

## [0.0.1] - 2019-12-03

Second version tag

## [0.0.0] - 2019-12-03

First tag
