<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Client')->group(function () {
	Route::get('/', 'SiteController@home')->name('home');

	Route::get(
		'bautizo',
		'SiteController@christening'
	)->name('christening')->middleware('auth');

	Route::get(
		'view-cv',
		'CurriculumVitaeController@viewCurriculumVitae'
	)->name('view-cv');
	Route::get(
		'download-cv',
		'CurriculumVitaeController@downloadCurriculumVitae'
	)->name('download-cv');
	Route::get(
		'curriculum-vitae-carlos-amores',
		'CurriculumVitaeController@index'
	)->name('curriculum');

	Route::get('curriculum-{name}', function ($name) {
		return redirect(route('curriculum'), 301);
	})->where('name', '[A-Za-z]+');

	// Blog
	Route::group(['prefix' => 'blog'], function () {
		Route::get(
			'/',
			'MiniblogController@home'
		)->name('blog');
		// Route::get(
		//     '/{category}',
		//     'MiniblogController@category'
		// )->name('blog.category');
		Route::get(
			'/{title}',
			'MiniblogController@post'
		)->name('blog.post');
	});
});

// Authentication
Auth::routes();

Route::namespace('Admin')->group(function () {
	Route::prefix('admin')->group(function () {
		Route::group(['middleware' => 'auth'], function () {
			Route::get('/', 'AdminController@home')->name('cms-home');

			Route::resource('/encryption', 'EncryptionController');

			// CRUD with Vuejs
			Route::get('articles', function () {
				return view('back.article.index');
			})->name('articles.index');
			// List articles
			Route::get('article', 'ArticleController@index');
			// List single article
			Route::get('article/{id}', 'ArticleController@show');
			// Create new article
			Route::post('article', 'ArticleController@store');
			// Update article
			Route::put('article', 'ArticleController@store');
			// Delete article
			Route::delete('article/{id}', 'ArticleController@destroy');

			// Crud with MongoDB
			Route::resource('/car', 'CarController');

			// Edit and update user profile
			// Route::resource(
			//     '/user',
			//     'UserController',
			//     ['only' => ['edit', 'update']]
			// );
			Route::get(
				'/user/{user}/edit',
				'UserController@edit'
			)->name('user-edit');
			Route::put(
				'/user/{user}',
				'UserController@update'
			)->name('user-update');

			// Invoices Carlos Cueva
			Route::resource('/invoice', 'InvoiceController');
			Route::get(
				'/download-all-invoices-{month}',
				'InvoiceController@downloadAllInvoice'
			)->name('download-all-invoices');
			Route::post(
				'/sent-all-invoices-{month}',
				'InvoiceController@sentAllInvoice'
			)->name('sent-all-invoices');
			Route::get(
				'/invoice-pdf-{cif}-{year}-{month}/{number?}',
				'InvoiceController@downloadInvoice'
			)->name('download-invoice');
			Route::get(
				'/invoice-{cif}-{year}-{month}/{number?}',
				'InvoiceController@showInvoice'
			)->name('show-invoice');

			Route::group(['middleware' => 'superadmin'], function () {
				Route::resource('/work', 'WorkController');
				Route::resource('/skill', 'SkillController');
				Route::resource('/miniskill', 'MiniskillController');
			});
		});
	});
});

// Routes to SuperAdmin
Route::group(
	[
		'prefix' => 'super-admin',
		'namespace' => 'Admin',
		'middleware' => ['auth', 'superadmin'],
	],
	function () {
		Route::get('/', 'SuperAdminController@home')->name('super.admin.home');
		Route::resource(
			'/user',
			'UserController'
		);
		Route::resource('/notice', 'NoticeController');
		Route::resource('/amazon-product', 'AmazonProductController');
		Route::resource('/post', 'PostController');
		Route::get(
			'/houses/simulation',
			'HouseController@simulation'
		)->name('simulation');
		Route::resource('/houses', 'HouseController');
	}
);
