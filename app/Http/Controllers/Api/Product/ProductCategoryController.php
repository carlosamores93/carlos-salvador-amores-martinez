<?php

namespace Carlos\Amores\Http\Controllers\Api\Product;

use Carlos\Amores\Http\Controllers\ApiController;
use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Models\Category;
use Carlos\Amores\Models\Product;
use Illuminate\Http\Request;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class ProductCategoryController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Product $product)
	{
		return $this->showAll($product->categories);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Product $product
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Product $product)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Product $product
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Product $product)
	{
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Carlos\Amores\Product   $product
	 * @param Category                 $category
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Product $product, Category $category)
	{
		// sync, attach, syncWithoutDetaching

		// Sustituye la lista por la categoria actual
		//$product->categories()->sync([$category->id]);

		// Repite las categorias
		//$product->categories()->attach([$category->id]);

		$product->categories()->syncWithoutDetaching([$category->id]);

		return $this->showAll($product->categories);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Product $product
	 * @param Category               $category
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Product $product, Category $category)
	{
		if (!$product->categories->find($category->id)) {
			return $this->errorResponse(
				'La categoria no pertenece a este producto',
				HttpStatusCodes::NOT_FOUND
			);
		}

		$product->categories()->detach([$category->id]);

		return $this->showAll($product->categories);
	}
}
