<?php

namespace Carlos\Amores\Http\Controllers\Api\Product;

use Carlos\Amores\Http\Controllers\ApiController;
use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Models\Product;
use Carlos\Amores\Models\Transaction;
use Carlos\Amores\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class ProductBuyerTransactionController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 * Creacion de una transaccion.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param Product                  $product
	 * @param User                     $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request, Product $product, User $buyer)
	{
		$rules = [
			'quantity' => 'required|integer|min:1',
		];
		$this->validate($request, $rules);
		if ($buyer->id === $product->selled_id) {
			return $this->errorResponse(
				'El comprador debe ser diferente al vendedor',
				HttpStatusCodes::CONFLICT
			);
		}
		if (!$buyer->isVerified()) {
			return $this->errorResponse(
				'El comprador debe ser verificado',
				HttpStatusCodes::CONFLICT
			);
		}
		if (!$product->seller->isVerified()) {
			return $this->errorResponse(
				'El vendedor debe ser verificado',
				HttpStatusCodes::CONFLICT
			);
		}
		if ($product->isNotAvaible()) {
			return $this->errorResponse(
				'El producto no está disponible',
				HttpStatusCodes::CONFLICT
			);
		}
		if ($product->quantity < $request->quantity) {
			return $this->errorResponse(
				'El producto no tiene cantidad suficiente disponible',
				HttpStatusCodes::CONFLICT
			);
		}

		// Transacciones de la bbdd: asegurarnos que una transaccion se haga.
		// Operacion completas, si falla todo vuelve a su estado normal.
		return DB::transaction(function () use ($request, $product, $buyer) {
			$product->quantity = $product->quantity - $request->quantity;
			$product->save();

			// Implementado en AppServiceProvider.php (Deprecated)
			// if ($product->quantity === 0 && $product->isAvailable()) {
			//     $product->status = Product::PRODUCTO_NO_DISPONIBLE;
			//     $product->save();
			// } Done in ProductObserver
			$transaction = Transaction::create(
				[
					'quantity' => $request->quantity,
					'product_id' => $product->id,
					'buyer_id' => $buyer->id,
				]
			);

			return $this->showOne($transaction, HttpStatusCodes::CREATED);
		});
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Product $product
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Product $product)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Product $product
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Product $product)
	{
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Carlos\Amores\Product   $product
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Product $product)
	{
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Product $product
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Product $product)
	{
	}
}
