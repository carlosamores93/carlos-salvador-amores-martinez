<?php

namespace Carlos\Amores\Http\Controllers\Api\Category;

use Carlos\Amores\Models\Category;
use Illuminate\Http\Request;
use Carlos\Amores\Http\Controllers\ApiController;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class CategoryTransactionController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 * Lista transaciones para una categoria.
	 *
	 * @param Category $category
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Category $category)
	{
		$transactions = $category->products()
			->whereHas('transactions') // Productos con al menos una transacion
			->with('transactions')
			->get()
			->pluck('transactions')
			->collapse();

		return $this->showAll($transactions);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Category $category
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Category $category)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Category $category
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Category $category)
	{
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Carlos\Amores\Category  $category
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Category $category)
	{
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Category $category
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Category $category)
	{
	}
}
