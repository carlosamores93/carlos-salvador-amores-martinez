<?php

namespace Carlos\Amores\Http\Controllers\Api\Category;

use Carlos\Amores\Http\Controllers\ApiController;
use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Models\Category;
use Illuminate\Http\Request;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class CategoryController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$categories = Category::all();

		return $this->showAll($categories);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$rules = [
			'name' => 'required',
			'description' => 'required',
		];
		$this->validate($request, $rules);
		$category = Category::create($request->all());

		return $this->showOne($category, HttpStatusCodes::CREATED);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Category $category
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Category $category)
	{
		return $this->showOne($category);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Carlos\Amores\Category  $category
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Category $category)
	{
		$category->fill(
			$request->only(
				[
					'name', 'description',
				]
			)
		);
		if ($category->isClean()) {
			return $this->errorResponse(
				'Debes actualizar al menos un campo',
				HttpStatusCodes::UNPROCESSABLE_ENTITY
			);
		}
		$category->save();

		return $this->showOne($category);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Category $category
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Category $category)
	{
		$category->delete();

		return $this->showOne($category);
	}
}
