<?php

namespace Carlos\Amores\Http\Controllers\Api\Transaction;

use Carlos\Amores\Models\Transaction;
use Illuminate\Http\Request;
use Carlos\Amores\Http\Controllers\ApiController;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class TransactionCategoryController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 * Obtener lista categories de una transaccion especifica.
	 *
	 * @param Transaction $transaction
	 */
	public function index(Transaction $transaction)
	{
		$categories = $transaction->product->categories;

		return $this->showAll($categories);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Transaction $transaction
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Transaction $transaction)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Transaction $transaction
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Transaction $transaction)
	{
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request   $request
	 * @param \Carlos\Amores\Transaction $transaction
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Transaction $transaction)
	{
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Transaction $transaction
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Transaction $transaction)
	{
	}
}
