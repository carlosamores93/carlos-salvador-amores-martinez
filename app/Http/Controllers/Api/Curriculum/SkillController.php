<?php

namespace Carlos\Amores\Http\Controllers\Api\Curriculum;

use Carlos\Amores\Http\Controllers\ApiController;
use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Models\Skill;
use Carlos\Amores\Repositories\SkillRepository;

class SkillController extends ApiController
{
	/**
	 * Repository.
	 *
	 * @var Carlos\Amores\Repositories\SkillRepository
	 */
	private $skillRepository;

	public function __construct(
		SkillRepository $skillRepository
	) {
		$this->skillRepository = $skillRepository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return $this->showAll(
			$this->skillRepository->getActiveSkills(),
			HttpStatusCodes::OK
		);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Models\Skill $skill
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Skill $skill)
	{
		return $this->showOne($skill, HttpStatusCodes::OK);
	}
}
