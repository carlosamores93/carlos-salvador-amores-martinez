<?php

namespace Carlos\Amores\Http\Controllers\Api\Curriculum;

use Carlos\Amores\Http\Controllers\ApiController;
use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Models\Work;
use Carlos\Amores\Repositories\WorkRepository;

class WorkController extends ApiController
{
	private $workRepository;

	public function __construct(
		WorkRepository $workRepository
	) {
		$this->workRepository = $workRepository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return $this->showAll(
			$this->workRepository->getActiveWorks(),
			HttpStatusCodes::OK
		);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Models\Work $work
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Work $work)
	{
		return $this->showOne($work, HttpStatusCodes::OK);
	}
}
