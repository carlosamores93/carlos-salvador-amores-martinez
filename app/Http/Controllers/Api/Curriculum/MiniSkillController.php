<?php

namespace Carlos\Amores\Http\Controllers\Api\Curriculum;

use Carlos\Amores\Http\Controllers\ApiController;
use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Repositories\MiniSkillRepository;

class MiniSkillController extends ApiController
{
	/**
	 * Repository.
	 *
	 * @var Carlos\Amores\Repositories\MiniSkillRepository
	 */
	private $miniSkillRepository;

	public function __construct(
		MiniSkillRepository $miniSkillRepository
	) {
		$this->miniSkillRepository = $miniSkillRepository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return $this->showAll(
			$this->miniSkillRepository->getActiveMiniSkills(),
			HttpStatusCodes::OK
		);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(int $id)
	{
		$miniSkill = $this->miniSkillRepository->find($id);

		return $this->showOne($miniSkill, HttpStatusCodes::OK);
	}
}
