<?php

namespace Carlos\Amores\Http\Controllers\Api\Task;

use Carlos\Amores\Http\Controllers\ApiController;
use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Models\Task;
use Illuminate\Http\Request;

class TaskController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$tasks = Task::all();

		return $this->showAll($tasks);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// if ($request->has('categories')) {
		// 	$request['categories'] = json_encode($request->categories);
		// } else {
		// 	$request['categories'] = json_encode([]);
		// }
		$task = Task::create($request->all());

		return $this->showOne($task, HttpStatusCodes::CREATED);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Models\Task $task
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Task $task)
	{
		return $this->showOne($task);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Models\Task $task
	 *
	 * @return \Illuminate\Http\Response
	 */
	// public function edit(Task $task)
	// {
	// 	//
	// }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request   $request
	 * @param \Carlos\Amores\Models\Task $task
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Task $task)
	{
		// if ($request->has('categories')) {
		// 	$request['categories'] = json_encode($request->categories);
		// } else {
		// 	$request['categories'] = json_encode([]);
		// }
		$task->fill(
			$request->all()
		);
		$task->save();

		return $this->showOne($task);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Models\Task $task
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Task $task)
	{
		$task->delete();

		return $this->showOne($task);
	}
}
