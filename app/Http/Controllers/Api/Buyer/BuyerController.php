<?php

namespace Carlos\Amores\Http\Controllers\Api\Buyer;

use Carlos\Amores\Models\Buyer;
use Carlos\Amores\Http\Controllers\ApiController;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class BuyerController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$compradores = Buyer::has('transactions')->get();

		return $this->showAll($compradores);
		// return response()->json(
		//     ['data' => $compradores],
		//     200
		// );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Buyer $buyer)
	{
		// $comprador = Buyer::has('transactions')->findOrFail($id);
		return $this->showOne($buyer);
		// return response()->json(
		//     ['data' => $comprador],
		//     200
		// );
	}
}
