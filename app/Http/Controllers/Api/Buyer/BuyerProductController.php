<?php

namespace Carlos\Amores\Http\Controllers\Api\Buyer;

use Carlos\Amores\Models\Buyer;
use Illuminate\Http\Request;
use Carlos\Amores\Http\Controllers\ApiController;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class BuyerProductController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 * Obtener todos los productos que un comprador ha pedido.
	 *
	 * @param Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Buyer $buyer)
	{
		// $buyer->transactions
		// esto devuelve una coleciion y no una lista de transacciones

		// $buyer->transactions()
		// accedemos al query de la relación, llamaos a la function
		// query builder, podemos poner where(), find() ...
		$prodcuts = $buyer->transactions()->with('product')
			->get()
			->pluck('product'); // solo queremos los products

		return $this->showAll($prodcuts);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Buyer $buyer)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Buyer $buyer)
	{
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Carlos\Amores\Buyer     $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Buyer $buyer)
	{
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Buyer $buyer)
	{
	}
}
