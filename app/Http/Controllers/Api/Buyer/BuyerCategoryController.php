<?php

namespace Carlos\Amores\Http\Controllers\Api\Buyer;

use Carlos\Amores\Models\Buyer;
use Illuminate\Http\Request;
use Carlos\Amores\Http\Controllers\ApiController;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class BuyerCategoryController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 * Obtener todas categories en las cuales un comprador ha realizado compras.
	 *
	 * @param Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Buyer $buyer)
	{
		$categories = $buyer->transactions()->with('product.categories')
			->get()
			->pluck('product.categories')
			->collapse()
			->unique('id')
			->values();

		return $this->showAll($categories);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Buyer $buyer)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Buyer $buyer)
	{
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Carlos\Amores\Buyer     $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Buyer $buyer)
	{
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Buyer $buyer)
	{
	}
}
