<?php

namespace Carlos\Amores\Http\Controllers\Api\Buyer;

use Carlos\Amores\Models\Buyer;
use Illuminate\Http\Request;
use Carlos\Amores\Http\Controllers\ApiController;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class BuyerSellerController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 * Obtener los vendedores de un comprador.
	 *
	 * @param Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Buyer $buyer)
	{
		$sellers = $buyer->transactions()->with('product.seller')
			->get()
			->pluck('product.seller')
			->unique('id')
			->values();

		return $this->showAll($sellers);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Buyer $buyer)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Buyer $buyer)
	{
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Carlos\Amores\Buyer     $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Buyer $buyer)
	{
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Buyer $buyer
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Buyer $buyer)
	{
	}
}
