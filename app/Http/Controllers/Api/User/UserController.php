<?php

declare(strict_types=1);

namespace Carlos\Amores\Http\Controllers\Api\User;

use Carlos\Amores\Http\Controllers\ApiController;
use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Mail\UserCreatedMail;
use Carlos\Amores\Models\Role;
use Carlos\Amores\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

// Anotations
// https://github.com/DarkaOnLine/L5-Swagger/blob/master/tests/storage/annotations/OpenApi/Anotations.php

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="API Doc",
 *      description="API Doc Description",
 *      @OA\Contact(
 *          email="amorescarlos93@hotmail.com"
 *      )
 * )
 */

/**
 *  @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="API Doc"
 *  )
 */
class UserController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	/**
	 * @OA\Get(
	 *     tags={"Users"},
	 *     path="/api/users",
	 *     summary="Show users",
	 *     description="List all users",
	 *     @OA\Response(
	 *         response=200,
	 *         description="Show all users"
	 *     ),
	 *     @OA\Response(
	 *         response="default",
	 *         description="Ha ocurrido un error."
	 *     )
	 * )
	 */
	public function index()
	{
		// $users = User::all();
		$users = User::forNotSuperAdmin()->get();

		return $this->showAll($users);
		// return response()->json(['data' => $users], 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request->validate([
			'name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|min:5',
		]);
		$fields = $request->all();
		$fields['password'] = bcrypt($request->password);
		$fields['verified'] = User::USER_NOT_VERIFIED;
		$fields['verification_token'] = User::generateVerificationToken();
		$fields['admin'] = User::USER_REGULAR;
		$fields['role_id'] = Role::DEFAULT;
		$user = User::create($fields);

		return $this->showOne($user);
		// return response()->json(['data' => $user], HttpStatusCodes::CREATED);
	}

	/**
	 * @OA\Get(
	 *      path="/api/users/{userId}",
	 *      operationId="getUserById",
	 *      tags={"Users"},
	 *      summary="Get user information",
	 *      description="Returns user data",
	 *      @OA\Parameter(
	 *          name="userId",
	 *          description="User id",
	 *          required=true,
	 *          in="path",
	 *          @OA\Schema(
	 *              type="integer"
	 *          )
	 *      ),
	 *      @OA\Response(
	 *          response=200,
	 *          description="successful operation"
	 *       ),
	 *      @OA\Response(response=400, description="Bad request"),
	 *      @OA\Response(response=404, description="Resource Not Found"),
	 * )
	 */

	/**
	 * Display the specified resource.
	 *
	 * @param User $user
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(User $user)
	{
		// $user = User::findOrFail($id);
		if ($user->isSuperadmin()) {
			return $this->errorResponse(
				'No se puede ver al usuario al superadmin',
				HttpStatusCodes::CONFLICT
			);
		}

		return $this->showOne($user);
		// return response()->json(['data' => $user], 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function edit($id)
	{
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param User                     $user
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
	 * @SuppressWarnings(PHPMD.NPathComplexity)
	 */
	public function update(Request $request, User $user)
	{
		if ($user->isSuperadmin()) {
			return $this->errorResponse(
				'No se puede editar al superadmin',
				HttpStatusCodes::CONFLICT
			);
		}
		// $user = User::findOrFail($id);
		$rules = [
			'email' => 'email|unique:users,email,' . $user->id,
			'password' => 'min:5',
			'admin' => 'in:' . User::USER_REGULAR . ',' . User::USER_ADMIN,
		];

		$this->validate($request, $rules);

		if ($request->has('name')) {
			$user->name = $request->name;
		}

		if ($request->has('role_id')) {
			$user->role_id = Role::DEFAULT;
		}

		if ($request->has('email') && $user->email !== $request->email) {
			$user->verified = User::USER_NOT_VERIFIED;
			$user->verification_token = User::generateVerificationToken();
			$user->email = $request->email;
		}

		if ($request->has('password')) {
			$user->password = bcrypt($request->password);
		}

		if ($request->has('admin')) {
			if (!$user->isVerified()) {
				return $this->errorResponse(
					'Únicamente los usuarios verificados pueden cambiar su valor de administrador',
					HttpStatusCodes::CONFLICT
				);
				// return response()->json(
				//     [
				//         'error' => 'Únicamente los usuarios verificados pueden cambiar su valor de administrador',
				//         'code' => HttpStatusCodes::CONFLICT
				//     ],
				//     HttpStatusCodes::CONFLICT
				// );
			}
			$user->admin = $request->admin;
		}

		if (!$user->isDirty()) {
			return $this->errorResponse(
				'Se debe especificar al menos un valor diferente para actualizar',
				HttpStatusCodes::UNPROCESSABLE_ENTITY
			);
			// return response()->json([
			//     'error' => 'Se debe especificar al menos un valor diferente para actualizar',
			//     'code' => HttpStatusCodes::UNPROCESSABLE_ENTITY
			// ], HttpStatusCodes::UNPROCESSABLE_ENTITY);
		}

		$user->save();

		return $this->showOne($user);
		// return response()->json(['data' => $user], 200);
	}

	/**
	 * @OA\Delete(
	 *     path="/api/users/{userId}",
	 *     summary="Deletes a user",
	 *     description="",
	 *     operationId="deleteUser",
	 *     tags={"Users"},
	 *     @OA\Parameter(
	 *         description="User id to delete",
	 *         in="path",
	 *         name="userId",
	 *         required=true,
	 *         @OA\Schema(
	 *             type="integer",
	 *             format="int64"
	 *         )
	 *     ),
	 *     @OA\Response(
	 *          response=200,
	 *          description="successful operation"
	 *       ),
	 *     @OA\Response(
	 *         response=400,
	 *         description="Invalid ID supplied"
	 *     ),
	 *     @OA\Response(
	 *         response=404,
	 *         description="User not found"
	 *     )
	 * )
	 */

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param User $user
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(User $user)
	{
		if ($user->isSuperadmin()) {
			return $this->errorResponse(
				'No se puede elimminar al superadmin',
				HttpStatusCodes::CONFLICT
			);
		}
		// $user = User::findOrFail($id);
		$user->delete();

		return $this->showOne($user);
		// return response()->json(['data' => $user], 200);
	}

	/**
	 * Verify.
	 *
	 * @param string $token
	 *
	 * @return JsonResponse
	 */
	public function verify(string $token)
	{
		$user = User::forVerificationToken($token)->firstOrFail();
		$user->verified = User::USER_VERIFIED;
		$user->verification_token = null;
		$user->email_verified_at = now();
		$user->save();

		return $this->showMessage('La cuenta ha sido verificada');
	}

	/**
	 * Verify.
	 *
	 * @param User $user
	 *
	 * @return JsonResponse
	 */
	public function resend(User $user)
	{
		if ($user->isVerified()) {
			return $this->errorResponse('El usuario ya ha sido verificado', HttpStatusCodes::CONFLICT);
		}
		Mail::to($user)->send(new UserCreatedMail($user));

		return $this->showMessage('El correo electrónico se ha reenviado');
	}
}
