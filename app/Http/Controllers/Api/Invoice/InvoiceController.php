<?php

namespace Carlos\Amores\Http\Controllers\Api\Invoice;

use Carlos\Amores\DataTransferObject\InvoiceDto;
use Carlos\Amores\Http\Controllers\Controller;
use Carlos\Amores\Models\Invoice;
use Carlos\Amores\Models\User;
use Carlos\Amores\Services\InvoiceService;
use Illuminate\Http\Request;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class InvoiceController extends Controller
{
	private $invoiceService;

	public function __construct(
		InvoiceService $invoiceService
	) {
		$this->invoiceService = $invoiceService;
		// $this->authorizeResource(Invoice::class, 'invoice');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$allInvoices = $this->invoiceService->getInvoices();
		$multiplied = $allInvoices->map(function ($invoices) {
			$dto = new InvoiceDto($invoices);

			return $dto->getDto();
		});

		return response()->json(['data' => $multiplied], 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Models\Invoice $invoice
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Invoice $invoice)
	{
		// $this->authorize('view', User::find(1), $invoice);
		// if (User::find(1)->can('view', $invoice)) {
		// }
		$dto = new InvoiceDto($invoice);

		return response()->json($dto->getDto(), 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Models\Invoice $invoice
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Invoice $invoice)
	{
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request      $request
	 * @param \Carlos\Amores\Models\Invoice $invoice
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Invoice $invoice)
	{
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Models\Invoice $invoice
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Invoice $invoice)
	{
	}
}
