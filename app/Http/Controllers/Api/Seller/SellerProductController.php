<?php

namespace Carlos\Amores\Http\Controllers\Api\Seller;

use Carlos\Amores\Http\Controllers\ApiController;
use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Models\Product;
use Carlos\Amores\Models\Seller;
use Carlos\Amores\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class SellerProductController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 * Productos de un vendedor.
	 *
	 * @param Seller $seller
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Seller $seller)
	{
		$products = $seller->products;

		return $this->showAll($products);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 * Crear productos asociados a un vendedor.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param User                     $seller
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request, User $seller)
	{
		$rules = [
			'name' => 'required',
			'description' => 'required',
			'quantity' => 'required|integer|min:1',
			'image' => 'required|image',
		];
		$this->validate($request, $rules);
		$data = $request->all();
		$data['status'] = Product::PRODUCTO_NO_DISPONIBLE;
		$data['image'] = $request->image->store('', 'imagesapi');
		$data['seller_id'] = $seller->id;

		$product = Product::create($data);

		return $this->showOne($product, HttpStatusCodes::CREATED);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Seller $seller
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Seller $seller)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Seller $seller
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Seller $seller)
	{
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Carlos\Amores\Seller    $seller
	 * @param Product                  $product
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Seller $seller, Product $product)
	{
		$rules = [
			'quantity' => 'integer|min:1',
			'status' => 'in: ' . Product::PRODUCTO_NO_DISPONIBLE
			. ', ' . Product::PRODUCTO_DISPONIBLE,
			'image' => 'image',
		];
		$this->validate($request, $rules);
		$this->verificarVendedor($seller, $product);

		$product->fill(
			$request->only(
				[
					'name', 'description', 'quantity',
				]
			)
		);

		if ($request->has('status')) {
			$product->status = $request->status;
			if ($product->isAvailable() && $product->categories()->count() === 0) {
				return $this->errorResponse(
					'Un producto activo debe tener al menos una categoria.',
					HttpStatusCodes::CONFLICT
				);
			}
		}

		if ($request->has('image')) {
			Storage::disk('imagesapi')->delete($product->image);
			$product->image = $request->image->store('', 'imagesapi');
		}

		if ($product->isClean()) {
			return $this->errorResponse(
				'No se ha actualizado ningun valor del producto',
				HttpStatusCodes::UNPROCESSABLE_ENTITY
			);
		}
		$product->save();

		return $this->showOne($product, HttpStatusCodes::CREATED);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Seller $seller
	 * @param Product               $product
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Seller $seller, Product $product)
	{
		$this->verificarVendedor($seller, $product);
		Storage::disk('imagesapi')->delete($product->image);
		$product->delete();

		return $this->showOne($product);
	}

	/**
	 * Undocumented function.
	 *
	 * @param Seller  $seller
	 * @param Product $product
	 *
	 * @return HttpException|null
	 */
	protected function verificarVendedor(Seller $seller, Product $product)
	{
		if ($seller->id !== $product->seller_id) {
			throw new HttpException(
				HttpStatusCodes::UNPROCESSABLE_ENTITY,
				'Este vendedor no es el propietario del producto'
			);
		}
	}
}
