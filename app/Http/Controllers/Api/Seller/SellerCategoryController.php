<?php

namespace Carlos\Amores\Http\Controllers\Api\Seller;

use Carlos\Amores\Models\Seller;
use Illuminate\Http\Request;
use Carlos\Amores\Http\Controllers\ApiController;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class SellerCategoryController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 * Lista de categories en las que un vendeor ha realizado una venta.
	 *
	 * @param Seller $seller
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Seller $seller)
	{
		$transactions = $seller->products()
			->with('categories')
			->get()
			->pluck('categories')
			->collapse()
			->unique('id')
			->values();

		return $this->showAll($transactions);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Seller $seller
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Seller $seller)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Seller $seller
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Seller $seller)
	{
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Carlos\Amores\Seller    $seller
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Seller $seller)
	{
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Seller $seller
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Seller $seller)
	{
	}
}
