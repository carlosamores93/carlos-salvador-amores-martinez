<?php

namespace Carlos\Amores\Http\Controllers\Api\Seller;

use Carlos\Amores\Http\Controllers\ApiController;
use Carlos\Amores\Models\Seller;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class SellerController extends ApiController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$vendedores = Seller::has('products')->get();

		return $this->showAll($vendedores);
		// return response()->json(
		//     ['data' => $vendedores],
		//     200
		// );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param Seller $seller
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Seller $seller)
	{
		// $vendedor = Seller::has('products')->findOrFail($id);
		return $this->showOne($seller);
		// return response()->json(
		//     ['data' => $vendedor],
		//     200
		// );
	}
}
