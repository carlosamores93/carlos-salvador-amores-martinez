<?php

namespace Carlos\Amores\Http\Controllers\Auth;

use Carlos\Amores\Models\User;
use Carlos\Amores\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/admin';

	/**
	 * Create a new controller instance.
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param array $data
	 *
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => ['required', 'string', 'max:255'],
			'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
			'password' => ['required', 'string', 'min:8', 'confirmed'],
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param array $data
	 *
	 * @return \Carlos\Amores\Models\User
	 */
	protected function create(array $data)
	{
		return User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'phone' => 666666666,
			'password' => Hash::make($data['password']),
		]);
	}

	/**
	 * Undocumented function.
	 *
	 *
	 *  @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function showRegistrationForm()
	{
		if (env('APP_ENV') == 'local') {
			$authentication = 'ok';

			return view('client.auth.register', compact('authentication'));
		} else {
			return abort(404);
		}
		// $authentication = 'ok';
		// return view('client.register', compact('authentication'));
	}
}
