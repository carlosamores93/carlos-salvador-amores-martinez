<?php

namespace Carlos\Amores\Http\Controllers\Auth;

use Carlos\Amores\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;

class VerificationController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Email Verification Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling email verification for any
	| user that recently registered with the application. Emails may also
	| be re-sent if the user didn't receive the original email message.
	|
	*/

	use VerifiesEmails;

	/**
	 * Where to redirect users after verification.
	 *
	 * @var string
	 */
	protected $redirectTo = '/admin';

	/**
	 * Create a new controller instance.
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('signed')->only('verify');
		$this->middleware('throttle:6,1')->only('verify', 'resend');
	}

	/**
	 * Undocumented function.
	 *
	 * @param Request $request
	 *
	 *  @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function show(Request $request)
	{
		if (env('APP_ENV') == 'local') {
			return $request->user()->hasVerifiedEmail()
						? redirect($this->redirectPath())
						: view('auth.verify');
		} else {
			return abort(404);
		}
	}
}
