<?php

namespace Carlos\Amores\Http\Controllers\Admin;

use Carlos\Amores\Models\Encryption;
use Carlos\Amores\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Webpatser\Uuid\Uuid;

class EncryptionController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$encryptions = Encryption::all();

		return view('admin.encryption.index', compact('encryptions'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin.encryption.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$encryption = new Encryption();
		$encryption->fill([
			'uuid' => Uuid::generate()->string,
			'title' => Crypt::encrypt($request->title),
			'description' => Crypt::encrypt($request->description),
		])->save();

		return redirect()->route('encryption.index')
			->with('success', 'Item created successfully');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Encryption $encryption
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function show(Encryption $encryption)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \App\Encryption $encryption
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Encryption $encryption)
	{
		try {
			$encryption->title = Crypt::decrypt($encryption->title);
			$encryption->description = Crypt::decrypt($encryption->description);
		} catch (\Exception $e) {
			dd($e);
		}

		return view('admin.encryption.edit', compact('encryption'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Encryption          $encryption
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Encryption $encryption)
	{
		$encryption->title = Crypt::encrypt($request->title);
		$encryption->description = Crypt::encrypt($request->description);
		$encryption->save();

		return redirect()->route('encryption.index')
			->with('primary', 'Item updated correctly');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Encryption $encryption
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Encryption $encryption)
	{
		$encryption->delete();

		return redirect()->route('encryption.index')
			->with('dark', 'Item deleted successfully');
	}
}
