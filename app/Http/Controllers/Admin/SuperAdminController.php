<?php

namespace Carlos\Amores\Http\Controllers\Admin;

use Carlos\Amores\Http\Controllers\Controller;
use Carlos\Amores\Models\AmazonProduct;
use Carlos\Amores\Models\Notice;
use Carlos\Amores\Models\Post;
use Carlos\Amores\Models\User;
use Carlos\Amores\Models\Work;

class SuperAdminController extends Controller
{
	public function home()
	{
		$numUsers = User::all()->count();
		$numWorks = Work::all()->count();
		$numNotices = Notice::all()->count();
		$numPosts = Post::all()->count();
		$numAmazonProducts = AmazonProduct::all()->count();

		return view(
			'super-admin.home',
			compact(
				'numUsers',
				'numWorks',
				'numNotices',
				'numPosts',
				'numAmazonProducts'
			)
		);
	}
}
