<?php

namespace Carlos\Amores\Http\Controllers\Admin;

use Carlos\Amores\Events\WorkSavedEvent;
use Carlos\Amores\Http\Controllers\Controller;
use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Models\Work;
use Carlos\Amores\Repositories\WorkRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Str;

class WorkController extends Controller
{
	// php artisan make:controller WorkController --resource
	// Route::resource('work', 'WorkController');

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(WorkRepository $work)
	{
		$works = $work->getWorks();
		//$works = Work::orderBy('start_date', 'ASC')->get();
		return view('admin.work.index', compact('works'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin.work.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 *
	 *  @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function store(Request $request)
	{
		$slug = Str::slug($request->company);
		$request['slug'] = $slug;
		$pathImage = $this->storeImgForWork($request, $slug);
		if (!empty($pathImage)) {
			$request['image'] = $pathImage;
		}
		$work = Work::create($request->all());
		WorkSavedEvent::dispatch($work);
		if (env('CACHE_DRIVER') === 'redis') {
			Cache::tags('works')->flush();
		} else {
			Cache::forget('works.all');
		}
		//Cache::flush();
		return redirect()->route('work.index')->with('success', 'Work created successfully');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function show($id)
	{
		return abort(HttpStatusCodes::NOT_FOUND);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$work = Work::where('id', $id)->firstOrFail();

		return view('admin.work.edit', compact('work'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 *
	 *  @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function update(Request $request, $id)
	{
		$work = Work::where('id', $id)->firstOrFail();
		$work->company = $request->company;
		$work->job = $request->job;
		$slug = Str::slug($request->company);
		$work->slug = $slug;
		$work->status = $request->status;
		$work->description = $request->description;
		$work->start_date = $request->start_date;
		$work->end_date = $request->end_date;
		$pathImage = $this->storeImgForWork($request, $slug);
		if (!empty($pathImage)) {
			Storage::delete(
				str_replace('storage', 'public', $work->image)
			);
			$work->image = $pathImage;
		}
		$work->save();
		WorkSavedEvent::dispatch($work);
		if (env('CACHE_DRIVER') === 'redis') {
			Cache::tags('works')->flush();
		} else {
			Cache::forget('works.all');
		}
		//Cache::flush();
		return redirect()->route('work.index')->with('primary', 'Work updated correctly');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 *
	 *  @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function destroy($id)
	{
		$work = Work::where('id', $id)->firstOrFail();
		Storage::delete(
			str_replace('storage', 'public', $work->image)
		);
		$work->image = null;
		$work->status = false;
		$work->save();
		$work->delete();
		if (env('CACHE_DRIVER') === 'redis') {
			Cache::tags('works')->flush();
		} else {
			Cache::forget('works.all');
		}
		//Cache::flush();
		return redirect()->route('work.index')->with('dark', 'Work deleted successfully');
	}

	/**
	 * Store images for works.
	 *
	 * @param Request $request
	 * @param string  $slug
	 *
	 * @return string|null
	 */
	private function storeImgForWork(Request $request, string $slug): ?string
	{
		if (empty($request->file)) {
			return null;
		}

		try {
			$path = Storage::putFileAs(
				'public/images/works',
				$request->file,
				$slug . '.jpg'
			);
		} catch (\Throwable $th) {
			//throw $th;
			return null;
		}

		return str_replace('public', 'storage', $path);
	}
}
