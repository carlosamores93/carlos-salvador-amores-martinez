<?php

namespace Carlos\Amores\Http\Controllers\Admin;

use Carlos\Amores\Models\Car;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carlos\Amores\Http\Controllers\Controller;

class CarController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		// Uso de scopes
		// https://www.youtube.com/watch?v=lsFXi1ILD2Y
		$cars = Car::searchitem($request->get('search'))->get();
		if (empty($request->get('search'))) {
			$cars = Car::all();
		}
		$develop = false;

		return view('admin.car.index', compact('cars', 'develop'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin.car.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$car = new Car();
		$car->company = $request->get('company');
		$car->model = $request->get('model');
		$car->status = $request->get('status');
		$car->slug = Str::slug($request->get('company') . '-' . $request->get('model'));
		$car->description = $request->get('description');
		$car->save();

		return redirect()->route('car.index')
			->with('success', 'Car has been successfully added');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Models\Car $car
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function show(Car $car)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Models\Car $car
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Car $car)
	{
		return view('admin.car.edit', compact('car'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request  $request
	 * @param \Carlos\Amores\Models\Car $car
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Car $car)
	{
		$car->company = $request->get('company');
		$car->model = $request->get('model');
		$car->status = $request->get('status');
		$car->slug = Str::slug($request->get('company') . '-' . $request->get('model'));
		$car->description = $request->get('description');
		$car->save();

		return redirect()->route('car.index')
			->with('success', 'Car has been successfully update');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Models\Car $car
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Car $car)
	{
		$car->delete();

		return redirect()->route('car.index')
			->with('success', 'Car has been  deleted');
	}
}
