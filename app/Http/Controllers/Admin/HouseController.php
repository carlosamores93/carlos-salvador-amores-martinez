<?php

namespace Carlos\Amores\Http\Controllers\Admin;

use Carlos\Amores\Http\Controllers\Controller;
use Carlos\Amores\Models\House;
use Illuminate\Http\Request;

class HouseController extends Controller
{
	private $model;

	public function __construct(House $house)
	{
		$this->model = $house;
	}

	/**
	 * Simulation.
	 *
	 * @return View
	 */
	public function simulation()
	{
		return view('super-admin.house.simulation');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function index(Request $request)
	{
		if (empty($request->all())) {
			$houses = House::all();
		} else {
			// Apply filters
			$query = $this->model->query();
			foreach ($request->all() as $filter => $value) {
				$query = $query->{$filter}($value);
			}
			$houses = $query->get();
		}

		return view('super-admin.house.index', compact('houses'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('super-admin.house.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request['precio_m_cuadrado'] = $request['precio'] / $request['superficie'];
		House::create($request->all());

		return redirect()->route('houses.index')
			->with('success', 'House created successfully');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Models\House $house
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function show(House $house)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Models\House $house
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(House $house)
	{
		return view('super-admin.house.edit', compact('house'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request    $request
	 * @param \Carlos\Amores\Models\House $house
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, House $house)
	{
		$request['precio_m_cuadrado'] = $request['precio'] / $request['superficie'];
		$house->fill($request->all());
		$house->save();

		return redirect()->route('houses.index')
			->with('primary', 'House updated correctly');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Models\House $house
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(House $house)
	{
		$house->delete();

		return redirect()->route('houses.index')
			->with('dark', 'House deleted successfully');
	}
}
