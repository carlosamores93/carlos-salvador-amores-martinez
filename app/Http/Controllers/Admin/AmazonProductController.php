<?php

namespace Carlos\Amores\Http\Controllers\Admin;

use Carlos\Amores\Http\Controllers\Controller;
use Carlos\Amores\Models\AmazonProduct;
use Illuminate\Http\Request;

class AmazonProductController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$amazonProducts = AmazonProduct::withTrashed()
			->orderBy('created_at', 'DESC')->get();

		return view('super-admin.amazon-product.index', compact('amazonProducts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('super-admin.amazon-product.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		AmazonProduct::create($request->all());

		return redirect()->route('amazon-product.index')
			->with('success', 'Created successfully');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Models\AmazonProduct $amazonProduct
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function show(AmazonProduct $amazonProduct)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Models\AmazonProduct $amazonProduct
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(AmazonProduct $amazonProduct)
	{
		return view('super-admin.amazon-product.edit', compact('amazonProduct'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request            $request
	 * @param \Carlos\Amores\Models\AmazonProduct $amazonProduct
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, AmazonProduct $amazonProduct)
	{
		$amazonProduct->description = $request->description;
		$amazonProduct->content = $request->content;
		$amazonProduct->status = $request->status;
		$amazonProduct->save();

		return redirect()->route('amazon-product.index')
			->with('primary', 'Updated correctly');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Models\AmazonProduct $amazonProduct
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(AmazonProduct $amazonProduct)
	{
		$amazonProduct->delete();

		return redirect()->route('amazon-product.index')
			->with('dark', 'Deleted successfully');
	}
}
