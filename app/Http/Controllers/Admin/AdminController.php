<?php

namespace Carlos\Amores\Http\Controllers\Admin;

use Carlos\Amores\Http\Controllers\Controller;

class AdminController extends Controller
{
	/**
	 * Create a new controller instance.
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function home()
	{
		return view('admin.home');
	}
}
