<?php

namespace Carlos\Amores\Http\Controllers\Admin;

use Carlos\Amores\Http\Controllers\Controller;
use Carlos\Amores\Models\Role;
use Carlos\Amores\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class UserController extends Controller
{
	public const PAGINATE = 500;
	/**
	 * Private variable.
	 *
	 * @var string
	 */
	private $publicPath;

	/**
	 * Create a new controller instance.
	 *
	 *
	 * @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function __construct()
	{
		if (App::environment(['local', 'staging'])) {
			$this->publicPath = 'public';
		} else {
			$this->publicPath = 'public_html';
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$users = User::with('role')->paginate(self::PAGINATE);
		// return view('admin.user.index', compact('users'));
		return view('super-admin.user.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('super-admin.user.create');
		// return view('admin.user.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$fields = $request->all();
		if ($request->has('editor1')) {
			$fields['description'] = $request->editor1;
			unset($fields['editor1']);
		}
		$fields['password'] = bcrypt($request->password);
		$fields['verified'] = User::USER_NOT_VERIFIED;
		$fields['verification_token'] = User::generateVerificationToken();
		$fields['admin'] = User::USER_REGULAR;
		$fields['role_id'] = Role::DEFAULT;
		$fields['password'] = bcrypt($request->password);
		$user = User::create($fields);
		$this->storeImgForUser($request, $user);
		$this->storeCurriculumVitae($request);

		return redirect()->route('user.index')
			->with('primary', 'User updated correctly');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Models\User $user
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function show(User $user)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Models\User $user
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function edit(User $user)
	{
		$authUser = Auth::user();
		if (!$authUser->isSuperadmin() && $authUser->id !== $user->id) {
			return redirect()->route('cms-home');
		}
		$roles = Role::all();
		if ($authUser->isSuperadmin()) {
			return view('super-admin.user.edit', compact('user', 'roles'));
		} else {
			return view('admin.user.edit', compact('user', 'roles'));
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request   $request
	 * @param \Carlos\Amores\Models\User $user
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function update(Request $request, User $user)
	{
		$fields = $request->all();
		if ($request->has('editor1')) {
			$fields['description'] = $request->editor1;
			unset($fields['editor1']);
		}
		if (!empty($request->password)) {
			$fields['password'] = bcrypt($request->password);
		} else {
			unset($fields['password']);
		}
		if (!Auth::user()->isSuperadmin()) {
			unset($fields['role_id']);
		} else {
			$fields['role_id'] = (int) $request->role_id;
		}
		$user->update($fields);
		$this->storeImgForUser($request, $user);
		$this->storeCurriculumVitae($request);
		if ($user->isSuperadmin()) {
			Cache::forget('user');
		}

		return redirect()->back()->with('primary', 'User updated correctly');
	}

	/**
	 * Store img for user.
	 *
	 * @param Request $request
	 * @param User    $user
	 */
	private function storeImgForUser(Request $request, User $user)
	{
		if ($request->file('image')) {
			$nameImage = $user->full_name_slug . '.' .
				$request->file('image')->extension();
			$usersPath = base_path() . '/' . $this->publicPath . '/img/users/';
			$request->file('image')->move($usersPath, $nameImage);
			$user->image = 'img/users/' . $nameImage;
			$user->save();
		}
	}

	/**
	 * Undocumented function.
	 *
	 * @param Request $request
	 *
	 * @SuppressWarnings(PHPMD)
	 */
	private function storeImgForProduct(Request $request)
	{
		if ($request->file('file')) {
			$imageName = Str::slug($request->name) . '-'
			. Str::slug($request->lastname) . '.'
			. $request->file('file')->getClientOriginalExtension();
			if (env('APP_ENV') == 'local') {
				$request->file('file')
					->move(base_path() . '/public/img/', $imageName);
			} else {
				$request->file('file')
					->move(base_path() . '/public_html/img/', $imageName);
				//$request->file('file')
				//    ->move(base_path() . '/public/img/', $imageName);
			}
		}
	}

	/**
	 * Undocumented function.
	 *
	 * @param Request $request
	 *
	 * @SuppressWarnings(PHPMD.ElseExpression)
	 */
	private function storeCurriculumVitae(Request $request)
	{
		if ($request->file('cv')) {
			$imageName = 'cv-' . Str::slug($request->name) . '-'
			. Str::slug($request->lastname) . '.'
			. $request->file('cv')->getClientOriginalExtension();
			if (env('APP_ENV') == 'local') {
				$request->file('cv')
					->move(base_path() . '/public/', $imageName);
			} else {
				$request->file('cv')
					->move(base_path() . '/public_html/', $imageName);
			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Models\User $user
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(User $user)
	{
		$user->delete();

		return redirect()->route('user.index')
			->with('dark', 'User deleted successfully');
	}
}
