<?php

namespace Carlos\Amores\Http\Controllers\Admin;

use Carlos\Amores\Http\Controllers\Controller;
use Carlos\Amores\Models\Notice;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$notices = Notice::all();

		return view('super-admin.notice.index', compact('notices'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('super-admin.notice.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if ($request->has('editor1')) {
			$request['message'] = $request->editor1;
			unset($request['editor1']);
		}
		Notice::create($request->all());

		return redirect()->route('notice.index')
			->with('success', 'Notice created successfully');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$notice = Notice::where('id', $id)->firstOrFail();

		return view('super-admin.notice.edit', compact('notice'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int                      $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$notice = Notice::where('id', $id)->firstOrFail();
		$notice->name = $request->name;
		$notice->subject = $request->subject;
		if ($request->has('editor1')) {
			$request['message'] = $request->editor1;
			unset($request['editor1']);
		}
		$notice->message = $request->message;
		$notice->type = $request->type;
		$notice->status = $request->status;
		$notice->repeat = $request->repeat;
		$notice->date = $request->date;
		$notice->save();

		return redirect()->route('notice.index')
			->with('primary', 'Notice updated correctly');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$notice = Notice::where('id', $id)->firstOrFail();
		$notice->delete();

		return redirect()->route('notice.index')
			->with('dark', 'Notice deleted successfully');
	}
}
