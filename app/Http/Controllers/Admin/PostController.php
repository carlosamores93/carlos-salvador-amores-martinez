<?php

declare(strict_types=1);

namespace Carlos\Amores\Http\Controllers\Admin;

use Carlos\Amores\Http\Controllers\Controller;
use Carlos\Amores\Models\Post;
use Carlos\Amores\Repositories\PostRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PostController extends Controller
{
	private $postRepository;

	public function __construct(
		PostRepositoryInterface $postRepository
	) {
		$this->postRepository = $postRepository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$posts = $this->postRepository->all();

		return view('super-admin.post.index', compact('posts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('super-admin.post.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request['user_id'] = Auth::user()->id;
		$request['published'] = false;
		$request['description'] = $request['editor1'];
		unset($request['editor1']);

		$validator = Validator::make(
			$request->all(),
			Post::getValidationRules()
		);
		if ($validator->fails()) {
			return redirect(route('post.create'))
				->withErrors($validator)
				->withInput();
		}
		$request['slug'] = Str::slug($request['title']);
		$this->postRepository->create($request->all());
		// Post::create($request->all());
		return redirect()->route('post.index')
			->with('success', 'Post created successfully');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Models\Post $post
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function show(Post $post)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Models\Post $post
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Post $post)
	{
		return view('super-admin.post.edit', compact('post'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request   $request
	 * @param \Carlos\Amores\Models\Post $post
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function update(Request $request, Post $post)
	{
		if ($request->has('published')) {
			if (is_null($post->published_at)) {
				$post->published_at = now();
			}
			$request['published'] = true;
		} else {
			$request['published'] = false;
		}
		$request['description'] = $request['editor1'];
		unset($request['editor1']);
		$validator = Validator::make(
			$request->all(),
			[
				'subtitle' => 'required|max:255',
				'status_id' => 'required|exists:statuses,id',
				'post_type_id' => 'required|exists:post_types,id',
			]
		);
		if ($validator->fails()) {
			return redirect(route('post.edit', $post))
				->withErrors($validator)
				->withInput();
		}
		unset($request['_token'], $request['_method']);
		$this->postRepository->update($request->all(), $post->id);
		// $post->subtitle = $request->subtitle;
		// $post->status_id = $request->status_id;
		// $post->post_type_id = $request->post_type_id;
		// $post->published = $request->published;
		// $post->description = $request->description;
		// $post->save();

		return redirect()->route('post.index')
			->with('primary', 'Post updated correctly');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Models\Post $post
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Post $post)
	{
		// $post->delete();
		$this->postRepository->delete($post->id);

		return redirect()->route('post.index')
			->with('dark', 'Post deleted successfully');
	}
}
