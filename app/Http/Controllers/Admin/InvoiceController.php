<?php

namespace Carlos\Amores\Http\Controllers\Admin;

use Carbon\Carbon;
use Carlos\Amores\Http\Controllers\Controller;
use Carlos\Amores\Lib\InvoiceProcessor;
use Carlos\Amores\Mail\InvoiceMail;
use Carlos\Amores\Models\Invoice;
use Carlos\Amores\Repositories\InvoiceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class InvoiceController extends Controller
{
	private $months;
	private $invoiceRepository;
	private $invoiceProcessor;

	public function __construct(
		InvoiceRepository $invoiceRepository,
		InvoiceProcessor $invoiceProcessor
	) {
		$this->invoiceRepository = $invoiceRepository;
		$this->invoiceProcessor = $invoiceProcessor;
		$this->months = [
			'01' => 'Enero',
			'02' => 'Febrero',
			'03' => 'Marzo',
			'04' => 'Abril',
			'05' => 'Mayo',
			'06' => 'Junio',
			'07' => 'Julio',
			'08' => 'Agosto',
			'09' => 'Septiembre',
			'10' => 'Octubre',
			'11' => 'Noviembre',
			'12' => 'Diciembre',
		];
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$user = Auth::user();
		$now = now();
		$date = [];
		$date['year'] = $now->format('Y');
		$date['month'] = $now->format('m');
		// $month = $now->format('F');
		$month = null;
		$month = $this->months[$date['month']];
		$invoices = $this->invoiceRepository->all();
		$allMonths = $this->months;
		if ($user->isSuperadmin()) {
			return view(
				'super-admin.invoice.index',
				compact('invoices', 'month', 'date', 'allMonths')
			);
		} elseif ($user->isCarlosCueva()) {
			return view(
				'admin.invoice.index',
				compact('invoices', 'month', 'date', 'allMonths')
			);
		}

		return redirect()->route('cms-home');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if (!Auth::user()->isSuperadmin()) {
			return redirect()->route('invoice.index');
		}
		$serviceOne = [
			'service' => $request['service'],
			'description' => $request['service_description'],
			'price' => $request['price'],
		];
		$request['description'] = json_encode([$serviceOne]);
		$invoice = new Invoice();
		$invoice->create($request->all());

		return redirect()->route('invoice.index')
			->with('primary', 'Invoice created correctly');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \Carlos\Amores\Invoice $invoice
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function show(Invoice $invoice)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Carlos\Amores\Invoice $invoice
	 *
	 * @return \Illuminate\Http\Response
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function edit(Invoice $invoice)
	{
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Carlos\Amores\Invoice   $invoice
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Invoice $invoice)
	{
		if (!Auth::user()->isSuperadmin()) {
			return redirect()->route('invoice.index');
		}
		$tax = isset($request["tax_{$invoice->id}_0"]) ? true : false;
		$price = (float) $request["price_{$invoice->id}_0"];
		if ($tax) {
			$price = round($price / 1.21, 3, PHP_ROUND_HALF_UP);
		}
		$serviceOne = [
			'service' => $request["service_{$invoice->id}_0"],
			'description' => $request["service_description_{$invoice->id}_0"],
			'price' => $price,
		];
		$request['description'] = json_encode([$serviceOne]);
		if (isset($request["service_{$invoice->id}_1"])) {
			$tax = isset($request["tax_{$invoice->id}_1"]) ? true : false;
			$price = (float) $request["price_{$invoice->id}_1"];
			if ($tax) {
				$price = round($price / 1.21, 3, PHP_ROUND_HALF_UP);
			}
			$serviceTwo = [
				'service' => $request["service_{$invoice->id}_1"],
				'description' => $request["service_description_{$invoice->id}_1"],
				'price' => $price,
			];
			$request['description'] = json_encode([$serviceOne, $serviceTwo]);
		}
		$invoice->fill($request->all());
		$invoice->save();

		return redirect()->route('invoice.index')
			->with('primary', 'Invoice updated correctly');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \Carlos\Amores\Invoice $invoice
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Invoice $invoice)
	{
		if (!Auth::user()->isSuperadmin()) {
			return redirect()->route('invoice.index');
		}

		$invoice->delete();

		return redirect()->route('invoice.index')
			->with('dark', 'Invoice deleted successfully');
	}

	/**
	 * Show invoice.
	 *
	 * @param string   $nif
	 * @param string   $year
	 * @param string   $month
	 * @param int|null $number
	 *
	 * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
	 */
	public function showInvoice(
		string $nif,
		string $year,
		string $month,
		int $number = null
	) {
		return $this->invoiceProcessor->showInvoice(
			$nif,
			$year,
			$month,
			$number
		);
	}

	/**
	 * Download invoice.
	 *
	 * @param string   $nif
	 * @param string   $year
	 * @param string   $month
	 * @param int|null $number
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function downloadInvoice(
		string $nif,
		string $year,
		string $month,
		int $number = null
	) {
		return $this->invoiceProcessor->downloadInvoice(
			$nif,
			$year,
			$month,
			$number
		);
	}

	/**
	 * Undocumented function.
	 *
	 * @param string $month
	 *
	 * @SuppressWarnings(PHPMD)
	 */
	public function downloadAllInvoice(string $month)
	{
		$year = now()->format('Y');
		// $zipFile = $month . '.zip'; // Directory public
		$zipFile = storage_path('app/public/invoices/' . $year . '/' . $month . '.zip');
		$zip = new \ZipArchive();
		$zip->open($zipFile, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

		$path = storage_path('app/public/invoices/' . $year . '/' . $month);
		$files = new \RecursiveIteratorIterator(
			new \RecursiveDirectoryIterator($path)
		);
		foreach ($files as $name => $file) {
			// We're skipping all subfolders
			if (!$file->isDir()) {
				$filePath = $file->getRealPath();

				// extracting filename with substr/strlen
				// $relativePath = 'invoices/' . substr($filePath, strlen($path) + 1);
				$relativePath = substr($filePath, strlen($path) + 1);

				$zip->addFile($filePath, $relativePath);
			}
		}
		$zip->close();

		return response()->download($zipFile);
	}

	public function sentAllInvoice(string $month)
	{
		try {
			$date = Carbon::createFromFormat('m', $month);
			$chaperra = 'cuevacuevacarlosalberto9@gmail.com';
			$monthNumber = $date->format('m');
			$year = $date->format('Y');
			$monthName = $date->isoFormat('MMMM');
			$subject = 'Generate invoices ' . $date->format('F');
			$chaperra = 'cuevacuevacarlosalberto9@gmail.com';
			$message = null;
			Mail::to($chaperra)->send(
				new InvoiceMail(
					$subject,
					$message,
					$monthName,
					$monthNumber,
					$year
				)
			);

			return redirect()->route('invoice.index')
				->with('primary', 'Invoices sent correctly');
		} catch (\Throwable $th) {
			return redirect()->route('invoice.index')
				->with('danger', 'Error sending invoices');
		}
	}
}
