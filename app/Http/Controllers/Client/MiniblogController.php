<?php

namespace Carlos\Amores\Http\Controllers\Client;

use Carlos\Amores\Http\Controllers\Controller;
use Carlos\Amores\Lib\Constants\Constants;
use Carlos\Amores\Models\Post;

class MiniblogController extends Controller
{
	public function home()
	{
		$posts = Post::published()->latest()->paginate(Constants::SIX);

		return view(
			'client.miniblog.home',
			compact('posts')
		);
	}

	/**
	 * Undocumented function.
	 *
	 * @param string $category
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function category(string $category)
	{
		return view('client.miniblog.category');
	}

	public function post(string $slug)
	{
		$post = Post::published()->forSlug($slug)->firstOrFail();

		return view(
			'client.miniblog.post',
			compact('post')
		);
	}
}
