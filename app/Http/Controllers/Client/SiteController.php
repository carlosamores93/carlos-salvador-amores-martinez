<?php

namespace Carlos\Amores\Http\Controllers\Client;

use Carlos\Amores\Http\Controllers\Controller;
use Carlos\Amores\Models\AmazonProduct;

class SiteController extends Controller
{
	public function home()
	{
		return view('client.home.home');
	}

	public function oldHome()
	{
		$amazonProducts = AmazonProduct::status(true)->get();

		return view(
			'client.home',
			compact('amazonProducts')
		);
	}

	public function christening()
	{
		return view('client.bautizo.home');
	}
}
