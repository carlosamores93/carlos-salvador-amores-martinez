<?php

namespace Carlos\Amores\Http\Controllers\Client;

use Carbon\Carbon;
use Carlos\Amores\Http\Controllers\Controller;
use Carlos\Amores\Models\MiniSkill;
use Carlos\Amores\Models\Skill;
use Carlos\Amores\Models\User;
use Carlos\Amores\Models\Work;
use Illuminate\Support\Facades\Cache;
use PDF;

class CurriculumVitaeController extends Controller
{
	/**
	 *  @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function index()
	{
		$works = $this->getAllWorks();
		$skills = $this->getAllSkills();
		$miniskills = $this->getAllMiniSkills();
		$user = $this->getUser();
		if (Cache::has('works.all')) {
			return view(
				'client.curriculum-vitae.curriculum-vitae',
				compact('works', 'skills', 'miniskills', 'user')
			);
		} elseif ($this->existsSkillsWorksUser($works, $skills, $miniskills, $user)) {
			$this->parseStartEndDateWorks($works);
			if (env('CACHE_DRIVER') === 'redis') {
				Cache::tags('works')->put('works.all', $works, 3600);
			} else {
				Cache::put('works.all', $works, 3600);
			}

			return view(
				'client.curriculum-vitae.curriculum-vitae',
				compact('works', 'skills', 'miniskills', 'user')
			);
		} else {
			Cache::flush();

			return view('client.curriculum-default.curriculum');
		}
	}

	private function existsSkillsWorksUser($works, $skills, $miniskills, $user)
	{
		return isset($works) && ($works->count() > 0) && isset($skills)
			&& ($skills->count() > 0) && isset($miniskills)
			&& ($miniskills->count() > 0) && isset($user);
	}

	private function getAllWorks()
	{
		$works = Work::where('status', 1)->orderBy('start_date', 'ASC')->get();
		if (Cache::has('works.all')) {
			$works = Cache::get('works.all');
		}

		return $works;
	}

	private function getAllSkills()
	{
		// Verifica automaitcamente si existe, sino lo actualiza
		$skills = Cache::remember('skills.all', 3600, function () {
			return Skill::where('status', 1)->inRandomOrder()->get();
		});

		return $skills;
	}

	/**
	 * Undocumented function.
	 *
	 *
	 *  @SuppressWarnings(PHPMD.ElseExpression)
	 */
	private function getAllMiniSkills()
	{
		if (Cache::has('miniskills.all')) {
			$miniskills = Cache::get('miniskills.all');
		} else {
			$miniskills = MiniSkill::where('status', 1)->orderBy('progress', 'DESC')->get();
			Cache::put('miniskills.all', $miniskills, 3600);
		}

		return $miniskills;
	}

	private function getUser()
	{
		/*if (Cache::has('user')) {
			$user = Cache::get('user');
		}else{
			$user = User::where('id', 1)->first();
			Cache::put('user', $user, 3600);
		}*/
		// $user = Cache::rememberForever('user', function () {
		//     return User::where('id', 1)->first();
		// });
		$days = 86400; // 1 dia <--> 86400 segundos
		$user = Cache::remember(
			'user',
			$days,
			function () {
				return User::where('id', 1)->first();
			}
		);

		return $user;
	}

	/**
	 * Undocumented function.
	 *
	 * @param [type] $works
	 *
	 * @SuppressWarnings(PHPMD.ElseExpression)
	 */
	private function parseStartEndDateWorks(&$works)
	{
		foreach ($works as $w) {
			if (isset($w->start_date)) {
				$w->start_date = ucfirst(
					Carbon::createFromFormat('Y-m-d H:i:s', $w->start_date)
						->locale('es')->isoFormat('MMMM YYYY')
				);
			}
			if (isset($w->end_date)) {
				$w->end_date = ucfirst(
					Carbon::createFromFormat('Y-m-d H:i:s', $w->end_date)
						->locale('es')->isoFormat('MMMM YYYY')
				);
			} else {
				$w->end_date = 'Actualmente';
			}
		}
	}

	/**
	 * Undocumented function.
	 *
	 *
	 * @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function downloadCurriculumVitae()
	{
		if (Cache::has('works.all')) {
			$works = Cache::get('works.all');
		} else {
			$works = Work::where('status', 1)->orderBy('start_date', 'ASC')->get();
			$this->parseStartEndDateWorks($works);
		}
		$skills = $this->getAllSkills();
		$miniskills = $this->getAllMiniSkills();
		$carlos = User::find(1);
		$data = [
			'works' => $works,
			'skills' => $skills,
			'miniskills' => $miniskills,
			'carlos' => $carlos,
		];
		$pdf = PDF::loadView('client.curriculum-vitae.download-cv', $data, [], 'UTF-8');
		$nameFile = User::find(1)->full_name_slug;

		return $pdf->download($nameFile . '.pdf');
	}

	/**
	 * Undocumented function.
	 *
	 *
	 * @SuppressWarnings(PHPMD.ElseExpression)
	 */
	public function viewCurriculumVitae()
	{
		if (Cache::has('works.all')) {
			$works = Cache::get('works.all');
		} else {
			$works = Work::where('status', 1)->orderBy('start_date', 'ASC')->get();
			$this->parseStartEndDateWorks($works);
		}
		$skills = $this->getAllSkills();
		$miniskills = $this->getAllMiniSkills();
		$carlos = User::find(1);
		$download = true;

		return view(
			'client.curriculum-vitae.download-cv',
			compact('carlos', 'works', 'skills', 'miniskills', 'download')
		);
	}
}
