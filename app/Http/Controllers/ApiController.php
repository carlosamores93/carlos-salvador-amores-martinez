<?php

namespace Carlos\Amores\Http\Controllers;

use Carlos\Amores\Traits\ApiResponse;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
class ApiController extends Controller
{
	use ApiResponse;
}
