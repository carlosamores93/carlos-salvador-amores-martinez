<?php

declare(strict_types=1);

namespace Carlos\Amores\Http\Middleware;

use Closure;

class SignatureMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure                 $next
	 * @param strinf                   $header
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next, string $header = 'X-Name')
	{
		$response = $next($request);
		$response->headers->set($header, config('app.name'));

		return $response;
	}
}
