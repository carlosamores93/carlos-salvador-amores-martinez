<?php

namespace Carlos\Amores\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SuperAdministrator
{
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure                 $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (Auth::user()->isSuperadmin()) {
			return $next($request);
		}

		return redirect(route('cms-home'));
	}
}
