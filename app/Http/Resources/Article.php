<?php

namespace Carlos\Amores\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Article extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function toArray($request)
	{
		// return parent::toArray($request);
		return [
			'id' => $this->id,
			'title' => $this->title,
			'body' => $this->body,
		];
	}

	/**
	 * Undocumented function.
	 *
	 * @param [type] $request
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function with($request)
	{
		return [
			'version' => '1.0.0',
			'author_url' => url('http://traversymedia.com'),
		];
	}
}
