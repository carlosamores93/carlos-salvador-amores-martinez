<?php

namespace Carlos\Amores\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
	private const LIMIT_PER_MINUTE = 60;
	/**
	 * This namespace is applied to your controller routes.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'Carlos\Amores\Http\Controllers';

	/**
	 * Define your route model bindings, pattern filters, etc.
	 */
	public function boot()
	{
		$this->configureRateLimiting();
		parent::boot();
	}

	/**
	 * Define the routes for the application.
	 */
	public function map()
	{
		$this->mapApiRoutes();

		$this->mapWebRoutes();
	}

	/**
	 * Define the "web" routes for the application.
	 *
	 * These routes all receive session state, CSRF protection, etc.
	 */
	protected function mapWebRoutes()
	{
		Route::middleware('web')
			 ->namespace($this->namespace)
			 ->group(base_path('routes/web.php'));
	}

	/**
	 * Define the "api" routes for the application.
	 *
	 * These routes are typically stateless.
	 */
	protected function mapApiRoutes()
	{
		Route::prefix('api')
			 ->middleware('api')
			 ->namespace($this->namespace)
			 ->group(base_path('routes/api.php'));
	}

	/**
	 * Configure the rate limiters for the application.
	 */
	protected function configureRateLimiting()
	{
		RateLimiter::for('api', function (Request $request) {
			return Limit::perMinute(self::LIMIT_PER_MINUTE)
				->by($request->user()?->id ?: $request->ip());
		});
	}
}
