<?php

namespace Carlos\Amores\Providers;

//use Illuminate\Support\Facades\Schema;

use Carlos\Amores\Mail\UserCreatedMail;
use Carlos\Amores\Mail\UserMailChangedMail;
use Carlos\Amores\Models\Product;
use Carlos\Amores\Models\User;
use Carlos\Amores\Observers\ProductObserver;
use Carlos\Amores\Observers\UserObserver;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Mongodb\MongodbServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Register any application services.
	 */
	public function register()
	{
		//if ($this->app->isLocal()) {
		if (App::environment(['local', 'staging'])) {
			$this->app->register(MongodbServiceProvider::class);
		}
		$this->app->register(\L5Swagger\L5SwaggerServiceProvider::class);
		$this->app->bind(
			'Carlos\Amores\Repositories\PostRepositoryInterface',
			'Carlos\Amores\Repositories\PostRepository'
		);
	}

	/**
	 * Bootstrap any application services.
	 */
	public function boot()
	{
		// Todas las cadenas de carateres de las migraciones tendrán este tamaño por defecto
		// Se hac eesto para resolver versiones de compatibilidad con versiones anterirores de bbdds
		//Schema::defaultStringLength(191);

		// Deprecated: implement in ProductObserver.php
		// Product::updated(function ($product) {
		//     info('AppServiceProvider -> Product::updated');
		//     // https://laravel.com/docs/5.8/eloquent
		//     if ($product->quantity === 0 && $product->isAvailable()) {
		//         $product->status = Product::PRODUCTO_NO_DISPONIBLE;
		//         $product->save();
		//     }
		// });

		Product::observe(ProductObserver::class);
		User::observe(UserObserver::class);

		User::created(function ($user) {
			info('Print in log');
			retry(5, function () use ($user) {
				// Attempt 5 times while resting 100ms in between attempts...
				Mail::to($user)->send(new UserCreatedMail($user));
			}, 100);
		});

		User::updated(function ($user) {
			info('Print in log when user updated');
			if ($user->isDirty('email')) {
				retry(5, function () use ($user) {
					// Attempt 5 times while resting 100ms in between attempts...
					Mail::to($user)->send(new UserMailChangedMail($user));
				}, 100);
			}
		});
	}
}
