<?php

namespace Carlos\Amores\Providers;

use Carlos\Amores\Models\Invoice;
use Carlos\Amores\Models\Post;
use Carlos\Amores\Policies\InvoicePolicy;
use Carlos\Amores\Policies\PostPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
	/**
	 * The policy mappings for the application.
	 *
	 * @var array
	 */
	protected $policies = [
		Post::class => PostPolicy::class,
		Invoice::class => InvoicePolicy::class,
		// 'Carlos\Amores\Model' => 'Carlos\Amores\Policies\ModelPolicy',
	];

	/**
	 * Register any authentication / authorization services.
	 */
	public function boot()
	{
		$this->registerPolicies();
	}
}
