<?php

namespace Carlos\Amores\Lib;

use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Models\Invoice;
use Carlos\Amores\Repositories\InvoiceRepository;
use Illuminate\Support\Facades\Storage;
use PDF;
use Str;

class InvoiceProcessor
{
	private $invoiceRepository;
	private $months;

	public function __construct(InvoiceRepository $invoiceRepository)
	{
		$this->invoiceRepository = $invoiceRepository;
		$this->months = [
			'01' => 'Enero',
			'02' => 'Febrero',
			'03' => 'Marzo',
			'04' => 'Abril',
			'05' => 'Mayo',
			'06' => 'Junio',
			'07' => 'Julio',
			'08' => 'Agosto',
			'09' => 'Septiembre',
			'10' => 'Octubre',
			'11' => 'Noviembre',
			'12' => 'Diciembre',
		];
	}

	/**
	 * Show invoice.
	 *
	 * @param string $nif
	 * @param int    $year
	 * @param string $month
	 * @param int    $number
	 *
	 * @return View
	 */
	public function showInvoice(
		string $nif,
		int $year,
		string $month,
		int $number = null
	) {
		$invoice = $this->invoiceRepository->getInvoiceByStatusAndNif(
			true,
			$nif
		);
		if (empty($invoice)) {
			return abort(HttpStatusCodes::NOT_FOUND);
		}
		$invoice->description = json_decode($invoice->description, true);
		$invoice->year = $year;
		$invoice->month = $this->months[$month];
		$invoice->monthNumber = $month;
		$invoice->download = true;
		if (!empty($number)) {
			$invoice->number = (int) $number;
		}

		return view('admin.invoice.invoice-pdf', compact('invoice'));
	}

	/**
	 * Download invoice.
	 *
	 * @param string $nif
	 * @param int    $year
	 * @param string $month
	 * @param int    $number
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function downloadInvoice(
		string $nif,
		int $year,
		string $month,
		int $number = null
	) {
		$invoice = $this->invoiceRepository->getInvoiceByStatusAndNif(
			true,
			$nif
		);
		if (empty($invoice)) {
			return abort(HttpStatusCodes::NOT_FOUND);
		}
		$address = $invoice->address;
		$invoice->description = json_decode($invoice->description, true);
		$invoice->month = $this->months[$month];
		$invoice->monthNumber = $month;
		$invoice->year = $year;
		$invoice->download = null;
		if (!empty($number)) {
			$invoice->number = (int) $number;
		}
		$invoice = [
			'invoice' => $invoice,
		];
		$pdf = PDF::loadView('admin.invoice.invoice-pdf', $invoice, [], 'UTF-8');
		$nameFile = Str::slug(
			$address . '-' . $this->months[$month]
				. '-' . $year
		);

		return $pdf->download($nameFile . '.pdf');
	}

	/**
	 * Download all invoice in folder.
	 *
	 * @param string $month
	 * @param string $year
	 */
	public function generateInvoices(string $month, string $year): void
	{
		// $year = date('Y');
		// $lastMonth = today()->subMonth();
		// $year = $lastMonth->format('Y');
		$invoices = $this->invoiceRepository->getInvoicesByStatus(true);
		foreach ($invoices as $invoice) {
			$address = $invoice->address;
			$invoice->description = json_decode($invoice->description, true);
			$invoice->month = $this->months[$month];
			$invoice->monthNumber = $month;
			$invoice->year = $year;
			$invoice->download = null;
			$invoice->number = null;
			$invoice = [
				'invoice' => $invoice,
			];
			$pdf = PDF::loadView('admin.invoice.invoice-pdf', $invoice);
			$nameFile = Str::slug(
				$address . '-' . $this->months[$month]
					. '-' . $year
			);
			Storage::put(
				"public/invoices/{$year}/{$month}/{$nameFile}.pdf",
				$pdf->output()
			);
		}
	}

	/**
	 * Download all invoices.
	 *
	 * @param string $month
	 * @param string $year
	 *
	 * @SuppressWarnings(PHPMD)
	 */
	public function downloadAllInvoice(string $month, string $year)
	{
		if (!file_exists(storage_path('app/public/invoices/' . $year))) {
			mkdir(storage_path('app/public/invoices/' . $year), 0755);
		}
		// $zipFile = $month . '.zip'; // Directory public
		$zipFile = storage_path('app/public/invoices/' . $year . '/' . $month . '.zip');
		$zip = new \ZipArchive();
		$zip->open($zipFile, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

		$path = storage_path('app/public/invoices/' . $year . '/' . $month);
		$files = new \RecursiveIteratorIterator(
			new \RecursiveDirectoryIterator($path)
		);
		foreach ($files as $name => $file) {
			// We're skipping all subfolders
			if (!$file->isDir()) {
				$filePath = $file->getRealPath();

				// extracting filename with substr/strlen
				// $relativePath = 'invoices/' . substr($filePath, strlen($path) + 1);
				$relativePath = substr($filePath, strlen($path) + 1);

				$zip->addFile($filePath, $relativePath);
			}
		}
		$zip->close();

		return response()->download($zipFile);
	}
}
