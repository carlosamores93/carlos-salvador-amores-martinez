<?php

declare(strict_types=1);

namespace Carlos\Amores\Lib\Constants;

class Constants
{
	public const TWO = 2;
	public const FIVE = 5;
	public const SIX = 6;
	public const SIXTY = 60;
}
