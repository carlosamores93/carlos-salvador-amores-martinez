<?php

namespace Carlos\Amores\Lib\Images;

use Intervention\Image\ImageManagerStatic;

class Compress
{
	public const SIZE = 600;
	private $imageManager;

	public function __construct(ImageManagerStatic $imageManager)
	{
		$this->imageManager = $imageManager;
	}

	/**
	 * Compress and return the compressed image.
	 *
	 * @param string $rawFile
	 *
	 * @return string
	 */
	public function getCompressedImage(string $rawFile)
	{
		$photo = $this->imageManager->make($rawFile);

		$photo->resize(self::SIZE, null, function ($constraint) {
			$constraint->aspectRatio();
			$constraint->upsize();
		});

		return (string) $photo->encode('data-url');
	}
}
