<?php

namespace Carlos\Amores\Jobs;

use Carlos\Amores\Mail\TestEmail;
use Exception;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendSimpleEmailJob implements ShouldQueue
{
	use Batchable; // Importante, para Bus::batch...
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	private $data;
	private $failed;

	/**
	 * Create a new job instance.
	 */
	public function __construct(array $data, bool $failed)
	{
		$this->data = $data;
		$this->failed = $failed;
	}

	/**
	 * Execute the job.
	 */
	public function handle()
	{
		if ($this->failed) {
			throw new Exception('Error Processing Request', 1);
		}
		Mail::to('amorescarlos93@gmail.com')->send(new TestEmail($this->data));
	}
}
