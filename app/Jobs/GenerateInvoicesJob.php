<?php

namespace Carlos\Amores\Jobs;

use Carlos\Amores\Lib\InvoiceProcessor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateInvoicesJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	private $year;
	private $month;

	/**
	 * Create a new job instance.
	 */
	public function __construct(string $year, string $month)
	{
		$this->year = $year;
		$this->month = $month;
	}

	/**
	 * Execute the job.
	 */
	public function handle(InvoiceProcessor $invoiceProcessor)
	{
		$invoiceProcessor->generateInvoices($this->month, $this->year);
	}
}
