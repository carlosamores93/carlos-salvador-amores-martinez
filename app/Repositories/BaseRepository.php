<?php

declare(strict_types=1);

namespace Carlos\Amores\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements BaseRepositoryInterface
{
	protected $model;

	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	/**
	 * Get all.
	 *
	 * @return Collection
	 */
	public function all(): Collection
	{
		return $this->model->all();
	}

	/**
	 * Get item.
	 *
	 * @param int $id
	 *
	 * @return Model|null
	 */
	public function show(int $id): ?Model
	{
		return $this->model->find($id);
	}

	/**
	 * Create item.
	 *
	 * @param array $data
	 *
	 * @return Model
	 */
	public function create(array $data): Model
	{
		return $this->model->create($data);
	}

	/**
	 * Find item.
	 *
	 * @param int $id
	 *
	 * @return Model
	 */
	public function find(int $id): ?Model
	{
		return $this->model->find($id);
	}

	/**
	 * Update item.
	 *
	 * @param array $data
	 * @param Model $model
	 */
	public function update(array $data, Model $model)
	{
		$model->update($data);
	}

	/**
	 * Delete item.
	 *
	 * @param Model $model
	 */
	public function delete(Model $model)
	{
		$model->delete();
	}
}
