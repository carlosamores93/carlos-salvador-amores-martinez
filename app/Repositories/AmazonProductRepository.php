<?php

declare(strict_types=1);

namespace Carlos\Amores\Repositories;

use Carlos\Amores\Models\AmazonProduct;

class AmazonProductRepository extends BaseRepository
{
	public function __construct(
		AmazonProduct $amazonProduct
	) {
		parent::__construct($amazonProduct);
	}
}
