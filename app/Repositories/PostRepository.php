<?php

namespace Carlos\Amores\Repositories;

use Carlos\Amores\Models\Post;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PostRepository implements PostRepositoryInterface
{
	protected $model;

	/**
	 * PostRepository constructor.
	 *
	 * @param Post $post
	 */
	public function __construct(Post $post)
	{
		$this->model = $post;
	}

	/**
	 * Undocumented function.
	 *
	 * @return Collection|null
	 */
	public function all()
	{
		return $this->model->all();
	}

	public function create(array $data)
	{
		return $this->model->create($data);
	}

	public function update(array $data, int $id)
	{
		return $this->model->where('id', $id)
			->update($data);
	}

	public function delete(int $id)
	{
		return $this->model->destroy($id);
	}

	public function find(int $id)
	{
		$post = $this->model->find($id);
		if (empty($post)) {
			throw new ModelNotFoundException('Post not found');
		}

		return $post;
	}
}
