<?php

declare(strict_types=1);

namespace Carlos\Amores\Repositories;

use Carlos\Amores\Models\Invoice;
use Illuminate\Database\Eloquent\Collection;

class InvoiceRepository extends BaseRepository
{
	/**
	 * Construct.
	 *
	 * @param Invoice $invoice
	 */
	public function __construct(Invoice $invoice)
	{
		parent::__construct($invoice);
	}

	/**
	 * Get invoices.
	 *
	 * @return Collection|null
	 */
	public function getInvoices(): ? Collection
	{
		return $this->model->forStatus(true)->orderBy('address')->get();
	}

	/**
	 * Get active invoices.
	 *
	 * @param bool $status
	 *
	 * @return Collection|null
	 */
	public function getInvoicesByStatus(bool $status): ?Collection
	{
		return $this->model->forStatus($status)->get();
	}

	/**
	 * Get invoice filter by status and nif,.
	 *
	 * @param bool   $status
	 * @param string $nif
	 *
	 * @return Invoice|null
	 */
	public function getInvoiceByStatusAndNif(bool $status, string $nif): ?Invoice
	{
		return $this->model->forStatus($status)->forNif($nif)->first();
	}
}
