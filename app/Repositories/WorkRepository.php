<?php

namespace Carlos\Amores\Repositories;

use Carlos\Amores\Models\Work;
use Illuminate\Database\Eloquent\Collection;

class WorkRepository extends BaseRepository
{
	public function __construct(
		Work $work
	) {
		parent::__construct($work);
	}

	/**
	 * Get active works.
	 *
	 * @return Collection
	 */
	public function getActiveWorks(): Collection
	{
		return $this->model->activeWork()->orderBy('start_date', 'DESC')->get();
	}

	/**
	 * Get works.
	 *
	 * @return Collection
	 */
	public function getWorks(): Collection
	{
		return $this->model->orderBy('start_date', 'DESC')->get();
	}
}
