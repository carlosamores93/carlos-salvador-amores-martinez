<?php

namespace Carlos\Amores\Repositories;

use Carlos\Amores\Models\MiniSkill;
use Illuminate\Database\Eloquent\Collection;

class MiniSkillRepository extends BaseRepository
{
	public function __construct(
		MiniSkill $miniSkill
	) {
		parent::__construct($miniSkill);
	}

	/**
	 * Get active skills.
	 *
	 * @return Collection
	 */
	public function getActiveMiniSkills(): Collection
	{
		return $this->model->forStatus(true)->get();
	}
}
