<?php

namespace Carlos\Amores\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface BaseRepositoryInterface
{
	public function all(): Collection;

	public function show(int $id);

	public function find(int $id): ?Model;

	public function create(array $data): Model;

	public function update(array $data, Model $model);

	public function delete(Model $model);
}
