************************************************************************************
Patron REPOSITORIO

No es obligatorio, es utilazo para aplicaciones escalables y de gran tamaño

Es una capa de intermediación entre la app y los datos

APP <-> Repositorio <---> BBDD


SE USA PARA INTERACTUAR CON LA BBDD

https://medium.com/@cesiztel/repository-pattern-en-laravel-f66fcc9ea492

************************************************************************************
Patron DECORADOR

Decora objetos, como por ejemplo guardar en cache las respuestas del repositorio

https://www.udemy.com/course/dominando-laravel-de-principiante-a-experto/learn/lecture/6703154#overview


Hace uso de las interfaces