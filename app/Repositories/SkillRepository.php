<?php

namespace Carlos\Amores\Repositories;

use Carlos\Amores\Models\Skill;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Str;

class SkillRepository extends BaseRepository
{
	public function __construct(
		Skill $skill
	) {
		parent::__construct($skill);
	}

	public function getSkills()
	{
		return Skill::all();
	}

	public function getSkill($id)
	{
		return Skill::where('id', $id)->firstOrFail();
	}

	public function storeSkill(Request $request)
	{
		$request['slug'] = Str::slug($request->title);
		Skill::create($request->all());
	}

	public function destroySkill($id)
	{
		//$skill = Skill::where('id', $id)->firstOrFail();
		$skill = $this->getSkill($id);
		$skill->delete();

		return $skill;
	}

	public function updateSkill(Request $request, $id)
	{
		//$skill = Skill::where('id', $id)->firstOrFail();
		$skill = $this->getSkill($id);
		$skill->title = $request->title;
		$skill->slug = Str::slug($request->title);
		$skill->status = $request->status;
		$skill->description = $request->description;
		$skill->save();

		return $skill;
	}

	/**
	 * Get active skills.
	 *
	 * @return Collection
	 */
	public function getActiveSkills(): Collection
	{
		return Skill::forStatus(true)->get();
	}
}
