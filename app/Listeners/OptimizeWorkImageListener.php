<?php

namespace Carlos\Amores\Listeners;

use Carlos\Amores\Events\WorkSavedEvent;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class OptimizeWorkImageListener
{
	private const WIDTH = 400;
	private const LIMIT_COLORS = 255;

	/**
	 * Create the event listener.
	 */
	public function __construct()
	{
	}

	/**
	 * Handle the event.
	 *
	 * @param WorkSavedEvent $event
	 */
	public function handle(WorkSavedEvent $event)
	{
		$work = $event->work;
		$pathImage = $work->image;
		if (empty($pathImage)) {
			return;
		}
		$image = Image::make(Storage::get(str_replace('storage', 'public', $pathImage)))
			->widen(self::WIDTH)
			->limitColors(self::LIMIT_COLORS)
			->encode();
		Storage::put(str_replace('storage', 'public', $pathImage), (string) $image);
	}
}
