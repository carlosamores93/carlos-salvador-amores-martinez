<?php

namespace Carlos\Amores\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class SellerScope implements Scope
{
	/**
	 * Execute scope.
	 *
	 * @param Builder $builder
	 * @param Model   $model
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function apply(Builder $builder, Model $model)
	{
		$builder->has('products');
	}
}
