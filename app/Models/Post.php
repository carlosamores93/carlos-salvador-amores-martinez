<?php

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends BaseModel
{
	use SoftDeletes;

	protected $fillable = [
		'title',
		'slug',
		'subtitle',
		'photo',
		'description',
		'published',
		'user_id',
		'status_id',
		'post_type_id',
		'published_at',
	];

	protected $dates = ['deleted_at'];

	/**
	 * Get validation rules.
	 *
	 * @return array
	 */
	public static function getValidationRules(): array
	{
		return [
			'title' => 'required|unique:posts|max:255',
			'subtitle' => 'required|max:255',
			'user_id' => 'required|exists:users,id',
			'status_id' => 'required|exists:statuses,id',
			'post_type_id' => 'required|exists:post_types,id',
		];
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function postType()
	{
		return $this->belongsTo(PostType::class);
	}

	public function status()
	{
		return $this->belongsTo(Status::class);
	}

	/**
	 * Scope a query to only include active posts.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeActive($query)
	{
		return $query->where('status_id', Status::ACTIVE);
	}

	/**
	 * Scope a query to only include user's posts.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @param int                                   $userId
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeMyPosts($query, int $userId)
	{
		return $query->where('user_id', $userId);
	}

	/**
	 * Scope a query post with slug.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @param string                                $slug
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeForSlug($query, string $slug)
	{
		return $query->where('slug', $slug);
	}

	/**
	 * Scope a query to only include posts published.
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopePublished($query)
	{
		return $query->where('published', true)
			->where('status_id', Status::ACTIVE);
	}

	/**
	 * Scope for username.
	 *
	 * @param Builder $query
	 * @param string  $name
	 */
	public function scopeForName(Builder $query, string $name): void
	{
		$query->select('posts.*');
		if (!$this->isJoined($query, 'users')) {
			$query->join('users', 'users.id', '=', 'posts.user_id');
		}
		$query->where('users.name', $name);
	}

	/**
	 * Scope for lastname.
	 *
	 * @param Builder $query
	 * @param string  $lastname
	 */
	public function scopeForLastname(Builder $query, string $lastname): void
	{
		$query->select('posts.*');
		if (!$this->hasJoin($query->getQuery(), 'users')) {
			$query->join('users', 'users.id', '=', 'posts.user_id');
		}
		$query->where('users.lastname', $lastname);
	}
}
