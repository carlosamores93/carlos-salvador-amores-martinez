<?php

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
	public const ACTIVE = 1;
	public const ACTIVE_NAME = 'Active';
	public const INACTIVE = 2;
	public const INACTIVE_NAME = 'Inactive';
}
