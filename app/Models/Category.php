<?php

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Category extends Model
{
	use Notifiable;
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $fillable = [
		'name',
		'description',
	];

	protected $hidden = [
		'pivot',
	];

	/**
	 * Una categoria puede tener varios productos.
	 */
	public function products()
	{
		return $this->belongsToMany(Product::class);
	}
}
