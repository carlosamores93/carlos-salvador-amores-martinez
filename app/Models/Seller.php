<?php

namespace Carlos\Amores\Models;

use Carlos\Amores\Scopes\SellerScope;
use Carlos\Amores\Transformers\SellerTransformer;

class Seller extends User
{
	public $transformer = SellerTransformer::class;

	/**
	 * Scope to use, only instaces with products.
	 */
	protected static function boot()
	{
		parent::boot();

		// Static, ya que estamos en un metodo estático
		static::addGlobalScope(new SellerScope());
	}

	public function products()
	{
		return $this->hasMany(Product::class);
	}
}
