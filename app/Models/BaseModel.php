<?php

declare(strict_types=1);

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder as QueryBuilder;

class BaseModel extends Model
{
	/**
	 * Has join.
	 *
	 * @param Builder $builder
	 * @param string  $table
	 *
	 * @return bool
	 */
	public function hasJoin(QueryBuilder $builder, string $table): bool
	{
		if (empty($builder->joins)) {
			return false;
		}
		foreach ($builder->joins as $join) {
			if ($join->table === $table) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Check is joined.
	 *
	 * @param Builder $query
	 * @param string  $table
	 *
	 * @return bool
	 */
	public function isJoined(Builder $query, string $table): bool
	{
		$joins = $query->getQuery()->joins;
		if ($joins == null) {
			return false;
		}
		foreach ($joins as $join) {
			if ($join->table === $table) {
				return true;
			}
		}

		return false;
	}
}
