<?php

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Model;

class Encryption extends Model
{
	protected $fillable = [
		'uuid',
		'title',
		'description',
	];
}
