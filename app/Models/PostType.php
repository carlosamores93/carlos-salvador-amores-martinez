<?php

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Model;

class PostType extends Model
{
	public const DEFAULT = 1;
	public const DEFAULT_NAME = 'Default';
}
