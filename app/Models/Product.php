<?php

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Product extends Model
{
	public const PRODUCTO_DISPONIBLE = 1;
	public const PRODUCTO_NO_DISPONIBLE = 0;

	use Notifiable;
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $fillable = [
		'name',
		'description',
		'quantity',
		'status',
		'image',
		'seller_id',
	];

	protected $hidden = [
		'pivot',
	];

	public function statusProduct()
	{
		return (bool) $this->status;
	}

	public function isAvailable()
	{
		return self::PRODUCTO_DISPONIBLE === (int) $this->status;
	}

	public function isNotAvaible()
	{
		return self::PRODUCTO_NO_DISPONIBLE === (int) $this->status;
	}

	public function categories()
	{
		return $this->belongsToMany(Category::class);
	}

	public function seller()
	{
		return $this->belongsTo(User::class);
	}

	public function transactions()
	{
		return $this->hasMany(Transaction::class);
	}
}
