<?php

declare(strict_types=1);

namespace Carlos\Amores\Models;

use Carlos\Amores\Transformers\UserTransformer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Str;

class User extends Authenticatable
{
	use Notifiable;
	use SoftDeletes;
	use HasFactory;

	public const USER_VERIFIED = '1';
	public const USER_NOT_VERIFIED = '0';

	public const USER_ADMIN = 'true';
	public const USER_REGULAR = 'false';
	public const LENGTH_TOKEN = 40;

	public const CARLOS_CUEVA = 'cuevacuevacarlosalberto9@gmail.com';

	protected $table = 'users';

	protected $dates = ['deleted_at'];

	protected $appends = [
		'full_name',
	];

	public $transformer = UserTransformer::class;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'lastname',
		'email',
		'username',
		'image',
		'password',
		'role_id',
		'description',
		'phone',
		'career',
		'profession',
		'university',
		'faculty',
		'address',
		'github',
		'gitlab',
		'linkedin',
		'website',
		'verified',
		'verification_token',
		'admin',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
		'verification_token',
		'role_id',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	/**
	 * Check if user is verified.
	 *
	 * @return bool
	 */
	public function isVerified(): bool
	{
		return User::USER_VERIFIED === $this->verified;
	}

	/**
	 * Check is admin.
	 *
	 * @return bool
	 */
	public function isAdmin(): bool
	{
		return User::USER_ADMIN === $this->admin;
	}

	/**
	 * Check if is superadmin.
	 *
	 * @return bool
	 */
	public function isSuperadmin(): bool
	{
		return Role::SUPER_ADMIN === (int) $this->role_id;
	}

	/**
	 * Check if is Carlos Cueva.
	 *
	 * @return bool
	 */
	public function isCarlosCueva(): bool
	{
		return self::CARLOS_CUEVA === $this->email;
	}

	public static function generateVerificationToken()
	{
		return Str::random(self::LENGTH_TOKEN);
	}

	// public function setEmailAttribute($value)
	// {
	//     $this->attribute['email'] = strtolower($value);
	//     //$this->attribute['email'] = $value;
	// }

	/**
	 * Accesor para email.
	 */
	public function getEmailAttribute(string $value)
	{
		//return strtoupper($value);
		return strtolower($value);
	}

	/**
	 * Un usuario solo tiene un roll.
	 */
	public function role()
	{
		return $this->belongsTo(Role::class);
	}

	/**
	 * Get full name.
	 */
	public function getFullNameAttribute()
	{
		return $this->name . ' ' . $this->lastname;
	}

	/**
	 * Get slug full name.
	 */
	public function getFullNameSlugAttribute(): string
	{
		return Str::slug($this->name . ' ' . $this->lastname);
	}

	/**
	 * Scope a query to users aren't superadmin.
	 */
	public function scopeForNotSuperAdmin(Builder $query): Builder
	{
		return $query->where('role_id', '!=', Role::SUPER_ADMIN);
	}

	public function posts()
	{
		return $this->hasMany(Post::class);
	}

	/**
	 * Scope for verification token.
	 */
	public function scopeForVerificationToken(Builder $query, string $token): Builder
	{
		return $query->where('verification_token', $token);
	}

	public function csamLogs()
	{
		return $this->morphMany(CsamLog::class, 'product');
	}
}
