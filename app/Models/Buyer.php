<?php

namespace Carlos\Amores\Models;

use Carlos\Amores\Scopes\BuyerScope;
use Carlos\Amores\Transformers\BuyerTransformer;

class Buyer extends User
{
	public $transformer = BuyerTransformer::class;

	/**
	 * Said scope to use, only instaces with transactions.
	 */
	protected static function boot()
	{
		parent::boot();

		// Static, ya que estamos en un metodo estático
		static::addGlobalScope(new BuyerScope());
	}

	/**
	 * Un comprador tiene muchas transacciones.
	 */
	public function transactions()
	{
		return $this->hasMany(Transaction::class);
	}
}
