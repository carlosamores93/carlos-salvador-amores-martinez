<?php

declare(strict_types=1);

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class House extends Model
{
	use SoftDeletes;
	public const PRICE_SLOT = 20000;

	protected $fillable = [
		'direccion',
		'numero',
		'piso',
		'puerta',
		'precio',
		'superficie',
		'precio_m_cuadrado',
		'habitaciones',
		'terraza',
		'cocina',
		'banio',
		'salon',
		'trastero',
		'ascensor',
		'map',
		'link_ad',
		'catastro',
		'like',
		'fecha_construccion',
		'estado_cargas',
		'inspecccion_tecnica_edificios',
		'estado_fontaneria_electricidad',
		'comunidad_propietarios',
		'obras_previstas_edificio',
		'calefaccion',
		'description',
		'created_at',
		'updated_at',
	];

	protected $dates = ['deleted_at'];

	/**
	 * Get filters.
	 */
	public function getFilters(): array
	{
		return [
			// Scope        Filter
			'like' => 'like',
		];
	}

	/**
	 * Undocumented function.
	 *
	 * @param QueryBuilder $query
	 */
	public function scopeLike(Builder $query, bool $like)
	{
		return $query->where('like', $like);
	}

	/**
	 * Undocumented function.
	 *
	 * @return Builder
	 */
	public function scopePiso(Builder $query, int $piso)
	{
		$query->where('piso', $piso);
	}

	/**
	 * Undocumented function.
	 *
	 * @return Builder
	 */
	public function scopeCalle(Builder $query, string $calle)
	{
		$query->where('direccion', 'like', "%{$calle}%");
	}

	/**
	 * Scope price.
	 *
	 * @return Builder
	 */
	public function scopePrecio(Builder $query, int $precio)
	{
		$query->whereBetween(
			'precio',
			[
				$precio - self::PRICE_SLOT,
				$precio + self::PRICE_SLOT,
			]
		);
	}
}
