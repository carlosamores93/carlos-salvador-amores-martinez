<?php

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notice extends Model
{
	use SoftDeletes;

	public const TYPES = [
		'birthday',
		'notice',
	];

	protected $fillable = [
		'name', 'subject', 'type', 'message', 'status', 'repeat', 'date',
	];
}
