<?php

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AmazonProduct extends Model
{
	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'description',
		'content',
		'status',
	];

	/**
	 * Filter status.
	 *
	 * @param Builder $query
	 * @param bool    $status
	 *
	 * @return Builder
	 */
	public function scopeStatus(Builder $query, bool $status)
	{
		return $query->where('status', $status);
	}
}
