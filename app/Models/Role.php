<?php

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Role class.
 *
 * @author Carlos Amores <amorecarlos93@hotmail.com>
 */
class Role extends Model
{
	public const SUPER_ADMIN = 1;
	public const SUPER_ADMIN_NAME = 'Super Administrator';
	public const ADMIN = 2;
	public const ADMIN_NAME = 'Administrator';
	public const DEFAULT = 3;
	public const DEFAULT_NAME = 'Default';

	/**
	 * Un rol puede tener varios usuarios.
	 */
	public function users()
	{
		return $this->hasMany(User::class);
	}
}
