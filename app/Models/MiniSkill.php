<?php

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MiniSkill extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'title', 'progress', 'status',
	];

	protected $casts = [
		'status' => 'boolean',
	];

	/**
	 * Scope for status.
	 *
	 * @param Builder $query
	 * @param bool    $status
	 *
	 * @return Builder
	 */
	public function scopeForStatus(Builder $query, bool $status): Builder
	{
		return $query->where('status', $status);
	}
}
