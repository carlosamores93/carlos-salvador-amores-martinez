<?php

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CsamLog extends Model
{
	use HasFactory;

	protected $fillable = [
		'product_type',
		'product_id',
		'user_id',
		'value',
		'created_at',
		'updated_at',
	];

	/**
	 * Get the owning commentable model.
	 */
	public function product()
	{
		return $this->morphTo();
	}
}
