<?php

declare(strict_types=1);

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
	use SoftDeletes;

	public const TYPES = [
		'urgent',
		'relax',
	];

	/**
	 * The attributes that should be cast.
	 *
	 * @var array
	 */
	protected $casts = [
		'categories' => 'array',
	];

	protected $fillable = [
		'name', 'categories', 'status', 'number',
	];

	protected $dates = ['deleted_at'];

	/**
	 * Get the user's first name.
	 *
	 * @param string $value
	 *
	 * @return mixed
	 */
	public function getCategoriesAttribute($value)
	{
		return json_decode($value);
	}
}
