<?php

namespace Carlos\Amores\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
	use SoftDeletes;
	use HasFactory;

	protected $dates = ['deleted_at'];

	protected $fillable = [
		'id',
		'address',
		'status',
		'city',
		'nif',
		'description',
	];

	/**
	 * Scope for status.
	 *
	 * @param Builder $query
	 * @param bool    $status
	 *
	 * @return Builder
	 */
	public function scopeForStatus(Builder $query, bool $status): Builder
	{
		return $query->where('status', $status);
	}

	/**
	 * Scope for nif.
	 *
	 * @param Builder $query
	 * @param string  $nif
	 *
	 * @return Builder
	 */
	public function scopeForNif(Builder $query, string $nif): Builder
	{
		return $query->where('nif', $nif);
	}
}
