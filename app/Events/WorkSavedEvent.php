<?php

namespace Carlos\Amores\Events;

use Carlos\Amores\Models\Work;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class WorkSavedEvent
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	public $work;

	/**
	 * Create the event listener.
	 *
	 * @param Work $work
	 */
	public function __construct(Work $work)
	{
		$this->work = $work;
	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return \Illuminate\Broadcasting\Channel|array
	 */
	public function broadcastOn()
	{
		return new PrivateChannel('channel-name');
	}
}
