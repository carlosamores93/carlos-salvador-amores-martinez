<?php

namespace Carlos\Amores\Transformers;

use Carlos\Amores\Models\Seller;
use League\Fractal\TransformerAbstract;

class SellerTransformer extends TransformerAbstract
{
	/**
	 * List of resources to automatically include.
	 *
	 * @var array
	 */
	protected $defaultIncludes = [
	];

	/**
	 * List of resources possible to include.
	 *
	 * @var array
	 */
	protected $availableIncludes = [
	];

	/**
	 * A Fractal transformer.
	 *
	 * @return array
	 */
	public function transform(Seller $seller)
	{
		return [
			'Id' => (int) $seller->id,
			'Name' => $seller->name,
			'LastName' => $seller->lastname,
			'FullName' => $seller->full_name,
			'Email' => $seller->email,
			'Verificado' => (int) $seller->verified,
			'FechaCreacion' => $seller->created_at,
			'FechaActualizacion' => $seller->updated_at,
			'FechaBorrado' => $seller->deleted_at,
		];
	}
}
