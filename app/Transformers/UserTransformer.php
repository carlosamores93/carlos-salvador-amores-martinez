<?php

declare(strict_types=1);

namespace Carlos\Amores\Transformers;

use Carlos\Amores\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
	/**
	 * List of resources to automatically include.
	 *
	 * @var array
	 */
	protected array $defaultIncludes = [
	];

	/**
	 * List of resources possible to include.
	 *
	 * @var array
	 */
	protected array $availableIncludes = [
	];

	/**
	 * A Fractal transformer.
	 *
	 * @return array
	 */
	public function transform(User $user): array
	{
		return [
			'Id' => (int) $user->id,
			'Name' => $user->name,
			'LastName' => $user->lastname,
			'FullName' => $user->full_name,
			'Email' => $user->email,
			'Phone' => $user->phone,
			'FechaBorrado' => $user->deleted_at,
		];
	}

	/**
	 * Return original attribute.
	 *
	 * @param string $index
	 *
	 * @return string|null
	 */
	public static function originalAttribute(string $index): ? string
	{
		$attrbitutes = [
			'Id' => 'id',
			'Name' => 'name',
			'LastName' => 'lastname',
			'FullName' => 'full_name',
			'Email' => 'email',
			'Phone' => 'phone',
			'FechaBorrado' => 'deleted_at',
		];

		return isset($attrbitutes[$index]) ? $attrbitutes[$index] : null;
	}
}
