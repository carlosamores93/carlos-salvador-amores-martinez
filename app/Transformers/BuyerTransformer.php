<?php

namespace Carlos\Amores\Transformers;

use Carlos\Amores\Models\Buyer;
use League\Fractal\TransformerAbstract;

class BuyerTransformer extends TransformerAbstract
{
	/**
	 * List of resources to automatically include.
	 *
	 * @var array
	 */
	protected $defaultIncludes = [
	];

	/**
	 * List of resources possible to include.
	 *
	 * @var array
	 */
	protected $availableIncludes = [
	];

	/**
	 * A Fractal transformer.
	 *
	 * @return array
	 */
	public function transform(Buyer $buyer)
	{
		return [
			'Id' => (int) $buyer->id,
			'Name' => $buyer->name,
			'LastName' => $buyer->lastname,
			'FullName' => $buyer->full_name,
			'Email' => $buyer->email,
			'Verificado' => (int) $buyer->verified,
			'FechaCreacion' => $buyer->created_at,
			'FechaActualizacion' => $buyer->updated_at,
			'FechaBorrado' => $buyer->deleted_at,
		];
	}
}
