<?php

namespace Carlos\Amores\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceMail extends Mailable
{
	use Queueable, SerializesModels;

	public $subject;
	public $message;
	public $month;
	public $monthNumber;
	public $year;

	/**
	 * Create a new message instance.
	 *
	 * @param string $subject
	 * @param string $message
	 * @param string $month
	 * @param string $monthNumber
	 */
	public function __construct(
		string $subject,
		string $message = null,
		string $month,
		string $monthNumber,
		string $year
	) {
		$this->subject = $subject;
		$this->message = $message;
		$this->month = $month;
		$this->monthNumber = $monthNumber;
		$this->year = $year;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		$address = env('MAIL_FROM_ADDRESS');
		$name = env('MAIL_FROM_NAME');
		// $year = now()->format('Y');
		return $this->markdown('emails.invoice')
			->attach(storage_path("app/public/invoices/{$this->year}/{$this->monthNumber}.zip"))
			->from($address, $name)
			->subject($this->subject)
			->with(
				[
					'month' => $this->month,
					'message' => $this->message,
				]
			);
	}
}
