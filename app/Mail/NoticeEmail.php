<?php

namespace Carlos\Amores\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NoticeEmail extends Mailable
{
	use Queueable, SerializesModels;

	public $subject;
	public $message;

	/**
	 * Create a new message instance.
	 */
	public function __construct($subject, $message)
	{
		$this->subject = $subject;
		$this->message = $message;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		$address = env('MAIL_FROM_ADDRESS');
		$name = env('MAIL_FROM_NAME');

		return $this->markdown('emails.notice')
			// ->attach(storage_path('app/public/invoices/06.zip'))
			->from($address, $name)
			// ->cc($address, $name)
			// ->bcc($address, $name)
			// ->replyTo($address, $name)
			->subject($this->subject)
			->with(
				[
					'message' => $this->message,
				]
			);
	}
}
