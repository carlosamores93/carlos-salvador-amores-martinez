<?php

declare(strict_types=1);

namespace Carlos\Amores\Mail;

use Carlos\Amores\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserCreatedMail extends Mailable
{
	use Queueable, SerializesModels;

	public $user;

	/**
	 * Create a new message instance.
	 */
	public function __construct(User $user)
	{
		$this->user = $user;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		// return $this->text('emails.welcome')
		return $this->markdown('emails.welcome')
			->subject('Por favor confirma tu email');
		// return $this->view('view.name');
	}
}
