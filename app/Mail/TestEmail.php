<?php

namespace Carlos\Amores\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TestEmail extends Mailable
{
	use Queueable, SerializesModels;

	public $data;

	/**
	 * Create a new message instance.
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		// dd($this->data['message']);
		// return $this->view('view.name');
		// $address = 'amorescarlos93@hotmail.com';
		$address = env('MAIL_FROM_ADDRESS');

		$subject = 'This is a demo!';
		// $name = 'Carlos Amores';
		$name = env('MAIL_FROM_NAME');

		return $this->view('emails.test')
		// return $this->markdown('emails.notice')
			->from($address, $name)
			->cc($address, $name)
			->bcc($address, $name)
			->replyTo($address, $name)
			->subject($subject)
			->with(['test_message' => $this->data['message']]);
	}
}
