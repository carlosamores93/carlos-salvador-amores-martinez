<?php

//https://desarrollowp.com/blog/tutoriales/declara-strict-types-en-tus-ficheros-php/
declare(strict_types=1);

namespace Carlos\Amores\Others\DataTransferObject;

use Carlos\Amores\Models\Invoice;

class InvoiceDto
{
	private $address;
	private $city;
	private $nif;
	private $description;

	public function __construct(Invoice $invoice)
	{
		$this->address = $invoice->address;
		$this->city = $invoice->city;
		$this->nif = $invoice->nif;
		$this->setDescription($invoice->description);
	}

	private function setDescription(string $description = null)
	{
		$descriptionArray = json_decode($description, true);
		foreach ($descriptionArray as &$value) {
			$value['totalPrice'] = number_format(
				$value['price'] * 1.21,
				2,
				'.',
				''
			);
			// $value['totalPrice'] = round(
			//     $value['price'] * 1.21,
			//     2
			// );
		}
		$this->description = $descriptionArray;
	}

	public function getAddress()
	{
		return $this->address;
	}

	public function getCity()
	{
		return $this->city;
	}

	public function getNif()
	{
		return $this->nif;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function getDto()
	{
		return [
			'address' => $this->address,
			'city' => $this->city,
			'nif' => $this->nif,
			'description' => $this->description,
		];
	}
}
