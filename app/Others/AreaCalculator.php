<?php

interface AreaCalculator
// abstract class AreaCalculator
{
	/**
	 * Calculates the area of a polygon.
	 *
	 * @return int
	 */
	public function calculateArea(): int;
	// abstract public function calculateArea(): int;
}

class Circle implements AreaCalculator
{
	private $radio;

	public function __construct(int $radio)
	{
		$this->radio = $radio;
	}

	public function calculateArea(): int
	{
		return pi() * $this->radio * $this->radio;
	}
}

class Triangle implements AreaCalculator
{
	private $b;
	private $a;

	public function __construct(int $b, int $a)
	{
		$this->a = $a;
		$this->b = $b;
	}

	public function calculateArea(): int
	{
		return ($this->b * $this->a) / 2;
	}
}

class Square implements AreaCalculator
{
	private $l;

	public function __construct(int $l)
	{
		$this->l = $l;
	}

	public function calculateArea(): int
	{
		return $this->l * $this->l;
	}
}

class Calculator
{
	/**
	 * Prints the area of the polygon given.
	 *
	 * @param AreaCalculator $poly
	 *
	 * @return string
	 */
	public function run(AreaCalculator $poly)
	{
		// info(get_class($poly) . ' = ' . $poly->calculateArea() . 'm²');
		var_dump(get_class($poly) . ' = ' . $poly->calculateArea() . 'm²');
	}
}

$calculator = new Calculator();

$triangle = new Triangle(10, 20);
$calculator->run($triangle);

$circle = new Circle(10);
$calculator->run($circle);

$square = new Square(9);
$calculator->run($square);

// Run
// php path/AreaCalculator.php
