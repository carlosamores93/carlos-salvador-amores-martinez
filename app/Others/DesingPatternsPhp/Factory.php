<?php

abstract class ClaimCreator
{
	abstract public function getClaimCreator(): ClaimCreatorInterface;

	public function createClaim(array $request): string
	{
		$claimCreator = $this->getClaimCreator();

		return $claimCreator->create($request);
	}
}

class AegonClaimCreator extends ClaimCreator
{
	public function getClaimCreator(): ClaimCreatorInterface
	{
		return new AegonCreator();
	}
}

class CaserClaimCreator extends ClaimCreator
{
	public function getClaimCreator(): ClaimCreatorInterface
	{
		return new CaserCreator();
	}
}

interface ClaimCreatorInterface
{
	public function create(array $request): string;
}

class AegonCreator implements ClaimCreatorInterface
{
	public function create(array $request): string
	{
		return "{Result of the AegonCreator} with claim_number: {$request['claim_number']}";
	}
}

class CaserCreator implements ClaimCreatorInterface
{
	public function create(array $request): string
	{
		return "{Result of the CaserCreator} with claim_number: {$request['claim_number']}";
	}
}

// AEGON
$request = [
	'claim_number' => 123456,
	'company' => 'Aegon',
];
$nameClass = $request['company'] . 'ClaimCreator';
$companyCreator = new $nameClass();
$resp = $companyCreator->createClaim($request);
echo "{$resp}\n\n";

// CASER
$request = [
	'claim_number' => 654321,
	'company' => 'Caser',
];
$nameClass = $request['company'] . 'ClaimCreator';
$companyCreator = new $nameClass();
$resp = $companyCreator->createClaim($request);
echo "{$resp}\n\n";

// echo "App: Launched with the CaserClaimCreator.\n";
// $caserCreator = new CaserClaimCreator();
// $resp = $caserCreator->createClaim($request);
// echo "{$resp}\n";

// Run
// php path/Factory.php
