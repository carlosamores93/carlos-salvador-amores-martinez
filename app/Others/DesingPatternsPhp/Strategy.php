<?php

interface Estrategia
{
	public function hacerReporte();
}

class ReporteBarra implements Estrategia
{
	private $datos;
	public function __construct($datos)
	{
		$this->datos = $datos;
	}

	public function hacerReporte()
	{
		var_dump('Grafica de barra con los Datos');
	}
}

class ReporteExportacionExcel implements Estrategia
{
	private $datos;

	public function __construct($datos)
	{
		$this->datos = $datos;
	}

	public function hacerReporte()
	{
		var_dump('Excel con los Datos');
	}
}

class ReportePdf implements Estrategia
{
	private $datos;

	public function __construct($datos)
	{
		$this->datos = $datos;
	}

	public function hacerReporte()
	{
		var_dump('PDF con los Datos');
	}
}

class Reporte
{
	private $estrategia;
	private $datos;

	public function __construct($datos)
	{
		$this->datos = $datos;
	}

	public function setEstrategia(Estrategia $estrategia)
	{
		$this->estrategia = $estrategia;
	}

	public function ejecutar()
	{
		$this->estrategia->hacerReporte();
	}
}

$datos = [];
$reporte = new Reporte($datos);

$reporteBarra = new ReporteBarra($datos);
$reporte->setEstrategia($reporteBarra);
$reporte->ejecutar();

$reporteExcel = new ReporteExportacionExcel($datos);
$reporte->setEstrategia($reporteExcel);
$reporte->ejecutar();

$reportePdf = new ReportePdf($datos);
$reporte->setEstrategia($reportePdf);
$reporte->ejecutar();

echo "\n";
echo "\n";
echo "\n";
echo "\n";

/**
 * The Strategy interface declares operations common to all supported versions
 * of some algorithm.
 *
 * The Context uses this interface to call the algorithm defined by Concrete
 * Strategies.
 */
interface Strategy
{
	public function doAlgorithm(array $data): array;
}

/**
 * Concrete Strategies implement the algorithm while following the base Strategy
 * interface. The interface makes them interchangeable in the Context.
 */
class ConcreteStrategyA implements Strategy
{
	public function doAlgorithm(array $data): array
	{
		sort($data);

		return $data;
	}
}

class ConcreteStrategyB implements Strategy
{
	public function doAlgorithm(array $data): array
	{
		rsort($data);

		return $data;
	}
}

/**
 * The Context defines the interface of interest to clients.
 */
class Context
{
	private $data;
	/**
	 * @var Strategy The Context maintains a reference to one of the Strategy
	 *               objects. The Context does not know the concrete class of a strategy. It
	 *               should work with all strategies via the Strategy interface.
	 */
	private $strategy;

	/**
	 * Usually, the Context accepts a strategy through the constructor, but also
	 * provides a setter to change it at runtime.
	 */
	public function __construct(Strategy $strategy, array $data)
	{
		$this->strategy = $strategy;
		$this->data = $data;
	}

	/**
	 * Usually, the Context allows replacing a Strategy object at runtime.
	 */
	public function setStrategy(Strategy $strategy)
	{
		$this->strategy = $strategy;
	}

	/**
	 * The Context delegates some work to the Strategy object instead of
	 * implementing multiple versions of the algorithm on its own.
	 */
	public function doSomeBusinessLogic(): void
	{
		// ...

		echo "Context: Sorting data using the strategy (not sure how it'll do it)\n";
		$result = $this->strategy->doAlgorithm($this->data);
		echo implode(',', $result) . "\n";

		// ...
	}
}

/**
 * The client code picks a concrete strategy and passes it to the context. The
 * client should be aware of the differences between strategies in order to make
 * the right choice.
 */
$data = ['a', 'b', 'c', 'd', 'e'];
$context = new Context(new ConcreteStrategyA(), $data);
echo "Client: Strategy is set to normal sorting.\n";
$context->doSomeBusinessLogic();

echo "\n";

echo "Client: Strategy is set to reverse sorting.\n";
$context->setStrategy(new ConcreteStrategyB());
$context->doSomeBusinessLogic();

// Run
// php path/ChainOfResponsibilities.php
// Example: https://fcarrizalest.com/content/patr%C3%B3n-de-dise%C3%B1o-estrategia-strategy-pattern-en-php
// Example: https://refactoring.guru/es/design-patterns/strategy/php/example
