<?php

abstract class HomeChecker
{
	protected $successor;

	abstract public function check(HomeStatus $home);

	public function succeedWith(HomeChecker $successor)
	{
		$this->successor = $successor;
	}

	public function next(HomeStatus $home)
	{
		if ($this->successor) {
			$this->successor->check($home);
		}
	}
}

class Locks extends HomeChecker
{
	public function check(HomeStatus $home)
	{
		if (!$home->locked) {
			// throw new Exception('The door are not locked.');
			var_dump('The door are not locked.');
		}
		$this->next($home);
	}
}

class Lights extends HomeChecker
{
	public function check(HomeStatus $home)
	{
		if (!$home->lightsOff) {
			// throw new Exception('The lights are still on.');
			var_dump('The lights are still on.');
		}
		$this->next($home);
	}
}

class Alarm extends HomeChecker
{
	public function check(HomeStatus $home)
	{
		// print_r($home->alarmOn);
		if (!$home->alarmOn) {
			// throw new Exception('The alarm  has not set.');
			var_dump('The alarm  has not set.');
		}
		$this->next($home);
	}
}

class HomeStatus
{
	public $alarmOn = false;
	public $locked = false;
	public $lightsOff = false;
}

$locks = new Locks();
$lights = new Lights();
$alarm = new Alarm();

$locks->succeedWith($lights);
$lights->succeedWith($alarm);

$homeStatus = new HomeStatus();
$locks->check($homeStatus);

abstract class OperationAbstract
{
	protected $operation;

	public function then(OperationAbstract $operation)
	{
		$this->operation = $operation;
	}

	public function next(Transaction $transaction)
	{
		if ($this->operation) {
			$this->operation->process($transaction);
		}
	}

	abstract public function process(Transaction $transaction);
}
class MultipleOfFifty extends OperationAbstract
{
	public function process(Transaction $transaction)
	{
		if ($transaction->amount % 50 !== 0) {
			var_dump('Amount is not multiple 50');

			return;
		}
		$this->next($transaction);
	}
}
class BalanceCheecker extends OperationAbstract
{
	public function process(Transaction $transaction)
	{
		if ($transaction->balance < $transaction->amount) {
			var_dump('Dont have moeny');

			return;
		}
		$this->next($transaction);
	}
}

class FiveHundredBillDispenser extends OperationAbstract
{
	public function process(Transaction $transaction)
	{
		if ($transaction->amount < 500) {
			$this->next($transaction);

			return;
		}
		$bills = intval($transaction->amount / 500);
		var_dump('Bills 500: ' . $bills);
		$remain = $transaction->amount % 500;
		if ($remain === 0) {
			return;
		}
		$transaction->amount = $remain;
		$this->next($transaction);
	}
}

class TwoHundredBillDispenser extends OperationAbstract
{
	public function process(Transaction $transaction)
	{
		if ($transaction->amount < 200) {
			$this->next($transaction);

			return;
		}
		$bills = intval($transaction->amount / 200);
		var_dump('Bills 200: ' . $bills);
		$remain = $transaction->amount % 200;
		if ($remain === 0) {
			return;
		}
		$transaction->amount = $remain;
		$this->next($transaction);
	}
}

class OneHundredBillDispenser extends OperationAbstract
{
	public function process(Transaction $transaction)
	{
		if ($transaction->amount < 100) {
			$this->next($transaction);

			return;
		}
		$bills = intval($transaction->amount / 100);
		var_dump('Bills 100: ' . $bills);
		$remain = $transaction->amount % 100;
		if ($remain === 0) {
			return;
		}
		$transaction->amount = $remain;
		$this->next($transaction);
	}
}

class FiftyBillDispenser extends OperationAbstract
{
	public function process(Transaction $transaction)
	{
		if ($transaction->amount < 50) {
			$this->next($transaction);

			return;
		}
		$bills = intval($transaction->amount / 50);
		var_dump('Bills 50: ' . $bills);
		$remain = $transaction->amount % 50;
		if ($remain === 0) {
			return;
		}
		$transaction->amount = $remain;
		$this->next($transaction);
	}
}

class Transaction
{
	public $amount;
	public $balance;
}

$transaction = new Transaction();
$transaction->amount = 50;
$transaction->balance = 5000;

$multiple = new MultipleOfFifty();
$balanceChecker = new BalanceCheecker();
$fiveHundredBillDispenser = new FiveHundredBillDispenser();
$twoHundredBillDispenser = new TwoHundredBillDispenser();
$oneHundredBillDispenser = new OneHundredBillDispenser();
$fiftyBillDispenser = new FiftyBillDispenser();

$multiple->then($balanceChecker);
$balanceChecker->then($fiveHundredBillDispenser);
$fiveHundredBillDispenser->then($twoHundredBillDispenser);
$twoHundredBillDispenser->then($oneHundredBillDispenser);
$oneHundredBillDispenser->then($fiftyBillDispenser);
$multiple->process($transaction); // 1º

// Run
// php path/ChainOfResponsibilities.php
