<?php

namespace Carlos\Amores\Policies;

use Carlos\Amores\Models\Invoice;
use Carlos\Amores\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class InvoicePolicy
{
	use HandlesAuthorization;

	/**
	 * Determine whether the user can view any invoices.
	 *
	 * @param \Carlos\Amores\Models\User $user
	 *
	 * @return mixed
	 */
	public function viewAny(User $user)
	{
		return $user->isSuperadmin();
	}

	/**
	 * Determine whether the user can view the invoice.
	 *
	 * @param \Carlos\Amores\Models\User    $user
	 * @param \Carlos\Amores\Models\Invoice $invoice
	 *
	 * @return mixed
	 */
	public function view(User $user, Invoice $invoice)
	{
		return !$user->isSuperadmin();
	}

	/**
	 * Determine whether the user can create invoices.
	 *
	 * @param \Carlos\Amores\Models\User $user
	 *
	 * @return mixed
	 */
	public function create(User $user)
	{
		return $user->isSuperadmin();
	}

	/**
	 * Determine whether the user can update the invoice.
	 *
	 * @param \Carlos\Amores\Models\User    $user
	 * @param \Carlos\Amores\Models\Invoice $invoice
	 *
	 * @return mixed
	 */
	public function update(User $user, Invoice $invoice)
	{
		return $user->isSuperadmin();
	}

	/**
	 * Determine whether the user can delete the invoice.
	 *
	 * @param \Carlos\Amores\Models\User    $user
	 * @param \Carlos\Amores\Models\Invoice $invoice
	 *
	 * @return mixed
	 */
	public function delete(User $user, Invoice $invoice)
	{
		return $user->isSuperadmin();
	}

	/**
	 * Determine whether the user can restore the invoice.
	 *
	 * @param \Carlos\Amores\Models\User    $user
	 * @param \Carlos\Amores\Models\Invoice $invoice
	 *
	 * @return mixed
	 */
	public function restore(User $user, Invoice $invoice)
	{
		return $user->isSuperadmin();
	}

	/**
	 * Determine whether the user can permanently delete the invoice.
	 *
	 * @param \Carlos\Amores\Models\User    $user
	 * @param \Carlos\Amores\Models\Invoice $invoice
	 *
	 * @return mixed
	 */
	public function forceDelete(User $user, Invoice $invoice)
	{
		return $user->isSuperadmin();
	}
}
