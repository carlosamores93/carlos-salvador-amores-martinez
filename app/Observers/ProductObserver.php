<?php

namespace Carlos\Amores\Observers;

use Carlos\Amores\Models\Product;

class ProductObserver
{
	/**
	 * Handle the product "created" event.
	 *
	 * @param \Carlos\Amores\Product $product
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function created(Product $product)
	{
		info('ProductObserver created(...)');
	}

	/**
	 * Handle the product "updated" event.
	 *
	 * @param \Carlos\Amores\Product $product
	 */
	public function updated(Product $product)
	{
		info('ProductObserver updated(...)');
		if ($product->quantity === 0 && $product->isAvailable()) {
			$product->status = Product::PRODUCTO_NO_DISPONIBLE;
			$product->save();
		}
	}

	/**
	 * Handle the product "deleted" event.
	 *
	 * @param \Carlos\Amores\Product $product
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function deleted(Product $product)
	{
	}

	/**
	 * Handle the product "restored" event.
	 *
	 * @param \Carlos\Amores\Product $product
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function restored(Product $product)
	{
	}

	/**
	 * Handle the product "force deleted" event.
	 *
	 * @param \Carlos\Amores\Product $product
	 *
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function forceDeleted(Product $product)
	{
	}
}
