<?php

declare(strict_types=1);

namespace Carlos\Amores\Observers;

use Carlos\Amores\Models\CsamLog;
use Carlos\Amores\Models\User;
use Illuminate\Support\Facades\Auth;

class UserObserver
{
	/**
	 * Handle the User "created" event.
	 *
	 * @param \Carlos\Amores\Models\User $user
	 */
	public function created(User $user)
	{
		info(__CLASS__);
		info('_______________________________');
		$result = $user->getChanges();
		info($result);
		info('_______________________________');
		$json = json_encode($user->toArray());
		info($json);
		info(json_decode($json, true));
		CsamLog::create([
			'product_type' => User::class,
			'product_id' => $user->id,
			'user_id' => Auth::id(),
			'value' => $json,
		]);
		info(__FUNCTION__);
	}

	/**
	 * Handle the User "updated" event.
	 *
	 * @param \Carlos\Amores\Models\User $user
	 */
	public function updated(User $user)
	{
		info(__CLASS__);
		info('_______________________________');
		$result = $user->getChanges();
		info($result);
		info('_______________________________');
		$json = json_encode($user->toArray());
		info($json);
		info(json_decode($json, true));
		// info($user->toArray());
		info(__FUNCTION__);
		unset($result['updated_at']);
		CsamLog::create([
			'product_type' => User::class,
			'product_id' => $user->id,
			'user_id' => Auth::id(),
			'value' => json_encode($result),
		]);
	}

	/**
	 * Handle the User "deleted" event.
	 *
	 * @param \Carlos\Amores\Models\User $user
	 */
	public function deleted(User $user)
	{
		info(__CLASS__);
		info($user->toArray());
		info(__FUNCTION__);
	}

	/**
	 * Handle the User "restored" event.
	 *
	 * @param \Carlos\Amores\Models\User $user
	 */
	public function restored(User $user)
	{
		info(__CLASS__);
		info($user->toArray());
		info(__FUNCTION__);
	}

	/**
	 * Handle the User "force deleted" event.
	 *
	 * @param \Carlos\Amores\Models\User $user
	 */
	public function forceDeleted(User $user)
	{
		info(__CLASS__);
		info($user->toArray());
		info(__FUNCTION__);
	}
}
