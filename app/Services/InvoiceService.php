<?php

declare(strict_types=1);

namespace Carlos\Amores\Services;

use Carlos\Amores\Repositories\InvoiceRepository;

class InvoiceService
{
	private $invoiceRepository;

	public function __construct(
		InvoiceRepository $invoiceRepository
	) {
		$this->invoiceRepository = $invoiceRepository;
	}

	/**
	 * Get invoices.
	 *
	 * @return Collection|null
	 */
	public function getInvoices()
	{
		return $this->invoiceRepository->getInvoices();
	}
}
