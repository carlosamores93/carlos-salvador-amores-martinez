<?php

declare(strict_types=1);

namespace Carlos\Amores\Exceptions;

use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Traits\ApiResponse;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Handler extends ExceptionHandler
{
	use ApiResponse;

	/**
	 * A list of the exception types that are not reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
	];

	/**
	 * A list of the inputs that are never flashed for validation exceptions.
	 *
	 * @var array
	 */
	protected $dontFlash = [
		'password',
		'password_confirmation',
	];

	/**
	 * Report or log an exception.
	 *
	 * @param \Throwable $exception
	 */
	public function report(Throwable $exception)
	{
		parent::report($exception);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Throwable               $exception
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Throwable $exception)
	{
		if ($exception instanceof HttpExceptionInterface
			|| !Str::contains(url()->current(), 'api')) {
			return parent::render($request, $exception);
		}

		if ($exception instanceof ValidationException) {
			return $this->convertValidationExceptionToResponse($exception, null);
		}
		if ($exception instanceof ModelNotFoundException) {
			$model = class_basename($exception->getModel());

			return $this->errorResponse(
				'No existe instancia de ' . $model . ' con el id especificado',
				HttpStatusCodes::NOT_FOUND
			);
		}
		if ($exception instanceof AuthenticationException) {
			return $this->unauthenticated($request, $exception);
		}
		if ($exception instanceof AuthorizationException) {
			return $this->errorResponse(
				'No tienes permisos para esta acción',
				HttpStatusCodes::FORBIDDEN
			);
		}
		if ($exception instanceof NotFoundHttpException) {
			return $this->errorResponse(
				'No se encontró la url especificada',
				HttpStatusCodes::NOT_FOUND
			);
		}
		if ($exception instanceof MethodNotAllowedHttpException) {
			return $this->errorResponse(
				'El método especificado en la petición no es válido',
				HttpStatusCodes::METHOD_NOT_ALLOWED
			);
		}
		if ($exception instanceof HttpException) {
			return $this->errorResponse(
				$exception->getMessage(),
				$exception->getStatusCode()
			);
		}
		// dump($exception->getMessage());
		if ($exception instanceof QueryException) {
			return $this->errorResponse(
				'No se puede relacionar esta recurso porque está relacionado con otro',
				HttpStatusCodes::CONFLICT
			);
		}

		if (config('app.debug')) {
			return parent::render($request, $exception);
		}

		return $this->errorResponse(
			'Fallo inesperado, intentelo luego',
			HttpStatusCodes::INTERNAL_SERVER_ERROR
		);
	}

	/**
	 * Convert an authentication exception into a response.
	 *
	 * @param \Illuminate\Http\Request                 $request
	 * @param \Illuminate\Auth\AuthenticationException $exception
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	protected function unauthenticated($request, AuthenticationException $exception)
	{
		if (!Str::contains(url()->current(), 'api')) {
			return parent::unauthenticated($request, $exception);
		}

		return $this->errorResponse('No autenticado', HttpStatusCodes::UNAUTHORIZED);
	}

	/**
	 * Create a response object from the given validation exception.
	 *
	 * @param \Illuminate\Validation\ValidationException $exception
	 * @param \Illuminate\Http\Request                   $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response *
	 */
	protected function convertValidationExceptionToResponse(
		ValidationException $exception,
		$request
	) {
		if (!Str::contains(url()->current(), 'api')) {
			return parent::convertValidationExceptionToResponse($exception, $request);
		}
		$errors = $exception->validator->errors()->getMessages();

		return $this->errorResponse($errors, HttpStatusCodes::UNPROCESSABLE_ENTITY);
	}
}
