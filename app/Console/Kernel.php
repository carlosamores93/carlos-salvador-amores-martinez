<?php

namespace Carlos\Amores\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		Commands\NoticeCommand::class,
		Commands\CarlosAmoresCommand::class,
		Commands\ScheduleListCommand::class,
		Commands\InvoiceCommand::class,
		Commands\GenerateInvoicesCommand::class,
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param \Illuminate\Console\Scheduling\Schedule $schedule
	 */
	protected function schedule(Schedule $schedule)
	{
		// $schedule->command('inspire')->hourly();
		$schedule->command('command:notices')->dailyAt('18:00');
		$schedule->command('command:invoice')->monthly('12:00');
		// $schedule->command('command:generate-invoice')->monthly('12:00');
	}

	/**
	 * Register the commands for the application.
	 */
	protected function commands()
	{
		$this->load(__DIR__ . '/Commands');

		require base_path('routes/console.php');
	}
}
