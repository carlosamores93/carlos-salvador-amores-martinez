<?php

namespace Carlos\Amores\Console\Commands;

use Carlos\Amores\Lib\InvoiceProcessor;
use Carlos\Amores\Mail\InvoiceMail;
use Carlos\Amores\Mail\TestEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class InvoiceCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'command:invoice {year?} {month?}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate all invoices last month, can pass argumts as year and month';

	protected $invoiceProcessor;

	/**
	 * Create a new command instance.
	 */
	public function __construct(
		InvoiceProcessor $invoiceProcessor
	) {
		parent::__construct();
		$this->invoiceProcessor = $invoiceProcessor;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		if ($this->existsAtguments()) {
			return;
		}
		$lastMonth = now()->subMonth();
		$meHotmail = 'amorescarlos93@hotmail.com';
		$meGmail = 'amorescarlos93@gmail.com';
		// $chaperra = 'cuevacuevacarlosalberto9@gmail.com';
		$monthNumber = $lastMonth->format('m');
		$year = $lastMonth->format('Y');
		$month = $lastMonth->isoFormat('MMMM');
		$subject = 'Generate invoices ' . $lastMonth->format('F');

		try {
			$this->invoiceProcessor->generateInvoices(
				$monthNumber,
				$year
			);
			$this->invoiceProcessor->downloadAllInvoice(
				$monthNumber,
				$year
			);
		} catch (\Throwable $th) {
			$message = 'Wrong generate invoices: ' . $th->getMessage();
			$this->sentMailDefault($meHotmail, $subject, $message);
			$data = ['message' => $message];
			Mail::to($meGmail)->send(new TestEmail($data));

			return;
		}
		$message = 'Generation invoices correctly';
		$this->sentMailDefault($meHotmail, $subject, $message);
		$this->sentMailable($meGmail, $subject, null, $month, $monthNumber, $year);
		// if (!App::environment(['local', 'staging'])) {
		// 	$this->sentMailable($chaperra, $subject, null, $month, $monthNumber, $year);
		// }
	}

	/**
	 * Execute generate invoices for year and month.
	 */
	private function existsAtguments(): bool
	{
		$year = $this->argument('year');
		$month = $this->argument('month');
		if (empty($year) || empty($month)) {
			return false;
		}
		$month = now()->setMonth($month)->format('m');
		$this->invoiceProcessor->generateInvoices($month, $year);
		$this->invoiceProcessor->downloadAllInvoice($month, $year);

		return true;
	}

	/**
	 * Sent mail default.
	 *
	 * @param string $to
	 * @param string $subject
	 * @param string $message
	 */
	private function sentMailDefault(
		string $to,
		string $subject,
		string $message
	) {
		mail(
			$to,
			$subject,
			$message
		);
	}

	/**
	 * Sent mailable.
	 *
	 * @param string $to
	 * @param string $subject
	 * @param string $message
	 * @param string $month
	 * @param string $monthNumber
	 */
	private function sentMailable(
		string $to,
		string $subject,
		string $message = null,
		string $month,
		string $monthNumber,
		string $year
	) {
		Mail::to($to)->send(
			new InvoiceMail(
				$subject,
				$message,
				$month,
				$monthNumber,
				$year
			)
		);
	}
}
