<?php

namespace Carlos\Amores\Console\Commands\Closure;

class UserLogicTest
{
	public static function tryToLogin($user, $pass, $closure)
	{
		$userTest = null;
		if ($user === 'user' && $pass === '123456') {
			$userTest = new UserTest();
			$userTest->id = 1;
			$userTest->name = 'Carlos Amores';
			$userTest->userName = 'carlosamores93';
		}
		$closure($userTest);
	}
}
