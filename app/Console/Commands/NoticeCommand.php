<?php

namespace Carlos\Amores\Console\Commands;

use Carbon\Carbon;
use Carlos\Amores\Lib\Constants\Constants;
use Carlos\Amores\Mail\NoticeEmail;
use Carlos\Amores\Models\Notice;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class NoticeCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'command:notices';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Recordatorios a mi correo';

	/**
	 * Create a new command instance.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$notices = Notice::where('status', true)->get();
		foreach ($notices as $notice) {
			$this->checkNoticeToday($notice);
			$this->checkNoticeDay($notice, 1);
			$this->checkNoticeDay($notice, Constants::TWO);
			$this->checkNoticeWeek($notice);
		}
	}

	/**
	 * Check day notice.
	 *
	 * @param Notice $notice
	 * @param int    $day
	 */
	private function checkNoticeDay(Notice $notice, int $day)
	{
		$week = today()->addDays($day);
		$noticeDate = Carbon::parse($notice->date);
		if ($noticeDate->month === $week->month
			&& $noticeDate->day === $week->day
		) {
			$message = 'After ' . $day . ' day. ' . $notice->message;
			if ($notice->type === 'birthday') {
				$edad = (int) today()->year - (int) $noticeDate->year;
				$message = $notice->name . ' will be ' . $edad .
					' years old in ' . $day . ' day. ' . $notice->message;
			}
			$this->sendEmailNotice(
				$notice->subject . ' de ' . $notice->name,
				$message
			);
		}
	}

	/**
	 * Check notice in a week.
	 *
	 * @param Notice $notice
	 */
	private function checkNoticeWeek(Notice $notice)
	{
		$week = today()->addWeek();
		$noticeDate = Carbon::parse($notice->date);
		if ($noticeDate->month === $week->month
			&& $noticeDate->day === $week->day
		) {
			$message = 'After week. ' . $notice->message;
			if ($notice->type === 'birthday') {
				$edad = (int) today()->year - (int) $noticeDate->year;
				$message = $notice->name . ' will be ' . $edad .
					' years old in a week. ' . $notice->message;
			}
			$this->sendEmailNotice(
				$notice->subject . ' de ' . $notice->name,
				$message
			);
		}
	}

	/**
	 * Check today notice.
	 *
	 * @param Notice $notice
	 */
	private function checkNoticeToday(Notice $notice)
	{
		$today = today();
		$noticeDate = Carbon::parse($notice->date);
		// $dateNotice->equalTo($today)
		if ($today->month === $noticeDate->month
			&& $today->day === $noticeDate->day
		) {
			if ($notice->type === 'birthday') {
				$edad = (int) $today->year - (int) $noticeDate->year;
				$message = $notice->name . ' are ' . $edad .
					' years old today. ' . $notice->message;
			}
			$this->sendEmailNotice(
				$notice->subject . ' de ' . $notice->name,
				isset($message) ? $message : $notice->message
			);
			$this->checkRepeatNotice($notice);
		}
	}

	/**
	 * Check if notice repeat.
	 *
	 * @param Notice $notice
	 */
	private function checkRepeatNotice(Notice $notice)
	{
		if (!(bool) $notice->repeat) {
			$notice->status = false;
			$notice->save();
			$notice->delete();
		}
	}

	/**
	 * Send email.
	 *
	 * @param mixed $asunto
	 * @param mixed $mensaje
	 */
	private function sendEmailNotice($asunto, $mensaje)
	{
		$this->line('Sending email...');
		$this->info('Subject: ' . strip_tags($asunto));
		$this->info('Message: ' . strip_tags($mensaje));
		mail(
			'amorescarlos93@hotmail.com',
			strip_tags($asunto),
			strip_tags($mensaje)
		);
		Mail::to('amorescarlos93@gmail.com')->send(
			new NoticeEmail(
				$asunto,
				$mensaje
			)
		);
		Mail::to('amorescarlos93@hotmail.com')->send(
			new NoticeEmail(
				$asunto,
				$mensaje
			)
		);

		$this->line('Email sent');
	}
}
