<?php

namespace Carlos\Amores\Console\Commands;

use Carbon\Carbon;
use Carlos\Amores\Lib\Constants\Constants;
use Cron\CronExpression;
use Illuminate\Console\Command;
use Illuminate\Console\Scheduling\Schedule;

class ScheduleListCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'command:schedule-list';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'List when scheduled commands are executed.';

	/**
	 * Var doc.
	 *
	 * @var Schedule
	 */
	protected $schedule;

	/**
	 * Create a new command instance.
	 */
	public function __construct(Schedule $schedule)
	{
		parent::__construct();
		$this->schedule = $schedule;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$events = array_map(function ($event) {
			return [
				'cron' => $event->expression,
				'command' => static::fixupCommand($event->command),
				'next' => $event->nextRunDate()->toDateTimeString(),
				'previous' => static::previousRunDate($event->expression)->toDateTimeString(),
			];
		}, $this->schedule->events());

		$this->table(
			['Cron', 'Command', 'Next Run', 'Previous Run'],
			$events
		);
	}

	/**
	 * If it's an artisan command, strip off the PHP.
	 *
	 * @param $command
	 *
	 * @return string
	 */
	protected static function fixupCommand($command)
	{
		$parts = explode(' ', $command);
		if (count($parts) > Constants::TWO && $parts[1] === "'artisan'") {
			array_shift($parts);
		}

		return implode(' ', $parts);
	}

	/**
	 * Determine the previous run date for an event.
	 *
	 * @param string $expression
	 *
	 * @return \Carbon\Carbon
	 */
	protected static function previousRunDate($expression)
	{
		return Carbon::instance(
			CronExpression::factory($expression)->getPreviousRunDate()
		);
	}
}
