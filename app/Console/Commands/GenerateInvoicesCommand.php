<?php

declare(strict_types=1);

namespace Carlos\Amores\Console\Commands;

use Carlos\Amores\Jobs\GenerateInvoicesJob;
use Carlos\Amores\Jobs\InvoicesInZipJob;
use Carlos\Amores\Mail\InvoiceMail;
use Carlos\Amores\Mail\TestEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Mail;
use Throwable;

class GenerateInvoicesCommand extends Command
{
	// For create test
	// https://medium.com/@nasrulhazim/write-laravel-artisan-commands-with-test-df8858b1a38f
	public const CARLOS_AMORES_HOTMAIL = 'amorescarlos93@hotmail.com';
	public const CARLOS_AMORES_GMAIL = 'amorescarlos93@gmail.com';
	public const CARLOS_CUEVA_GMAIL = 'cuevacuevacarlosalberto9@gmail.com';

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'command:generate-invoice {year?} {month?}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate invoices last month, can pass argumts as year and month';

	/**
	 * Create a new command instance.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 */
	public function handle()
	{
		if ($this->existsAtguments()) {
			return;
		}
		$lastMonth = now()->subMonth();
		$month = $lastMonth->format('m');
		$year = $lastMonth->format('Y');
		$this->createJobToGenerateInvoices($year, $month);
	}

	/**
	 * Execute generate invoices for year and month.
	 */
	private function existsAtguments(): bool
	{
		$year = $this->argument('year');
		$month = $this->argument('month');
		if (empty($year) || empty($month)) {
			return false;
		}
		$month = now()->setMonth($month)->format('m');
		$this->createJobToGenerateInvoices($year, $month);

		return true;
	}

	/**
	 * Create jobs to generate invoices.
	 *
	 * @param string $year
	 * @param string $month
	 */
	private function createJobToGenerateInvoices(string $year, string $month)
	{
		$failed = '';
		$lastMonth = now()->subMonth();
		$subject = 'Generate invoices ' . $lastMonth->format('F');
		$monthName = $lastMonth->isoFormat('MMMM');
		Bus::chain([
			new GenerateInvoicesJob($year, $month),
			new InvoicesInZipJob($year, $month),
		])
		->catch(function (Throwable $th) use (&$failed) {
			// A batch job has failed!
			$failed = 'Wrong generate invoices: ' . $th->getMessage();
		})
		->onQueue('generate-invoices')
		// ->delay(now()->addMinutes(1))
		->dispatch();
		dd($failed);
		if ($failed !== '') {
			$this->sentMailDefault(self::CARLOS_AMORES_HOTMAIL, $subject, $failed);
			$data = ['message' => $failed];
			Mail::to(self::CARLOS_CUEVA_GMAIL)->send(new TestEmail($data));

			return;
		}
		$message = 'Generation invoices correctly';
		$this->sentMailDefault(self::CARLOS_AMORES_HOTMAIL, $subject, $message);
		$this->sentMailable(self::CARLOS_AMORES_GMAIL, $subject, null, $monthName, $month, $year);
		if (!App::environment(['local', 'staging'])) {
			$this->sentMailable(self::CARLOS_CUEVA_GMAIL, $subject, null, $monthName, $month, $year);
		}
	}

	/**
	 * Sent mail default.
	 *
	 * @param string $to
	 * @param string $subject
	 * @param string $message
	 */
	private function sentMailDefault(string $to, string $subject, string $message)
	{
		mail($to, $subject, $message);
	}

	/**
	 * Sent mailable.
	 *
	 * @param string $to
	 * @param string $subject
	 * @param string $message
	 * @param string $month
	 * @param string $monthNumber
	 */
	private function sentMailable(
		string $to,
		string $subject,
		string $message = null,
		string $month,
		string $monthNumber,
		string $year
	) {
		Mail::to($to)->send(
			new InvoiceMail($subject, $message, $month, $monthNumber, $year)
		);
	}
}
