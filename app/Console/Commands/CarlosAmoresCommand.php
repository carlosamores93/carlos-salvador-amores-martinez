<?php

declare(strict_types=1);

namespace Carlos\Amores\Console\Commands;

use Carlos\Amores\Console\Commands\Closure\UserLogicTest;
use Carlos\Amores\Jobs\SendSimpleEmailJob;
use Carlos\Amores\Mail\TestEmail;
use Carlos\Amores\Models\Invoice;
use Faker\Factory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Mail;
use Throwable;

class CarlosAmoresCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'command:csam';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command for play';

	/**
	 * Create a new command instance.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->calculateDistance();
		dd();
		$this->testJob();
		$this->testEmail();
		$this->testFactory();
		$this->testClosure();
	}

	/**
	 * Calculate distance between two coordinates.
	 *
	 *
	 * @SuppressWarnings(PHPMD.ShortVariable)
	 */
	public function calculateDistance()
	{
		// Coordenadas de Madrid, calle  Hacienda de Pavones, 178
		$lat1 = 40.401903;
		$lon1 = -3.641294;
		// Coordenadas de Madrid, FDI, UCM: 40.452955, -3.733814
		$lat2 = 40.452955;
		$lon2 = -3.733814;
		// Segun google maps: Distancia total: 9,68 km (6,01 mi)

		// Radio de la Tierra en km
		$earthRadius = 6371;
		// Convertir grados a radianes
		$lat1 = deg2rad($lat1);
		$lon1 = deg2rad($lon1);
		$lat2 = deg2rad($lat2);
		$lon2 = deg2rad($lon2);
		// Diferencias de longitud y latitud
		$dlat = $lat2 - $lat1;
		$dlon = $lon2 - $lon1;
		// Fórmula de Haversine
		$a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
		$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
		$distance = $earthRadius * $c;

		$this->info('La distancia entre las dos coordenadas es: ' . $distance . ' km');
		$this->info('La distancia entre las dos coordenadas es: ' . $distance / 0.6214 . ' mi');
	}

	/**
	 * Test jobs.
	 *
	 * @link https://laravel.com/docs/8.x/queues#job-chaining
	 */
	private function testJob(): void
	{
		// QUEUE_CONNECTION = database
		$this->info('Job for send email');
		$data = ['message' => 'JOB JOB JOB JOB JOB JOB JOB JOB JOB JOB JOB '];
		$otherData = ['message' => 'QUEUE QUEUE QUEUE QUEUE QUEUE '];
		SendSimpleEmailJob::dispatch($data, true);
		SendSimpleEmailJob::dispatch($data, false);
		SendSimpleEmailJob::dispatch($data, false)->delay(now()->addMinutes(1));
		// php artisan queue:listen --queue=email
		SendSimpleEmailJob::dispatch($data, false)->onQueue('email');
		Bus::chain([
			new SendSimpleEmailJob($data, false),
			new SendSimpleEmailJob($otherData, false),
		])->catch(function (Throwable $e) {
			// A job within the chain has failed...
			SendSimpleEmailJob::dispatch(['message' => $e->getMessage()], false);
		})
			->onQueue('chain')
			->delay(now()->addMinutes(1))
			->dispatch();

		// Envia los jobs a la queue.
		Bus::batch([
			new SendSimpleEmailJob($data, false),
			new SendSimpleEmailJob($otherData, false),
		])
			->catch(function (Throwable $e) {
				// A batch job has failed!
				SendSimpleEmailJob::dispatch(['message' => $e->getMessage()], false);
			})
			->then(function () {
				// All jobs have successfully run!
				SendSimpleEmailJob::dispatch(['message' => 'THEN'], false)
					->delay(now()->addMinutes(1));
			})
			->finally(function () {
				// All jobs have run! Some may have failed.
				SendSimpleEmailJob::dispatch(['message' => 'FINALLY'], false)
					->delay(now()->addMinutes(1));
			})
			->name('BATCH')
			->dispatch();
	}

	/**
	 * Test closure.
	 *
	 * @link https://anexsoft.com/implementacion-de-closures-en-php
	 */
	private function testClosure(): void
	{
		$this->info('******* CLOSURE ********');
		UserLogicTest::tryToLogin('user', '123456', function ($res) {
			dump($res);
		});
		UserLogicTest::tryToLogin('user', '654321', function ($res2) {
			dump($res2);
		});

		// Closure: funciones anónimas
		// Heredar $mensaje
		$mensaje = 'CSAM';
		$ejemplo = function () use ($mensaje) {
			dump($mensaje);
		};
		$ejemplo();

		$mensaje = 'Amores';
		// Los cierres también aceptan argumentos normales
		$ejemplo = function ($arg) use ($mensaje) {
			dump($arg . ' ' . $mensaje);
		};
		$ejemplo('Carlos');
	}

	/**
	 * Test email.
	 */
	private function testEmail(): void
	{
		$this->info('Send email to amorescarlos93@gmail.com');
		$data = ['message' => 'This is a test!'];
		Mail::to('amorescarlos93@gmail.com')->send(new TestEmail($data));
		$this->info('Email enviado a amorescarlos93@gmail.com');
	}

	/**
	 * Test factories.
	 *
	 * @link https://fakerphp.github.io/
	 */
	private function testFactory(): void
	{
		$this->info('Factory with faker es_ES');
		$invoice = Invoice::factory()->create();
		dump($invoice->toArray());
		$invoice->forceDelete();
		$this->info('Factory with faker es_VE');
		$faker = Factory::create('es_VE');
		$invoice = Invoice::factory()->create([
			'address' => $faker->address(),
			'status' => $faker->randomElement([true, false]),
			'city' => $faker->city(),
			'nif' => $faker->nationalId(),
		]);
		dump($invoice->toArray());
		$invoice->forceDelete();
	}
}
