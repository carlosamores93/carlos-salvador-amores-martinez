<?php

declare(strict_types=1);

namespace Carlos\Amores\Traits;

use Carlos\Amores\Lib\Constants\Constants;
use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Spatie\Fractal\Facades\Fractal;

trait ApiResponse
{
	/**
	 * Undocumented function.
	 *
	 * @param [type] $data
	 * @param [type] $code
	 *
	 * @return JsonResponse
	 */
	private function successResponse($data, $code)
	{
		return response()->json($data, $code);
	}

	/**
	 * Undocumented function.
	 *
	 * @param [type] $message
	 * @param [type] $code
	 *
	 * @return JsonResponse
	 */
	protected function errorResponse($message, $code)
	{
		return response()->json(['error' => $message, 'code' => $code], $code);
	}

	/**
	 * Undocumented function.
	 *
	 * @param Collection $collection
	 * @param int        $code
	 *
	 * @return JsonResponse
	 */
	protected function showAll(
		Collection $collection,
		int $code = HttpStatusCodes::OK
	) {
		$transformer = $collection->first()->transformer;
		if ($collection->isEmpty() || empty($transformer)) {
			return $this->successResponse(['data' => $collection], $code);
		}
		$collection = $this->filterData($collection, $transformer);
		$collection = $this->sortData($collection, $transformer);
		$collection = $this->paginateData($collection);
		$collection = $this->transformData($collection, $transformer);

		return $this->successResponse($collection, $code);
	}

	/**
	 * Undocumented function.
	 *
	 * @param Model $instance
	 * @param int   $code
	 *
	 * @return JsonResponse
	 */
	protected function showOne(
		Model $instance,
		int $code = HttpStatusCodes::OK
	) {
		$transformer = $instance->transformer;
		if (empty($transformer)) {
			return $this->successResponse(['data' => $instance], $code);
		}
		$instance = $this->transformData($instance, $transformer);

		return $this->successResponse($instance, $code);
	}

	/**
	 * Undocumented function.
	 *
	 * @param string $message
	 * @param int    $code
	 *
	 * @return JsonResponse
	 */
	protected function showMessage(
		string $message,
		int $code = HttpStatusCodes::OK
	) {
		return $this->successResponse(['data' => $message], $code);
	}

	/**
	 * Transform data.
	 *
	 * @param [type] $data
	 * @param [type] $transformer
	 *
	 * @return array
	 *
	 * @SuppressWarnings(PHPMD.MissingImport)
	 */
	protected function transformData($data, $transformer)
	{
		// $transformation = Fractal::create($data, new $transformer);
		$transformation = fractal($data, new $transformer());

		return $transformation->toArray();
	}

	/**
	 * Sort data.
	 *
	 * @param Collection $collection
	 *
	 * @return Collection
	 */
	protected function sortData(Collection $collection, $transformer): Collection
	{
		if (request()->has('sort_by')) {
			$attribute = $transformer::originalAttribute(request()->sort_by);
			// $collection = $collection->sortBy($attribute);
			$collection = $collection->sortBy->{$attribute};
		}

		return $collection;
	}

	/**
	 * Filter data.
	 *
	 * @param Collection $collection
	 * @param [type]     $transformer
	 *
	 * @return Collection
	 */
	protected function filterData(Collection $collection, $transformer): Collection
	{
		foreach (request()->query() as $query => $value) {
			$attribute = $transformer::originalAttribute($query);
			if (isset($attribute, $value)) {
				// $collection = $collection->sortBy($attribute);
				$collection = $collection->where($attribute, $value);
			}
		}

		return $collection;
	}

	/**
	 * Undocumented function.
	 *
	 * @param Collection $collection
	 */
	protected function paginateData(Collection $collection)
	{
		$rule = [
			'per_page' => 'integer|min:2|max:50',
		];
		Validator::validate(request()->all(), $rule);

		$page = LengthAwarePaginator::resolveCurrentPage();
		$perPage = Constants::FIVE;
		if (request()->has('per_page')) {
			$perPage = (int) request()->per_page;
		}
		$results = $collection->slice(($page - 1) * $perPage, $perPage)->values();
		$paginated = new LengthAwarePaginator(
			$results,
			$collection->count(),
			$perPage,
			$page,
			['path' => LengthAwarePaginator::resolveCurrentPath()]
		);
		$paginated->appends(request()->all());

		return $paginated;
	}

	protected function cacheResponse(array $data)
	{
		$url = request()->url();
		$ttl = 1 * Constants::SIXTY * Constants::SIXTY;

		return Cache::remember($url, $ttl, function () use ($data) {
			return $data;
		});
	}
}
