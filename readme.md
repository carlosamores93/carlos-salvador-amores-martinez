# Carlos Salvador Amores Martínez


- **Página principal: [Home](http://carlosamores.hol.es)**
- Página entrar al CMS: [Login](http://carlosamores.hol.es/login).
- Blog: [Blog](http://carlosamores.hol.es/blog).
- API Doc: [Blog](http://carlosamores.hol.es/api/documentation).


## Setup project

### 1. Clonar repositorio
```sh
git clone pathRepository [optionalName]
```
### 2. Composer
```sh
composer install
```
### 3. Generate key
```sh
php artisan key:generate
```
### 4. Create .env
Create .env and update parameters and auth.json
```sh
cp .env.example .env
cp auth.json.example auth.json
```
### 5. Install NPM
```sh
npm install
```
### 6. Copy files to public folder
```sh
npm run dev

or

cp -r resources/templates/* public/
cp -r resources/img/* public/
```
### 7. Execute migrations
```sh
php artisan migrate

or

php artisan migrate  --seed

or

php artisan migrate
php artisan db:seed
```
### 8. Run project
```sh
php artisan serve
```

### 8. Run swagger doc
```sh
php artisan l5-swagger:generate
```

## CONECT DATABASE AND MORE

Default host is localhost(127.0.0.1)
```
mysql -h host -u username -p

More commands
show databases;
use mibasedatos;
create database miprueba;
use miprueba;
show tables;
describe name_table;

Create table and operation
create table prueba (id_prueba int);
insert into prueba (id_prueba) values (1);
select * from prueba;
```

## BACKUP DATABASE
```sh
mysqldump -u username -p database_to_backup > backup_name.sql
```

## RESTORE DATABASE
Before create a new database and after make backup

#### Create db
```sh
mysql -u username -p
CREATE DATABASE database_name;
exit;
```

#### Restore database with backup
```sh
mysql -u username -p database_name < backup_name.sql
```

## My changelog format
All notable changes to this project will be documented in my changelog: [Page changelog](https://keepachangelog.com/es-ES/0.3.0/)

#### [Tag] - DATE
##### Added
##### Changed
##### Deprecated
##### Removed
##### Fixed
##### Security



## My commits
All my commits are based in this document: [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0-beta.4/)
```sh
<type>: <description>

[optional body]

[optional footer]
```

```sh
type [
    'build',
    'ci',
    'chore',
    'docs',
    'feat',
    'fix',
    'perf',
    'refactor',
    'revert',
    'style',
    'test'
]
```

## Execute migrations and factories
```sh
php artisan migrate  --seed
php artisan migrate:refresh  --seed
```

## Composer in production
Execute composer in production, user composer 2
```sh
composer self-update --2
```
```sh
composer install --no-dev

OR

//  TO USER COMPOSER 2
php composer.phar install  --no-dev
```

## Others
CRON in php:
```sh
php /path/artisan schedule:run
php /path/artisan queue:work
php /path/artisan queue:listen --queue=email
```