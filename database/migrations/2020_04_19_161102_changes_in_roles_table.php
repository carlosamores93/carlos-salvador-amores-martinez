<?php

use Carlos\Amores\Models\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ChangesInRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('roles')->where('id', Role::SUPER_ADMIN)->update(
            [
                'name' => Role::SUPER_ADMIN_NAME,
                'key' => Str::slug(Role::SUPER_ADMIN_NAME),
            ]
        );

        DB::table('roles')->where('id', Role::ADMIN)->update(
            [
                'name' => Role::ADMIN_NAME,
                'key' => Str::slug(Role::ADMIN_NAME),
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('roles')->where('id', Role::SUPER_ADMIN)->update(
            [
                'name' => 'SuperAdmin',
                'key' => 'superadmin'
            ]
        );

        DB::table('roles')->where('id', Role::ADMIN)->update(
            [
                'name' => 'Admin',
                'key' => 'admin'
            ]
        );
    }
}
