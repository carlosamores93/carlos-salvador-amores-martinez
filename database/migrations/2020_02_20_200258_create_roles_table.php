<?php

use Carlos\Amores\Models\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('key');
            //$table->timestamps();
        });

        DB::table('roles')->insert(
            [
                'id' => Role::SUPER_ADMIN,
                'name' => 'SuperAdmin',
                'key' => 'superadmin'
            ]
        );

        DB::table('roles')->insert(
            [
                'id' => Role::ADMIN,
                'name' => 'Admin',
                'key' => 'admin'
            ]
        );

        DB::table('roles')->insert(
            [
                'id' => Role::DEFAULT,
                'name' => 'Default',
                'key' => 'default'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
