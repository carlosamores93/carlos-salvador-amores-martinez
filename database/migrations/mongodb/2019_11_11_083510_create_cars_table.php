<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{

	// Run migration in mongodb
	// php artisan migrate --path database/migrations/mongodb --database mongodb
	// php artisan migrate:status --path database/migrations/mongodb --database mongodb

	// In database default
	// php artisan migrate:rollback --path database/migrations/mongodb

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cars', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('company')->nullable();
			$table->string('model')->nullable();
			$table->boolean('status')->default(0);
			$table->string('slug')->nullable();
			$table->text('description')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('cars');
	}
}
