<?php

use Carlos\Amores\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('verified')
                ->default(User::USER_NOT_VERIFIED)
                ->after('website');
            $table->string('verification_token')->nullable()->after('verified');
            $table->string('admin')
                ->default(User::USER_REGULAR)
                ->after('verification_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('verified');
            $table->dropColumn('verification_token');
            $table->dropColumn('admin');
        });
    }
}
