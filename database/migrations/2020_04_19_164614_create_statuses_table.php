<?php

use Carlos\Amores\Models\Status;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('key');
            $table->timestamps();
        });

        DB::table('statuses')->insert(
            [
                'id' => Status::ACTIVE,
                'name' => Status::ACTIVE_NAME,
                'key' => Str::slug(Status::ACTIVE_NAME),
                'created_at' => now(),
                'updated_at' => now(),
            ]
        );
        DB::table('statuses')->insert(
            [
                'id' => Status::INACTIVE,
                'name' => Status::INACTIVE_NAME,
                'key' => Str::slug(Status::INACTIVE_NAME),
                'created_at' => now(),
                'updated_at' => now(),
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }
}
