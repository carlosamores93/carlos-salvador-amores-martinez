<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsInHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('houses', function (Blueprint $table) {
            $table->boolean('like')->default(false)->after('ascensor');
            $table->longText('catastro')->nullable()->after('ascensor');
            $table->longText('link_ad')->nullable()->after('ascensor');
            $table->longText('map')->nullable()->after('ascensor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('houses', function (Blueprint $table) {
            $table->dropColumn('like');
            $table->dropColumn('catastro');
            $table->dropColumn('link_ad');
            $table->dropColumn('map');
        });
    }
}
