<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('direccion')->nullable();
            $table->string('numero')->nullable();
            $table->string('piso')->nullable();
            $table->string('puerta')->nullable();
            $table->string('precio')->nullable();
            $table->string('superficie')->nullable();
            $table->string('precio_m_cuadrado')->nullable();
            $table->string('habitaciones')->nullable();
            $table->string('terraza')->nullable();
            $table->string('cocina')->nullable();
            $table->string('banio')->nullable();
            $table->string('salon')->nullable();
            $table->string('trastero')->nullable();
            $table->string('ascensor')->nullable();
            $table->string('fecha_construccion')->nullable();
            $table->string('estado_cargas')->nullable();
            $table->string('inspecccion_tecnica_edificios')->nullable();
            $table->string('estado_fontaneria_electricidad')->nullable();
            $table->string('comunidad_propietarios')->nullable();
            $table->string('obras_previstas_edificio')->nullable();
            $table->string('calefaccion')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
