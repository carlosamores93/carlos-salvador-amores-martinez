<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCsamLogsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('csam_logs', function (Blueprint $table) {
			$table->id();
			$table->string('product_type');
			$table->unsignedBigInteger('product_id');
			$table->unsignedBigInteger('user_id')->nullable();
			$table->json('value');
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('csam_logs');
	}
}
