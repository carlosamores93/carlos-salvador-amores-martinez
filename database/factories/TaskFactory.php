<?php

namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carlos\Amores\Models\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
	return [
		'name' => $faker->name,
		'categories' => json_encode([]),
		'status' => $faker->randomElement(Task::TYPES),
		'number' => $faker->numberBetween(1, 200),
	];
});
