<?php

namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carlos\Amores\Models\Post;
use Carlos\Amores\Models\PostType;
use Carlos\Amores\Models\Status;
use Carlos\Amores\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Post::class, function (Faker $faker) {
    $title = $faker->text(50);
    $published = $faker->randomElement(
        [
            true,
            false,
        ]
    );
    return [
        'title' => $title,
        'slug' => Str::slug($title),
        'subtitle' => $faker->text(150),
        'published' => $published,
        'description' => $faker->text,
        'published_at' => $published ? now() : null,
        'user_id' => factory(User::class)->create()->id,
        'status_id' => $faker->randomElement(
            [
                Status::ACTIVE,
                Status::INACTIVE,
            ]
        ),
        'post_type_id' => $faker->randomElement(
            [
                PostType::DEFAULT,
            ]
        ),
    ];
});
