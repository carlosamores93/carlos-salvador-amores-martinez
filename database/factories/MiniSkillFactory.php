<?php

namespace Database\Factories;

use Carlos\Amores\Models\MiniSkill;
use Faker\Generator as Faker;

$factory->define(MiniSkill::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->jobTitle,
        'progress' => $faker->numberBetween(0, 100),
        'status' => $faker->randomElement([0, 1])
    ];
});
