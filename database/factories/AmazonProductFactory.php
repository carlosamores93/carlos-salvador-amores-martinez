<?php

namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carlos\Amores\Models\AmazonProduct;
use Faker\Generator as Faker;

$factory->define(AmazonProduct::class, function (Faker $faker) {
    return [
        'description' => $faker->paragraph(1),
        'content' => '<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="https://rcm-eu.amazon-adsystem.com/e/cm?ref=qf_sp_asin_til&t=amorescarlos9-21&m=amazon&o=30&p=8&l=as1&IS2=1&asins=B07MQJWP42&linkId=0b582d07645797d979a066adeb31b888&bc1=000000&lt1=_blank&fc1=333333&lc1=0066c0&bg1=ffffff&f=ifr">
    </iframe>',
        'status' => $faker->randomElement([true, false]),
    ];
});
