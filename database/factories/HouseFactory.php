<?php

namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carlos\Amores\Models\House;
use Faker\Generator as Faker;

$factory->define(House::class, function (Faker $faker) {
    $price = $faker->randomElement(
        [
            100000,
            110000,
            120000,
            130000,
            140000,
            150000,
        ]
    );
    $m = $faker->numberBetween(60, 100);
    return [
        'direccion' => $faker->address,
        'numero' => $faker->numberBetween(1, 200),
        'piso' => $faker->randomElement([1, 2, 3, 4, 5]),
        'puerta' => $faker->randomElement(['a', 'b']),
        'precio' => $price,
        'superficie' => $m,
        'precio_m_cuadrado' => $price / $m,
        'habitaciones' => $faker->text,
        'terraza' => $faker->text,
        'cocina' => $faker->text,
        'banio' => $faker->text,
        'salon' => $faker->text,
        'trastero' => $faker->text,
        'ascensor' => $faker->text,
        'map' => $faker->text,
        'link_ad' => $faker->text,
        'catastro' => $faker->text,
        'like' => $faker->randomElement([true, false]),
        'fecha_construccion' => $faker->text,
        'estado_cargas' => $faker->text,
        'inspecccion_tecnica_edificios' => $faker->text,
        'estado_fontaneria_electricidad' => $faker->text,
        'comunidad_propietarios' => $faker->text,
        'obras_previstas_edificio' => $faker->text,
        'calefaccion' => $faker->text,
        'description' => $faker->text,
    ];
});
