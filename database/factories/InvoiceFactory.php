<?php

namespace Database\Factories;

use Carlos\Amores\Models\Invoice;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceFactory extends Factory
{

	/**
	 * The name of the factory's corresponding model.
	 *
	 * @var string
	 */
	protected $model = Invoice::class;

	/**
	 * Define the model's default state.
	 *
	 * @return array
	 */
	public function definition()
	{
		// Invoice::factory()->count(5)->create();
		return [
			'address' => $this->faker->address(),
			'status' => $this->faker->randomElement([true, false]),
			'city' =>  $this->faker->city(),
			'nif' => $this->faker->dni(),
			'description' => json_encode(
				[
					[
						'service' => 'Jardinería',
						'description' => 'Mantenimiento de jardines',
						'price' => 100
					]
				]
			),
		];
	}
}
