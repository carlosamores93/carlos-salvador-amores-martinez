<?php

namespace Database\Seeders;

use Carlos\Amores\Models\Invoice;
use Illuminate\Database\Seeder;

class InvoiceSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// php artisan db:seed --class=InvoiceSeeder
		Invoice::factory()->count(5)->create();
	}
}
