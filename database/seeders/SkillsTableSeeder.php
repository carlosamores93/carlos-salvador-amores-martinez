<?php

namespace Database\Seeders;


use Carlos\Amores\Models\Skill;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Crear Seeder
        // php artisan make:seeder SkillsTableSeeder

        // Ejecutar
        //php artisan db:seed --class=SkillsTableSeeder
        //

        //factory(Skill::class, 30)->create();

        DB::table('skills')->insert(
            [
                'title' => 'SEO',
                'slug' => 'seo',
                'description' => 'SEO',
                'status' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'title' => 'Laravel',
                'slug' => 'laravel',
                'description' => 'Laravel',
                'status' => true,
                'created_at' => now(),
                'updated_at' => now()
            ]
        );

        Skill::create(
            [
                'title' => 'Atlassian',
                'slug' => 'atlassian',
                'description' => 'Atlassian',
                'status' => true
            ]
        );
    }
}
