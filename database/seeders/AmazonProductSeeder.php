<?php

namespace Database\Seeders;

use Carlos\Amores\Models\AmazonProduct;
use Illuminate\Database\Seeder;

class AmazonProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AmazonProduct::class, 5)->create();
    }
}
