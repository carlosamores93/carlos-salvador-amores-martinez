<?php

namespace Database\Seeders;

use Carlos\Amores\Models\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=PostsTableSeeder
        factory(Post::class, 5)->create();
    }
}
