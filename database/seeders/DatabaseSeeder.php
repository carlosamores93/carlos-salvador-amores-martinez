<?php

namespace Database\Seeders;

use Carlos\Amores\Models\Category;
use Carlos\Amores\Models\MiniSkill;
use Carlos\Amores\Models\Product;
use Carlos\Amores\Models\Skill;
use Carlos\Amores\Models\Transaction;
use Carlos\Amores\Models\Work;
use Carlos\Amores\Models\Article;
use Carlos\Amores\Models\Post;
use Carlos\Amores\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		// Volver migraciones atras y rellenar tablas
		// php artisan migrate:refresh  --seed

		// Rellenar poco a poco
		// php artisan db:seed

		//$this->call(UsersTableSeeder::class);

		User::flushEventListeners();

		if (App::environment(['local', 'staging'])) {
			factory(Skill::class, 5)->create();
			factory(MiniSkill::class, 5)->create();
			factory(Work::class, 2)->create();
		}

		$this->call(
			[
				UsersTableSeeder::class,
				WorksTableSeeder::class,
				SkillsTableSeeder::class,
				MiniSkillsTableSeeder::class,
				ArticlesTableSeeder::class,
				PostsTableSeeder::class,
				HouseSeeder::class,
			]
		);

		factory(Post::class, 5)->create();
		factory(Article::class, 5)->create();

		// Factories to API
		factory(User::class, 100)->create();
		factory(Category::class, 30)->create();
		factory(Product::class, 100)->create()->each(
			function ($product) {
				$categories = Category::all()->random(mt_rand(1, 5))->pluck('id');
				$product->categories()->attach($categories);
			}
		);
		factory(Transaction::class, 100)->create();
	}
}
