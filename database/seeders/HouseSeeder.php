<?php

namespace Database\Seeders;

use Carlos\Amores\Models\House;
use Illuminate\Database\Seeder;

class HouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=HousesTableSeeder
        factory(House::class, 5)->create();
    }
}
