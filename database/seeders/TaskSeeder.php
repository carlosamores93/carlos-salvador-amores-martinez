<?php

namespace Database\Seeders;

use Carlos\Amores\Models\Task;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//php artisan db:seed --class=TaskSeeder
		factory(Task::class, 5)->create();
	}
}
