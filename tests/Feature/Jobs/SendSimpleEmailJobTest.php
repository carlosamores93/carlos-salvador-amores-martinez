<?php

namespace Tests\Feature\Jobs;

use Carlos\Amores\Jobs\SendSimpleEmailJob;
use Illuminate\Support\Facades\Bus;
use Tests\IntegrationTestCase;

class SendSimpleEmailJobTest extends IntegrationTestCase
{
	/**
	 * A basic feature test example.
	 */
	public function testSendSimpleEmailJob()
	{
		Bus::fake();
		SendSimpleEmailJob::dispatch([], false);
		// Assert that a job was dispatched...
		Bus::assertDispatched(SendSimpleEmailJob::class);
	}
}
