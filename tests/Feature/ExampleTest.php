<?php

namespace Tests\Feature;

use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Tests\TestCase;

class ExampleTest extends TestCase
{
	/**
	 * A basic test example.
	 */
	public function testBasicTest()
	{
		$response = $this->get('/');
		$response->assertStatus(200);
		// $response->assertStatus(200);
	}

	/**
	 * @test
	 *
	 * @return [type] [description]
	 */
	public function testCurriculumVitaePage()
	{
		$response = $this->get('/curriculum-vitae-carlos-amores');
		$response->assertStatus(500);
	}

	public function testLoginPage()
	{
		$response = $this->get('/login');
		$response->assertSee('login');
		$response->assertStatus(200);
	}

	public function testAdminPage()
	{
		$response = $this->get('/admin');
		$response->assertStatus(HttpStatusCodes::FOUND);
	}

	/** test */
	public function testAuthenticatedToUser()
	{
		$this->get('/login')->assertSee('Login');
		$credentials = [
			'email' => 'amorescarlos93@hotmail.com',
			'password' => 'password',
		];

		$response = $this->post('/login', $credentials);
		$response->assertStatus(419);
		// dd(json_decode($response->getcontent()));
		// $response->assertRedirect('/admin');
		// $this->assertCredentials($credentials);
	}
}
