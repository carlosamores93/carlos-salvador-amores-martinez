<?php

declare(strict_types=1);

namespace Tests\Integration\Controllers\Api\Curriculum;

use Carlos\Amores\Models\Skill;
use Tests\IntegrationTestCase;

class SkillControllerTest extends IntegrationTestCase
{
	/**
	 * A basic feature test example.
	 */
	public function testIndex()
	{
		$times = 5;
		factory(Skill::class, $times)->create([
			'status' => true,
		]);
		$response = $this->json('GET', 'api/skills');
		// $response = $this->get('api/skills');
		$response->assertStatus(200);
		$skills = json_decode($response->getContent());
		$this->assertEquals($times, count($skills->data));
	}

	public function testShow()
	{
		$skill = factory(Skill::class)->create();
		$response = $this->json('GET', "api/skills/{$skill->id}");
		// $response = $this->get('api/skills');
		$response->assertStatus(200);
		$skillResp = json_decode($response->getContent());
		$this->assertEquals($skill->id, $skillResp->data->id);
	}
}
