<?php

declare(strict_types=1);

namespace Tests\Integration\Controllers\Api\Curriculum;

use Carlos\Amores\Models\Work;
use Tests\IntegrationTestCase;

class WorkControllerTest extends IntegrationTestCase
{
	/**
	 * A basic feature test example.
	 */
	public function testIndex()
	{
		$times = 5;
		factory(Work::class, $times)->create([
			'status' => true,
		]);
		$response = $this->json('GET', 'api/works');
		// $response = $this->get('api/works');
		$response->assertStatus(200);
		$works = json_decode($response->getContent());
		$this->assertEquals($times, count($works->data));
	}

	public function testShow()
	{
		$work = factory(Work::class)->create();
		$response = $this->json('GET', "api/works/{$work->id}");
		// $response = $this->get('api/works');
		$response->assertStatus(200);
		$workResp = json_decode($response->getContent());
		$this->assertEquals($work->id, $workResp->data->id);
	}
}
