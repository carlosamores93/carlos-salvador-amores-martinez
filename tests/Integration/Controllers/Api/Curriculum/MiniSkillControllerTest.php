<?php

declare(strict_types=1);

namespace Tests\Integration\Controllers\Api\Curriculum;

use Carlos\Amores\Models\MiniSkill;
use Tests\IntegrationTestCase;

class MiniSkillControllerTest extends IntegrationTestCase
{
	/**
	 * A basic feature test example.
	 */
	public function testIndex()
	{
		$times = 5;
		factory(MiniSkill::class, $times)->create([
			'status' => true,
		]);
		$response = $this->json('GET', 'api/miniskills');
		// $response = $this->get('api/skills');
		$response->assertStatus(200);
		$miniSkills = json_decode($response->getContent());
		$this->assertEquals($times, count($miniSkills->data));
	}

	public function testShow()
	{
		$miniSkill = factory(MiniSkill::class)->create();
		$response = $this->json('GET', "api/miniskills/{$miniSkill->id}");
		// $response = $this->get('api/skills');
		$response->assertStatus(200);
		$miniSkillResp = json_decode($response->getContent());
		$this->assertEquals($miniSkill->id, $miniSkillResp->data->id);
	}
}
