<?php

declare(strict_types=1);

namespace Tests\Integration\Controllers\Admin;

use Carlos\Amores\Lib\Constants\HttpStatusCodes;
use Carlos\Amores\Models\Role;
use Carlos\Amores\Models\User;
use Carlos\Amores\Models\Work;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\IntegrationTestCase;

class WorkControllerCRUDTest extends IntegrationTestCase
{
	use WithoutMiddleware;

	public function setUp(): void
	{
		parent::setUp();
		$user = factory(User::class)->create(
			[
				'role_id' => Role::SUPER_ADMIN,
			]
		);
		$this->actingAs($user);
	}

	/**
	 * A basic feature test example.
	 */
	public function testIndex()
	{
		$times = 5;
		factory(Work::class, $times)->create([
			'status' => true,
		]);
		$response = $this->json('GET', 'admin/work');
		$response->assertSee('All works');
		$response->assertStatus(HttpStatusCodes::OK);
	}

	public function testCreate()
	{
		$response = $this->json('GET', 'admin/work/create');
		$response->assertSee('New work');
		$response->assertStatus(HttpStatusCodes::OK);
	}

	public function testStore()
	{
		$data = [
			'company' => 'Company',
			'job' => 'Job',
			'status' => true,
			'description' => 'description',
			'image' => null,
			'start_date' => now(),
			'end_date' => null,
			// 'csrf-token' => csrf_token(),
		];
		$response = $this->post('admin/work', $data);
		// $response = $this->json('POST', 'admin/work', $data);
		$response->assertStatus(HttpStatusCodes::FOUND);
		$works = Work::all();
		$this->assertCount(1, $works);
	}

	public function testEdit()
	{
		$work = factory(Work::class)->create();
		$response = $this->json('GET', 'admin/work/11/edit');
		$response->assertStatus(HttpStatusCodes::NOT_FOUND);
		$response = $this->json('GET', "admin/work/{$work->id}/edit");
		$response->assertSee('Edit work');
		$response->assertStatus(HttpStatusCodes::OK);
	}

	public function testShow()
	{
		$work = factory(Work::class)->create();
		$response = $this->json('GET', "admin/work/{$work->id}");
		$response->assertStatus(HttpStatusCodes::NOT_FOUND);
	}

	public function testUpdate()
	{
		$company = 'CSAM SL';
		$work = factory(Work::class)->create();
		$data = $work->toArray();
		$data['company'] = $company;
		$response = $this->json('PATCH', "admin/work/{$work->id}", $data);
		$response->assertStatus(HttpStatusCodes::FOUND);
		$works = Work::all();
		$this->assertCount(1, $works);
		$this->assertEquals($company, $works->first()->company);
	}

	public function testDestroy()
	{
		$work = factory(Work::class)->create();
		$response = $this->json('DELETE', "admin/work/{$work->id}");
		$response->assertStatus(HttpStatusCodes::FOUND);
		$works = Work::all();
		$this->assertCount(0, $works);
	}
}
