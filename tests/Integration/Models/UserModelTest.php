<?php

namespace Tests\Integration\Models;

use Carlos\Amores\Models\Role;
use Carlos\Amores\Models\User;
use Tests\IntegrationTestCase;

class UserModelTest extends IntegrationTestCase
{
	private $model;

	protected function setUp(): void
	{
		parent::setUp();
		$this->model = app()->make(User::class);
	}

	protected function tearDown(): void
	{
		parent::tearDown();
	}

	/**
	 * A basic feature test example.
	 */
	public function testGetUserSuperAdmin()
	{
		// Error tu acces super-admin user
		$user = factory(User::class)->create(
			[
				'role_id' => Role::SUPER_ADMIN,
			]
		);
		$response = $this->get('/api/users/' . $user->id);
		$response->assertStatus(409);
		$this->assertEquals(
			'No se puede ver al usuario al superadmin',
			json_decode($response->getContent())->error
		);
	}

	public function testGetUser()
	{
		$user = factory(User::class)->create();
		$response = $this->get('/api/users/' . $user->id);
		$response->assertStatus(200);

		$response = $this->json('GET', '/api/users/' . $user->id);
		$this->assertEquals(
			$user->id,
			json_decode($response->getContent(), true)['data']['Id']
		);
	}

	public function testGetUserWithAtutentication()
	{
		$user = factory(User::class)->create(
			[
				'role_id' => Role::SUPER_ADMIN,
			]
		);
		$response = $this->get('super-admin/user');
		$response->assertStatus(302);

		$this->actingAs($user);
		$response = $this->get('super-admin/user');
		$response->assertStatus(200);
	}

	public function testStoreUser()
	{
		$data = [
			'email' => 'a@a.a',
			'name' => 'AAA',
			'password' => 'password',
		];
		$response = $this->json('POST', '/api/users/', $data);
		$response->assertStatus(200);
		$this->assertEquals(
			$data['name'],
			json_decode($response->getContent(), true)['data']['Name']
		);
	}

	public function testStoreUserWithAtutentication()
	{
		$user = factory(User::class)->create(
			[
				'role_id' => Role::SUPER_ADMIN,
			]
		);
		$this->actingAs($user);

		$data = [
			'email' => 'a@a.a',
			'name' => 'AAA',
			'password' => 'password',
		];
		$response = $this->json('POST', '/super-admin/user/', $data);
		$response->assertStatus(419);
	}

	public function testUpdateUser()
	{
		$user = factory(User::class)->create();
		$data = [
			'email' => 'test@test.com',
		];
		$response = $this->json('PUT', '/api/users/' . $user->id, $data);
		$response->assertStatus(200);
		$this->assertEquals(
			$data['email'],
			json_decode($response->getContent(), true)['data']['Email']
		);
	}

	public function testDestroyUser()
	{
		$user = factory(User::class)->create();
		$response = $this->json('DELETE', '/api/users/' . $user->id);
		$response->assertStatus(200);
		$this->assertNotNull(
			json_decode($response->getContent(), true)['data']['FechaBorrado']
		);
	}

	public function testScopeForVerificationToken()
	{
		$token = 'klk';
		$userToken = factory(User::class)->create([
			'verification_token' => $token,
		]);

		$user = $this->model->forVerificationToken($token)->first();
		$this->assertEquals(
			$user->id,
			$userToken->id
		);
	}

	public function testVerifyUser()
	{
		$token = 'klk';
		$user = factory(User::class)->create([
			'verification_token' => $token,
		]);
		$this->assertEquals(
			$user->verified,
			User::USER_NOT_VERIFIED
		);
		$response = $this->get('/api/users/verify/' . $token);
		$response->assertStatus(200);
		$this->assertEquals(
			'La cuenta ha sido verificada',
			json_decode($response->getContent())->data
		);
		$user->refresh();
		$this->assertEquals(
			$user->verified,
			User::USER_VERIFIED
		);
	}
}
