<?php

declare(strict_types=1);

namespace Tests\Integration\Models;

use Carlos\Amores\Models\Post;
use Carlos\Amores\Models\User;
use Tests\IntegrationTestCase;

class PostModelTest extends IntegrationTestCase
{
	private $model;

	protected function setUp(): void
	{
		parent::setUp();
		$this->model = new Post();
	}

	protected function tearDown(): void
	{
		parent::tearDown();
	}

	public function testScopeForName()
	{
		$user = factory(User::class)->create([
			'name' => 'Carlos',
		]);
		$this->assertNull($this->model->forName('Carlos')->first());
		factory(Post::class)->create(
			[
				'user_id' => $user->id,
			]
		);
		$this->assertNull($this->model->forName('Salva')->first());
		$this->assertNotNull($this->model->forName('Carlos')->first());
	}

	public function testScopeForLastname()
	{
		$user = factory(User::class)->create([
			'lastname' => 'Amores',
		]);
		$this->assertNull($this->model->forLastname('Amores')->first());
		factory(Post::class)->create(
			[
				'user_id' => $user->id,
			]
		);
		$this->assertNull($this->model->forLastname('Martinez')->first());
		$this->assertNotNull($this->model->forLastname('Amores')->first());
	}

	public function testScopeForNameAndLastname()
	{
		$user = factory(User::class)->create([
			'name' => 'Carlos',
			'lastname' => 'Amores',
		]);
		$this->assertNull(
			$this->model->forName('Carlos')->forLastname('Amores')->first()
		);
		factory(Post::class)->create(
			[
				'user_id' => $user->id,
			]
		);
		$this->assertNull(
			$this->model->forName('Carloss')->forLastname('Amoress')->first()
		);
		$this->assertNotNull(
			$this->model->forName('Carlos')->forLastname('Amores')->first()
		);
	}
}
