<?php

declare(strict_types=1);

namespace Tests\Integration\Models;

use Carlos\Amores\Models\MiniSkill;
use Tests\IntegrationTestCase;

class MiniSkillModelTest extends IntegrationTestCase
{
	private $model;

	protected function setUp(): void
	{
		parent::setUp();
		$this->model = app()->make(MiniSkill::class);
	}

	protected function tearDown(): void
	{
		parent::tearDown();
	}

	/**
	 * A basic feature test example.
	 */
	public function testScopeForStatus()
	{
		factory(MiniSkill::class)->create(
			[
				'status' => false,
			]
		);
		$response = $this->model->forStatus(true);
		$this->assertEquals(0, $response->count());
		$response = $this->model->forStatus(false);
		$this->assertEquals(1, $response->count());
	}
}
