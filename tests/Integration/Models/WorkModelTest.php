<?php

namespace Tests\Integration\Models;

use Carlos\Amores\Models\Work;
use Tests\IntegrationTestCase;

class WorkModelTest extends IntegrationTestCase
{
	private $model;

	protected function setUp(): void
	{
		parent::setUp();
		$this->model = app()->make(Work::class);
	}

	protected function tearDown(): void
	{
		parent::tearDown();
	}

	/**
	 * A basic feature test example.
	 */
	public function testScopeActiveWork()
	{
		$work = factory(Work::class)->create(
			[
				'status' => false,
			]
		);
		$response = $this->model->activeWork();
		$this->assertEquals(0, $response->count());
		$work->status = true;
		$work->save();
		$this->assertEquals(1, $response->count());
		$this->assertEquals($work->id, $response->first()->id);
	}
}
