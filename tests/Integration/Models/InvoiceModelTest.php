<?php

declare(strict_types=1);

namespace Tests\Integration\Models;

use Carlos\Amores\Models\Invoice;
use Tests\IntegrationTestCase;

class InvoiceModelTest extends IntegrationTestCase
{
	private $model;

	protected function setUp(): void
	{
		parent::setUp();
		$this->model = app()->make(Invoice::class);
	}

	protected function tearDown(): void
	{
		parent::tearDown();
	}

	/**
	 * A basic feature test example.
	 */
	public function testScopeForStatus()
	{
		Invoice::factory()->create(
			[
				'status' => false,
			]
		);
		$response = $this->model->forStatus(true);
		$this->assertEquals(0, $response->count());
		$response = $this->model->forStatus(false);
		$this->assertCount(1, $response->get());
	}

	public function testScopeForNif()
	{
		$nif = '12345678A';
		$response = $this->model->forNif($nif);
		$this->assertEquals(0, $response->count());
		Invoice::factory()->create(
			[
				'nif' => $nif,
			]
		);
		$response = $this->model->forNif($nif);
		$this->assertCount(1, $response->get());
	}
}
