<?php

declare(strict_types=1);

namespace Tests\Integration\Models;

use Carlos\Amores\Models\Task;
use Tests\IntegrationTestCase;

class TaskModelTest extends IntegrationTestCase
{
	private $model;

	protected function setUp(): void
	{
		parent::setUp();
		$this->model = app()->make(Task::class);
	}

	protected function tearDown(): void
	{
		parent::tearDown();
	}

	public function testGetAllTask()
	{
		$count = 3;
		factory(Task::class, $count)->create();
		$response = $this->get('/api/tasks');
		$response->assertStatus(200);

		$response = $this->json('GET', '/api/tasks');
		$this->assertEquals(
			$count,
			count(json_decode($response->getContent(), true)['data'])
		);
	}

	public function testGetTask()
	{
		$task = factory(Task::class)->create();
		$response = $this->get('/api/tasks/' . $task->id);
		$response->assertStatus(200);

		$response = $this->json('GET', '/api/tasks/' . $task->id);
		$this->assertEquals(
			$task->id,
			json_decode($response->getContent(), true)['data']['id']
		);
	}

	public function testStoreTask()
	{
		$data = [
			'name' => 'Carlos',
			'categories' => json_encode([]),
			'status' => 'relax',
			'number' => 10,
		];
		$response = $this->json('POST', '/api/tasks/', $data);
		$response->assertStatus(201);
		$this->assertEquals(
			$data['name'],
			json_decode($response->getContent(), true)['data']['name']
		);
	}

	public function testUpdateTask()
	{
		$task = factory(Task::class)->create();
		$data = [
			'name' => 'carlos',
		];
		$response = $this->json('PUT', '/api/tasks/' . $task->id, $data);
		$response->assertStatus(200);
		$this->assertEquals(
			$data['name'],
			json_decode($response->getContent(), true)['data']['name']
		);
	}

	public function testDestroyTask()
	{
		$task = factory(Task::class)->create();
		$response = $this->json('DELETE', '/api/tasks/' . $task->id);
		$response->assertStatus(200);
		$this->assertNotNull(
			json_decode($response->getContent(), true)['data']['deleted_at']
		);
	}
}
