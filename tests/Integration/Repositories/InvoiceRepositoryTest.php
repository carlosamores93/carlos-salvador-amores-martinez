<?php

declare(strict_types=1);

namespace Tests\Integration\Repositories;

use Carlos\Amores\Models\Invoice;
use Carlos\Amores\Repositories\InvoiceRepository;
use Tests\IntegrationTestCase;

class InvoiceRepositoryTest extends IntegrationTestCase
{
	private $invoiceRepository;

	protected function setUp(): void
	{
		parent::setUp();
		$this->invoiceRepository = new InvoiceRepository(
			new Invoice()
		);
	}

	protected function tearDown(): void
	{
		parent::tearDown();
	}

	public function testGetInvoices()
	{
		$resp = $this->invoiceRepository->getInvoices();
		$this->assertEquals(0, $resp->count());
		$times = 10;
		Invoice::factory()->count($times)->create([
			'status' => true,
		]);
		$resp = $this->invoiceRepository->getInvoices();
		$this->assertCount($times, $resp);
	}

	public function testGetInvoicesByStatus()
	{
		$times = 10;
		Invoice::factory()->count($times)->create([
			'status' => false,
		]);
		$resp = $this->invoiceRepository->getInvoicesByStatus(true);
		$this->assertEquals(0, $resp->count());
		$resp = $this->invoiceRepository->getInvoicesByStatus(false);
		$this->assertCount($times, $resp);
	}

	public function testGetInvoiceByStatusAndNif()
	{
		$nif = '12345678Z';
		$resp = $this->invoiceRepository->getInvoiceByStatusAndNif(true, $nif);
		$this->assertNull($resp);
		Invoice::factory()->create([
			'status' => false,
			'nif' => $nif,
		]);
		$resp = $this->invoiceRepository->getInvoiceByStatusAndNif(false, $nif);
		$this->assertNotNull($resp);
	}
}
