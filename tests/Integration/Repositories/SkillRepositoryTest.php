<?php

declare(strict_types=1);

namespace Tests\Integration\Repositories;

use Carlos\Amores\Models\Skill;
use Carlos\Amores\Repositories\SkillRepository;
use Tests\IntegrationTestCase;

class SkillRepositoryTest extends IntegrationTestCase
{
	private $skillRepository;

	protected function setUp(): void
	{
		parent::setUp();
		$this->skillRepository = new SkillRepository(
			new Skill()
		);
		// $this->skillRepository = app()->make(SkillRepository::class);
	}

	protected function tearDown(): void
	{
		parent::tearDown();
	}

	public function testAllSkills()
	{
		$resp = $this->skillRepository->all();
		$this->assertEquals(
			0,
			$resp->count()
		);
		factory(Skill::class, 5)->create();
		$resp = $this->skillRepository->all();
		$this->assertEquals(
			5,
			$resp->count()
		);
	}

	public function testGetSkill()
	{
		$item = factory(Skill::class)->create();
		$resp = $this->skillRepository->show(
			$item->id
		);
		$this->assertEquals(
			$resp->id,
			$item->id
		);
	}
}
