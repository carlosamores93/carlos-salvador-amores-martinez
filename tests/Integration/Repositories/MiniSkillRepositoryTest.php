<?php

declare(strict_types=1);

namespace Tests\Integration\Repositories;

use Carlos\Amores\Models\MiniSkill;
use Carlos\Amores\Repositories\MiniSkillRepository;
use Tests\IntegrationTestCase;

class MiniSkillRepositoryTest extends IntegrationTestCase
{
	private $miniSkillRepository;

	protected function setUp(): void
	{
		parent::setUp();
		$this->miniSkillRepository = new MiniSkillRepository(
			new MiniSkill()
		);
	}

	protected function tearDown(): void
	{
		parent::tearDown();
	}

	public function testAllSkills()
	{
		$resp = $this->miniSkillRepository->all();
		$this->assertEquals(
			0,
			$resp->count()
		);
		factory(MiniSkill::class, 5)->create();
		$resp = $this->miniSkillRepository->all();
		$this->assertEquals(
			5,
			$resp->count()
		);
	}

	public function testGetSkill()
	{
		$item = factory(MiniSkill::class)->create();
		$resp = $this->miniSkillRepository->show(
			$item->id
		);
		$this->assertEquals(
			$resp->id,
			$item->id
		);
	}
}
