<?php

declare(strict_types=1);

namespace Tests\Integration\Repositories;

use Carlos\Amores\Models\AmazonProduct;
use Carlos\Amores\Repositories\AmazonProductRepository;
use Tests\IntegrationTestCase;

class AmazonProductRepositoryTest extends IntegrationTestCase
{
	private $amazonProductRepository;

	protected function setUp(): void
	{
		parent::setUp();
		$this->amazonProductRepository = new AmazonProductRepository(
			new AmazonProduct()
		);
		// $this->amazonProductRepository = app()->make(AmazonProductRepository::class);
	}

	protected function tearDown(): void
	{
		parent::tearDown();
	}

	public function testAllAmazonProducts()
	{
		$resp = $this->amazonProductRepository->all();
		$this->assertEquals(
			0,
			$resp->count()
		);
		factory(AmazonProduct::class, 5)->create();
		$resp = $this->amazonProductRepository->all();
		$this->assertEquals(
			5,
			$resp->count()
		);
	}

	public function testCreateAmazonProduct()
	{
		$amazonProduct = factory(AmazonProduct::class)->make();
		$data = $amazonProduct->toArray();
		$resp = $this->amazonProductRepository->create($data);
		$this->assertEquals(
			$amazonProduct->description,
			$resp->description
		);
	}

	public function testUpdateAmazonProducts()
	{
		$item = factory(AmazonProduct::class)->create();
		$newDescription = 'Hii';
		$this->assertNotEquals($item->description, $newDescription);
		$data = [];
		$data['description'] = $newDescription;
		$this->amazonProductRepository->update(
			$data,
			$item
		);
		$this->assertEquals($item->description, $newDescription);
	}

	public function testDeleteAmazonProducts()
	{
		$item = factory(AmazonProduct::class)->create();
		$this->amazonProductRepository->delete(
			$item
		);
		$this->assertTrue($item->trashed());
	}

	public function testFindAmazonProducts()
	{
		$item = factory(AmazonProduct::class)->create();
		$itemFind = $this->amazonProductRepository->find(
			$item->id
		);
		$this->assertEquals($item->id, $itemFind->id);
	}
}
