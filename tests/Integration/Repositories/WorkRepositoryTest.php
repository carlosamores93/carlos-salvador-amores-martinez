<?php

declare(strict_types=1);

namespace Tests\Integration\Repositories;

use Carlos\Amores\Models\Work;
use Carlos\Amores\Repositories\WorkRepository;
use Tests\IntegrationTestCase;

class WorkRepositoryTest extends IntegrationTestCase
{
	private $workRepository;

	protected function setUp(): void
	{
		parent::setUp();
		$this->workRepository = new WorkRepository(
			new Work()
		);
		// $this->workRepository = app()->make(WorkRepository::class);
	}

	protected function tearDown(): void
	{
		parent::tearDown();
	}

	public function testAllWorks()
	{
		$resp = $this->workRepository->all();
		$this->assertEquals(
			0,
			$resp->count()
		);
		factory(Work::class, 5)->create();
		$resp = $this->workRepository->all();
		$this->assertEquals(
			5,
			$resp->count()
		);
	}

	public function testGetWork()
	{
		$work = factory(Work::class)->create();
		$resp = $this->workRepository->show(
			$work->id
		);
		$this->assertEquals(
			$resp->id,
			$work->id
		);
	}
}
