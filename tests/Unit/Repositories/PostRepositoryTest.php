<?php

namespace Tests\Unit\Repositories;

use Carlos\Amores\Models\Post;
use Carlos\Amores\Repositories\PostRepository;
use Mockery;
use PHPUnit\Framework\TestCase;

class PostRepositoryTest extends TestCase
{
	private $model;
	private $repository;

	public function setUp(): void
	{
		parent::setUp();
		$this->model = Mockery::mock(Post::class);
		$this->repository = new PostRepository(
			$this->model
		);
	}

	public function tearDown(): void
	{
		parent::tearDown();
		Mockery::close();
	}

	/**
	 * A basic unit test example.
	 */
	public function testAll()
	{
		$this->model->shouldReceive('all')
			->with()
			->once()
			->andReturnSelf();
		$this->assertNotNull(
			$this->repository->all()
		);
	}

	public function testAllReturnNull()
	{
		$this->model->shouldReceive('all')
			->with()
			->once()
			->andReturn(null);
		$this->assertNull(
			$this->repository->all()
		);
	}
}
