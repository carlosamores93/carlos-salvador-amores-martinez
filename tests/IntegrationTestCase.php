<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class IntegrationTestCase extends TestCase
{
	use RefreshDatabase, WithFaker;
}
